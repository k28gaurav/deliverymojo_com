package com.traconmobi.tom;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.xmlpull.v1.XmlPullParserException;

import com.crashlytics.android.Crashlytics;


//import android.R;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
//import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.ActionBarActivity;
//import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.fabric.sdk.android.Fabric;


public class Start_End_TripMainActivity extends Activity {

    String currentTime, endcurTime;
    TextView version_name;
    Spinner s_trsprt, s_strt_loc, s_end_loc;
    TextView strt_tme, end_tme;
    TextView total_km_val, hrs_val, min_val, sec_val;
    public static String device_id, deviceid;
    public String s_trsport_val;
    public String s_strt_loc_val;
    public String s_end_loc_val;
    public String session_trip_resp, gettrip_response, session_DB_PATH, session_DB_PWD, session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME, gettomnoti_response, session_noti_resp, session_outscan_resp, getoutscan_response;
    public String trip_km, imei_value, trsport_mode, strt_tme_val, end_tme_val, total_km, imei_val, strt_loc_val, end_loc_val;
    SQLiteDatabase db, db_trip_complete, db_pickup_complete_resp1, db_pickup_complete_resp2;
    Cursor c, c_pickup_complete_sync_resp1, c_pickup_complete_sync_resp2;
    // Session Manager Class
    SessionManager session;
    static TinyDB tiny;

    //    private LongOperation tripTask;
    Button btn_endtrip, btn_strt_trip;
    public String strt_time_val, SESSION_START_TIME, SESSION_TRANSPORT, SESSION_START_LOCN, strt_CiDateTime, end_CiDateTime, SESSION_STARTED_TIME;
    boolean SESSION_FLAG_START;
    private boolean strt_cliked = false;
    String SESSION_End_Location, SESSION_End_dt, SESSION_END_trip_km;
    protected static final String END_TRIP_USERID = null;
    public String FORM_TYPE_VAL;
    //    ArrayAdapter<String>
    ArrayAdapter<String> adapter_strt_loc, adapter_trsprt, adapter_end_loc;
    //    ArrayAdapter adapter_strt_loc;
    ArrayList<String> list, list_trsprt, list_end_loc;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public static final String PREF_NAME = "Pager";

    //	android.support.v7.widget.Toolbar toolbar;
    ImageButton back;
    TextView title;
    Button strt_trip, end_trip;
    public String TAG = "Start_End_TripMainActivity";
//	private Toolbar mToolbar;
//    ArrayAdapter<CharSequence> 
//    ArrayAdapter<CharSequence>  adapter_trsprt;
  /*  @Override
       public void notifyHTTPRespons(final HttpHandler http)
    {
   		// TODO Auto-generated method stub
    	try{
	         session = new SessionManager(getApplicationContext());  
	   	 
	   		 if(http.getReqId() == 101)
	   		{	   			
	   			gettrip_response=http.getResponse().toString();
	   			session.createhttpsLoginSession(gettrip_response);
	   			
	   		}
    	}
    	catch(Exception e)
    	{
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Exception TOMWelcomeActivity notifyHTTPRespons " + e.getMessage());
//    		onClickLogOut();
    	}
   	}*/

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_start__end__trip_main);
            //getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_start__end__trip_main);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("START END TRIP");
            strt_trip = (Button) findViewById(R.id.btn_strttrip);
            end_trip = (Button) findViewById(R.id.btn_endtrip);

            tiny = new TinyDB(getApplicationContext());
            session = new SessionManager(getApplicationContext());
            /**
             * GETTING SESSION VALUES
             * */

            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);


            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);


            s_trsprt = (Spinner) findViewById(R.id.spinnertransport);

            s_strt_loc = (Spinner) findViewById(R.id.spinner_strt_loc);

            s_end_loc = (Spinner) findViewById(R.id.spinner_end_loc);

            btn_strt_trip = (Button) findViewById(R.id.btn_strttrip);
            btn_endtrip = (Button) findViewById(R.id.btn_endtrip);
            total_km_val = (TextView) findViewById(R.id.et_travelled_val);
            hrs_val = (TextView) findViewById(R.id.et_hrs_val);
            min_val = (TextView) findViewById(R.id.et_mins_val);
            sec_val = (TextView) findViewById(R.id.et_sec_val);

            strt_tme = (TextView) findViewById(R.id.tv_strt_tme_val);
            end_tme = (TextView) findViewById(R.id.tv_end_tme_val);

            Intent intnt = getIntent();
            FORM_TYPE_VAL = intnt.getStringExtra("NAV_FORM_TYPE_VAL");
            SESSION_FLAG_START = tiny.getBoolean("FLAG_START");

            if (SESSION_FLAG_START == false) {
                try {
                    strt_trip.setVisibility(View.VISIBLE);
                    end_trip.setVisibility(View.INVISIBLE);
                    btn_strt_trip.setEnabled(true);
                    btn_endtrip.setEnabled(true);

                    /*******************SELECT TRANSPORT**************************************************/
                    list_trsprt = new ArrayList<String>();
                    list_trsprt.add("BIKER");
                    list_trsprt.add("NON-BIKER");
                    list_trsprt.add("SELECT TRANSPORT");
                    final int listsize_trsprt = list_trsprt.size() - 1;
                    Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner listsize_trsprt " + listsize_trsprt + list_trsprt);
                    adapter_trsprt = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_trsprt) {
                        @Override
                        public int getCount() {
                            return (listsize_trsprt); // Truncate the list
                        }
                    };
                    adapter_trsprt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    s_trsprt.setAdapter(adapter_trsprt);
                    s_trsprt.setSelection(listsize_trsprt); // Hidden item to appear in the spinner
                    s_trsprt.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_trsport());

                    /*******************SELECT START LOCATION**************************************************/

//		        ArrayList<String> 
                    list = new ArrayList<String>();
                    list.add("OFFICE");
                    list.add("HOME");
                    list.add("FEEDER POINT");
                    list.add("SELECT START LOCATION");
                    final int listsize = list.size() - 1;
                    Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length " + listsize + list);
//		        ArrayAdapter<String> 
//		        adapter_strt_loc = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list) {
//		            @Override
//		            public int getCount() {
//		                return(listsize); // Truncate the list
//		            }
//		        };
                    adapter_strt_loc = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list) {
                        @Override
                        public int getCount() {
                            return (listsize); // Truncate the list
                        }
                    };
                    adapter_strt_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    s_strt_loc.setAdapter(adapter_strt_loc);
                    s_strt_loc.setSelection(listsize); // Hidden item to appear in the spinner

                    s_strt_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_strt_loc());

                    s_end_loc.setEnabled(false);
                    list_end_loc = new ArrayList<String>();
                    list_end_loc.add("OFFICE");
                    list_end_loc.add("HOME");
                    list_end_loc.add("FEEDER POINT");
                    list_end_loc.add("SELECT END LOCATION");
                    final int listsize_end_loc = list_end_loc.size() - 1;
                    Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length " + listsize_end_loc + list_end_loc);
                    adapter_end_loc = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_end_loc) {
                        @Override
                        public int getCount() {
                            return (listsize_end_loc); // Truncate the list
                        }
                    };
                    adapter_end_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    s_end_loc.setAdapter(adapter_end_loc);
                    s_end_loc.setSelection(listsize_end_loc); // Hidden item to appear in the spinner
                    s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());


                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Start_End_TripMainActivity SESSION_FLAG_START==false" + e.getMessage());
                }
            }


            if (SESSION_FLAG_START == true) {
                try {
                    strt_trip.setVisibility(View.INVISIBLE);
                    end_trip.setVisibility(View.VISIBLE);
                    s_trsprt.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_trsport());
                    s_strt_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_strt_loc());
                    s_strt_loc.setEnabled(false);
                    s_trsprt.setEnabled(false);
//			 s_strt_loc.setTextColor(Color.WHITE);

//			 s_trsprt.setTextColor(Color.WHITE);
                    btn_strt_trip.setEnabled(false);
                    btn_strt_trip.setTextColor(Color.WHITE);
                    SESSION_START_TIME = tiny.getString("strt_time_val");
                    SESSION_TRANSPORT = tiny.getString("transport_val");
                    SESSION_START_LOCN = tiny.getString("srt_loc_val");

                    int strt_loc_position = tiny.getInt("strt_loc_position");
                    int transposrt_position = tiny.getInt("position_trnspt");
//			 int end_loc_position=tiny.getInt("end_loc_position");

                    Crashlytics.log(android.util.Log.ERROR, TAG, "SESSION_START_TIME " + SESSION_START_TIME + SESSION_START_LOCN + SESSION_TRANSPORT + strt_loc_position + transposrt_position);
                    strt_tme.setText(SESSION_START_TIME);
//			String aa= s_strt_loc.getAdapter().getItem(strt_loc_position).toString();
//			 Crashlytics.log(android.util.Log.ERROR,TAG,"aa" +aa);
                    try {
//				 s_strt_loc.setEnabled(false);
//				 s_strt_loc.setFocusable(false);

                        list = new ArrayList<String>();
                        list.add("OFFICE");
                        list.add("HOME");
                        list.add("FEEDER POINT");
//		        list.add("SELECT START LOCATION");
                        final int listsize = list.size() - 1;
                        Crashlytics.log(android.util.Log.ERROR, TAG, "print flag true spinner length " + listsize + list);
//		        ArrayAdapter<String> 
//		        adapter_strt_loc = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list) {
//		            @Override
//		            public int getCount() {
//		                return(listsize); // Truncate the list
//		            }
//		        };
                        adapter_strt_loc = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list) {
                            @Override
                            public int getCount() {
                                return (listsize); // Truncate the list
                            }
                        };
                        adapter_strt_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length values here flag true" + list);
//				  s_strt_loc.setAdapter(adapter_strt_loc);
//				 int spinnerPostion = 
//				 int spinnerPostion1 = adapter_strt_loc.getPosition(SESSION_START_LOCN); 
//				  s_strt_loc.setSelection(getIndex(s_strt_loc, SESSION_START_LOCN));
//				  
//				  int pos=((ArrayList<String>) s_strt_loc.getAdapter()).indexOf(SESSION_START_LOCN);
                        int post = adapter_strt_loc.getPosition(SESSION_START_LOCN);
//			        getSpinnerField().setSelection(pos);
                        s_strt_loc.setAdapter(adapter_strt_loc);
                        Crashlytics.log(android.util.Log.ERROR, TAG, "pos value " + post);
                        s_strt_loc.setSelection(post);

                        /*********************************Transport spinner*****************************************************/
//				 s_trsprt.getSelectedView().setEnabled(false);
//				 s_trsprt.setEnabled(false);
//				 s_trsprt.setFocusable(false);
                        list_trsprt = new ArrayList<String>();
                        list_trsprt.add("BIKER");
                        list_trsprt.add("NON-BIKER");
                        list_trsprt.add("SELECT TRANSPORT");
                        final int listsize_trsprt = list_trsprt.size() - 1;
                        Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner listsize_trsprt " + listsize_trsprt + list_trsprt);
                        adapter_trsprt = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_trsprt) {
                            @Override
                            public int getCount() {
                                return (listsize_trsprt); // Truncate the list
                            }
                        };
                        adapter_trsprt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        s_trsprt.setAdapter(adapter_trsprt);
                        int trspt_pos = adapter_trsprt.getPosition(SESSION_TRANSPORT);
                        Crashlytics.log(android.util.Log.ERROR, TAG, "pos trspt_pos value " + post + trspt_pos);
//				  int transposrt_position1=tiny.getInt("position_trnspt");
                        s_trsprt.setSelection(trspt_pos);
                    } catch (Exception e) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Exception spinner value " + e.getMessage());
                    }


                    Intent intent = getIntent();

//		 		SESSION_End_Location=intent.getStringExtra("END_LOC");
                    SESSION_End_Location = intent.getStringExtra("END_LOC");
                    SESSION_End_dt = intent.getStringExtra("END_dt");
                    SESSION_END_trip_km = intent.getStringExtra("END_trip_km");

//				 Bundle extras = getIntent().getExtras();
//				  SESSION_End_Location = extras.getInt("END_LOC");
                    Crashlytics.log(android.util.Log.ERROR, TAG, "SESSION_End_Location pos " + SESSION_End_Location + SESSION_End_dt + SESSION_END_trip_km);


                    /*************************************COMMENTED ON 22 APR2015****************************************************************/
//		 		//commented on 19mar2015
//			 /** Create an ArrayAdapter using the string array and a default spinner end location layout*/
//		        ArrayAdapter<CharSequence> adapter_end_loc = ArrayAdapter.createFromResource(this,
//		                R.array.end_loc, android.R.layout.simple_spinner_item);
//		        /** Specify the layout to use when the list of choices appears*/
//		        adapter_end_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    /*****************************************************************************************************/
//		        list_end_loc = new ArrayList<String> ();
//		        list_end_loc.add("OFFICE");
//		        list_end_loc.add("HOME");
//		        list_end_loc.add("FEEDER POINT");
//		        list_end_loc.add("SELECT END LOCATION");
//		        final int listsize_end_loc = list_end_loc.size() - 1;
//		        Crashlytics.log(android.util.Log.ERROR,TAG,"print spinner length "+ listsize_end_loc + list_end_loc);
//		        adapter_end_loc = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_end_loc)
//		         		{
//					         @Override
//					         public int getCount() {
//					             return(listsize_end_loc); // Truncate the list
//					         }
//					     };
//					     adapter_end_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
//		        /** Apply the adapter to the spinner*/         
//		        
//		        s_end_loc.setAdapter(adapter_end_loc);
//		        s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());


                    if ((SESSION_End_Location == null || SESSION_End_Location == "" || SESSION_End_Location.equals("null") || SESSION_End_Location.equals("") || SESSION_End_Location == "null" || SESSION_End_Location.equals("SELECT END LOCATION")) && (SESSION_End_dt == null || SESSION_End_dt == "" || SESSION_End_dt.equals("null") || SESSION_End_dt.equals("") || SESSION_End_dt == "null") && (SESSION_END_trip_km == null || SESSION_END_trip_km == "" || SESSION_END_trip_km.equals("null") || SESSION_END_trip_km.equals("") || SESSION_END_trip_km == "null")) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "else ");
                        list_end_loc = new ArrayList<String>();
                        list_end_loc.add("OFFICE");
                        list_end_loc.add("HOME");
                        list_end_loc.add("FEEDER POINT");
                        list_end_loc.add("SELECT END LOCATION");
                        final int listsize_end_loc = list_end_loc.size() - 1;
                        Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length " + listsize_end_loc + list_end_loc);
                        adapter_end_loc = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_end_loc) {
                            @Override
                            public int getCount() {
                                return (listsize_end_loc); // Truncate the list
                            }
                        };
                        adapter_end_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        s_end_loc.setAdapter(adapter_end_loc);
                        s_end_loc.setSelection(listsize_end_loc); // Hidden item to appear in the spinner
//		        	s_end_loc.setSelection(0);
//		        	s_end_loc_val=s_end_loc.getAdapter().getItem(0).toString();
                        s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());
                    } else if ((SESSION_End_Location != null || SESSION_End_Location != "" || !(SESSION_End_Location.equals("null")) || !SESSION_End_Location.equals("") || SESSION_End_Location != "null") && (SESSION_End_dt != null || SESSION_End_dt != "" || !(SESSION_End_dt.equals("null")) || !SESSION_End_dt.equals("") || SESSION_End_dt != "null") && (SESSION_END_trip_km != null || SESSION_END_trip_km != "" || !(SESSION_END_trip_km.equals("null")) || !SESSION_END_trip_km.equals("") || SESSION_END_trip_km != "null")) {
//		 			int postn=s_end_loc.getAdapter().getItem(0);
//		 			s_end_loc.getAdapter().
//		 			int postn=Integer.valueOf(SESSION_End_Location);
                        list_end_loc = new ArrayList<String>();
                        list_end_loc.add("OFFICE");
                        list_end_loc.add("HOME");
                        list_end_loc.add("FEEDER POINT");
                        list_end_loc.add("SELECT END LOCATION");
                        final int listsize_end_loc = list_end_loc.size() - 1;
                        Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length " + listsize_end_loc + list_end_loc);
                        adapter_end_loc = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_end_loc) {
                            @Override
                            public int getCount() {
                                return (listsize_end_loc); // Truncate the list
                            }
                        };
                        adapter_end_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        s_end_loc.setAdapter(adapter_end_loc);
                        int spinnerPosition = adapter_end_loc.getPosition(SESSION_End_Location);
//		 			s_end_loc.setSelection(SESSION_End_Location);
                        Crashlytics.log(android.util.Log.ERROR, TAG, "adapter_end_loc pos " + spinnerPosition + "enddt" + SESSION_End_dt + "tripkm " + SESSION_END_trip_km);
                        end_tme.setText(SESSION_End_dt);
//		 			total_km_val.setText(SESSION_END_trip_km +"KM");
                        s_end_loc.setSelection(spinnerPosition);
                        s_end_loc_val = s_end_loc.getAdapter().getItem(spinnerPosition).toString();
                        strt_cliked = false;
                        tiny.putBoolean("FLAG_START", strt_cliked);
                        s_strt_loc.setEnabled(true);
//		 			s_strt_loc_val=s_strt_loc.getAdapter().getItem(strt_loc_position).toString();

                        /********************commented on 22APR2015*********************************/
//		 			s_strt_loc.setSelection(0);
//		 			s_strt_loc_val=s_strt_loc.getAdapter().getItem(0).toString();
                        /********************commented on 22APR2015*********************************/

                        s_trsprt.setEnabled(true);

                        /********************commented on 22APR2015*********************************/
//					 s_trsprt.setSelection(0);
//					 s_trsport_val=s_trsprt.getAdapter().getItem(0).toString();
                        /********************commented on 22APR2015*********************************/

                        /********************Added on 22APR2015*********************************/
                        list = new ArrayList<String>();
                        list.add("OFFICE");
                        list.add("HOME");
                        list.add("FEEDER POINT");
                        list.add("SELECT START LOCATION");
                        final int listsize = list.size() - 1;
                        Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length " + listsize + list);
                        adapter_strt_loc = new ArrayAdapter<String>(Start_End_TripMainActivity.this, android.R.layout.simple_spinner_item, list) {
                            @Override
                            public int getCount() {
                                return (listsize); // Truncate the list
                            }
                        };
                        adapter_strt_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        s_strt_loc.setAdapter(adapter_strt_loc);
                        s_strt_loc.setSelection(listsize); // Hidden item to appear in the spinner
                        s_strt_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_strt_loc());

                        /***********select transport***********************/
                        list_trsprt = new ArrayList<String>();
                        list_trsprt.add("BIKER");
                        list_trsprt.add("NON-BIKER");
                        list_trsprt.add("SELECT TRANSPORT");
                        final int listsize_trsprt = list_trsprt.size() - 1;
                        Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner listsize_trsprt " + listsize_trsprt + list_trsprt);
                        adapter_trsprt = new ArrayAdapter<String>(Start_End_TripMainActivity.this, android.R.layout.simple_spinner_item, list_trsprt) {
                            @Override
                            public int getCount() {
                                return (listsize_trsprt); // Truncate the list
                            }
                        };
                        adapter_trsprt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        s_trsprt.setAdapter(adapter_trsprt);
                        s_trsprt.setSelection(listsize_trsprt); // Hidden item to appear in the spinner
                        s_trsprt.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_trsport());
                        /******************************************************/
                        btn_strt_trip.setEnabled(true);
                        s_end_loc.setEnabled(false);
                        btn_endtrip.setEnabled(false);
                        btn_endtrip.setTextColor(Color.WHITE);
                    }
                    btn_endtrip.setEnabled(true);
                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Start_End_TripMainActivity SESSION_FLAG_START==true" + e.getMessage());
                }
            }
//        btn_endtrip.setEnabled(false);
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Start_End_TripMainActivity oncreate" + e.getMessage());
        }
    }

    public int getIndex(Spinner spinner, String myString) {

        int index = 1;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).equals(myString)) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "index value " + index + myString);
                index = i;
            }
        }
        return index;

    }


    public void toggleMenu(View v) {
        finish();
        /*if(FORM_TYPE_VAL !=null && FORM_TYPE_VAL !="" && FORM_TYPE_VAL.equals("FORM_HOME"))
		{
//	    		Toast.makeText(AssignedMultipleDelActivity.this,"Record Saved succesfully", Toast.LENGTH_LONG).show();
			Intent goToNextActivity = new Intent(getApplicationContext(), HomeMainActivity.class);
			startActivity(goToNextActivity);
		}

		else if(FORM_TYPE_VAL !=null && FORM_TYPE_VAL !="" && FORM_TYPE_VAL.equals("FORM_DEL"))
		{
		pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		editor = pref.edit();
		editor.clear();
		editor.putString("position", String.valueOf(0));
		// editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
		editor.commit();
		Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
		startActivity(homeActivity);
		}
		else if(FORM_TYPE_VAL !=null && FORM_TYPE_VAL !="" && FORM_TYPE_VAL.equals("FORM_UNDEL"))
		{
			pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
			editor = pref.edit();
			editor.clear();
			editor.putString("position", String.valueOf(0));
			// editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
			editor.commit();
			Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
			startActivity(homeActivity);
		}
		else if(FORM_TYPE_VAL !=null && FORM_TYPE_VAL !="" && FORM_TYPE_VAL.equals("FORM_PICKUP_SCAN"))
		{
	    	pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
			editor = pref.edit();
			editor.clear();
			editor.putString("position", String.valueOf(1));
			// editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
			editor.commit();
			Intent homeActivity = new Intent(Start_End_TripMainActivity.this, AssignedListMainActivity.class);
			startActivity(homeActivity);
		}
		else if(FORM_TYPE_VAL !=null && FORM_TYPE_VAL !="" && FORM_TYPE_VAL.equals("FORM_PICKUP_CANCEL"))
		{
			pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
			editor = pref.edit();
			editor.clear();
			editor.putString("position", String.valueOf(1));
			// editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
			editor.commit();
			Intent homeActivity = new Intent(Start_End_TripMainActivity.this, AssignedListMainActivity.class);
			startActivity(homeActivity);
		}
		else if(FORM_TYPE_VAL !=null && FORM_TYPE_VAL !="" && FORM_TYPE_VAL.equals("FORM_INCOMPLETE_PICKUP_SCAN"))
		{
			pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
			editor = pref.edit();
			editor.clear();
			editor.putString("position", String.valueOf(1));
			// editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
			editor.commit();
			Intent homeActivity = new Intent(Start_End_TripMainActivity.this, AssignmentInCompleteStatus.class);
			startActivity(homeActivity);
		}

		else if(FORM_TYPE_VAL !=null && FORM_TYPE_VAL !="" && FORM_TYPE_VAL.equals("FORM_COMPLETE_PICKUP_SCAN"))
		{
			pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
			editor = pref.edit();
			editor.clear();
			editor.putString("position", String.valueOf(1));
			// editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
			editor.commit();
			Intent homeActivity = new Intent(Start_End_TripMainActivity.this, AssignmentCompleteStatus.class);
			startActivity(homeActivity);
		}
		else
		{
			Intent homeActivity = new Intent(Start_End_TripMainActivity.this, HomeMainActivity.class);
			startActivity(homeActivity);
		}*/
    }


    public void onclk_noti(View v) {
        Intent homeActivity = new Intent(this, Notification.class);
        startActivity(homeActivity);
    }

    public void onclk_trip(View v) {
        Intent homeActivity = new Intent(this, Start_End_TripMainActivity.class);
        startActivity(homeActivity);
    }

    public void Onstrt_trip(View v) {
        try {
            SESSION_FLAG_START = tiny.getBoolean("FLAG_START");
            Crashlytics.log(android.util.Log.ERROR, TAG, "SESSION_FLAG_START " + SESSION_FLAG_START + s_strt_loc_val + s_trsport_val);
//		if(SESSION_FLAG_START==false)
//		{
//			 int strt_loc_position=tiny.getInt("strt_loc_position");
//			 int transposrt_position=tiny.getInt("position_trnspt");
//			Crashlytics.log(android.util.Log.ERROR,TAG,"SESSION_FLAG_START " + SESSION_FLAG_START + s_strt_loc_val + s_trsport_val+ "\n" + "strt_loc_position" + strt_loc_position + "transposrt_position "+ transposrt_position);
//			 s_strt_loc.setSelection(strt_loc_position);
//			 s_trsprt.setSelection(transposrt_position);
//			 btn_strt_trip.setEnabled(true);
//			 btn_endtrip.setEnabled(true);
//			 /** Create an ArrayAdapter using the string array and a default spinner transport layout*/
//		        ArrayAdapter<CharSequence> adapter_trsprt = ArrayAdapter.createFromResource(this,
//		                R.array.transportMode, android.R.layout.simple_spinner_item);
//		        /** Specify the layout to use when the list of choices appears*/
//		        adapter_trsprt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		        /** Apply the adapter to the spinner*/         
//		        
//		        s_trsprt.setAdapter(adapter_trsprt);
            s_trsprt.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_trsport());
//		        
//		        /** Create an ArrayAdapter using the string array and a default spinner start_location layout*/
//		        ArrayAdapter<CharSequence> adapter_strt_loc = ArrayAdapter.createFromResource(this,
//		                R.array.start_loc, android.R.layout.simple_spinner_item);
//		        /** Specify the layout to use when the list of choices appears*/
//		        adapter_strt_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		        /** Apply the adapter to the spinner*/         
//		        
//		        s_strt_loc.setAdapter(adapter_strt_loc);
            s_strt_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_strt_loc());
//		        
//		        /** Create an ArrayAdapter using the string array and a default spinner end location layout*/
//		        ArrayAdapter<CharSequence> adapter_end_loc = ArrayAdapter.createFromResource(this,
//		                R.array.end_loc, android.R.layout.simple_spinner_item);
//		        /** Specify the layout to use when the list of choices appears*/
//		        adapter_end_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		        /** Apply the adapter to the spinner*/         
//		        
//		        s_end_loc.setAdapter(adapter_end_loc);
//		        s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());
//			 int transposrt_positionn=tiny.getInt("position_trnspt");
//			 String aa= s_trsprt.getAdapter().getItem(transposrt_positionn).toString();
//			 Crashlytics.log(android.util.Log.ERROR,TAG,"false val " + s_trsport_val + aa);
//			 tiny.putString("transport_val",aa);
//		}
            if (s_strt_loc_val.equals("SELECT START LOCATION")) {
                Toast.makeText(Start_End_TripMainActivity.this,
                        "Please Select the start location !!", Toast.LENGTH_SHORT)
                        .show();
            } else if (s_trsport_val.equals("SELECT TRANSPORT")) {

                Toast.makeText(Start_End_TripMainActivity.this,
                        "Please Select the transport !!", Toast.LENGTH_SHORT)
                        .show();
            } else if (!s_trsport_val.equals("SELECT TRANSPORT")) {
                session.setStartTrip(true);
                int tripNo = session.getEndTripCount();
                tripNo = tripNo + 1;
                session.setEndTripCount(tripNo);
                session.removeDistancePref();
                total_km_val.setText("0 KM");
                Calendar ci = Calendar.getInstance();
                String month = ci.get(Calendar.MONTH) + 1 + "";
                if (month.length() < 2) {
                    month = "0" + month;
                }
                String date = ci.get(Calendar.DAY_OF_MONTH) + "";
                if (date.length() < 2) {
                    date = "0" + date;
                }
                strt_CiDateTime = "" + date + "-" + month + "-" +
                        ci.get(Calendar.YEAR) + " " +
                        ci.get(Calendar.HOUR_OF_DAY) + ":" +
                        ci.get(Calendar.MINUTE) + ":" +
                        ci.get(Calendar.SECOND);
                tiny.putString("strted_time_val", strt_CiDateTime);
//	       String currentdate= CiDateTime.substring(0, 10); 

                final Calendar c = Calendar.getInstance();
                String hour = c.get(Calendar.HOUR_OF_DAY) + "";
                if (hour.length() < 2) {
                    hour = "0" + hour;
                }
                String min = c.get(Calendar.MINUTE) + "";
                if (min.length() < 2) {
                    min = "0" + min;
                }
                String sec = c.get(Calendar.SECOND) + "";
                if (sec.length() < 2) {
                    sec = "0" + sec;
                }
			/*currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
			        (c.get(Calendar.MINUTE)) + ":" +
			        (c.get(Calendar.SECOND));*/
                currentTime = hour + ":" +
                        min + ":" +
                        sec;
                Log.w("TIME:", String.valueOf(currentTime) + currentTime);
                strt_time_val = String.valueOf(currentTime);
//		strt_tme.setText(CiDateTime);
                strt_tme.setText(currentTime);
//		tiny.putString("strt_time_val", strt_CiDateTime);
                tiny.putString("strt_time_val", currentTime);
                //putBoolean("FLAG_START", strt_cliked);

                final String strt_tme_val = strt_tme.getText().toString().trim();
                Crashlytics.log(android.util.Log.ERROR, TAG, "strt_tme_val " + strt_tme_val);
                db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                ContentValues cv = new ContentValues();
                cv.put("T_usr_id", session_USER_NAME);
                tiny.putString("transport_val", s_trsport_val);
                cv.put("T_Mode_transport", s_trsport_val);
                tiny.putString("srt_loc_val", s_strt_loc_val);
                cv.put("T_Start_Location", s_strt_loc_val);
                cv.put("T_Cust_Acc_NO", session_CUST_ACC_CODE);
                cv.put("IMEI", device_id);
                cv.put("D_Start_TM", session_CURRENT_DT + " " + strt_time_val);
                try {
                    db.insertOrThrow("Trip_User_MASTER", null, cv);
                } catch (SQLiteConstraintException ex) {
                    //what ever you want to do
                }
                strt_cliked = true;
                tiny.putBoolean("FLAG_START", strt_cliked);
                btn_strt_trip.setEnabled(false);
                btn_strt_trip.setTextColor(Color.WHITE);
//		btn_strt_trip.setBackground(null);
                s_strt_loc.setEnabled(false);
                s_trsprt.setEnabled(false);
                s_end_loc.setEnabled(true);
//		s_end_loc.setSelection(0);
//		s_end_loc_val=s_end_loc.getAdapter().getItem(0).toString();
                list_end_loc = new ArrayList<String>();
                list_end_loc.add("OFFICE");
                list_end_loc.add("HOME");
                list_end_loc.add("FEEDER POINT");
                list_end_loc.add("SELECT END LOCATION");
                final int listsize_end_loc = list_end_loc.size() - 1;
                Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length " + listsize_end_loc + list_end_loc);
                adapter_end_loc = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_end_loc) {
                    @Override
                    public int getCount() {
                        return (listsize_end_loc); // Truncate the list
                    }
                };
                adapter_end_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                s_end_loc.setAdapter(adapter_end_loc);
                s_end_loc.setSelection(listsize_end_loc); // Hidden item to appear in the spinner
//    	s_end_loc.setSelection(0);
//    	s_end_loc_val=s_end_loc.getAdapter().getItem(0).toString();
                s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());
                end_tme.setText("");
                hrs_val.setText("00");
                min_val.setText("00");
                sec_val.setText("00");
                strt_trip.setVisibility(View.INVISIBLE);
                end_trip.setVisibility(View.VISIBLE);
                btn_endtrip.setEnabled(true);

            }
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Onend_trip" + e.getMessage());
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
    }

    public void Onend_trip(View v) {
        try {

            SESSION_FLAG_START = tiny.getBoolean("FLAG_START");
//		 s_end_loc_val="";
            Crashlytics.log(android.util.Log.ERROR, TAG, "SESSION_FLAG_START Onend_trip" + SESSION_FLAG_START + s_strt_loc_val + s_trsport_val + s_end_loc_val);
            if (SESSION_FLAG_START == true && ((s_strt_loc_val == null || s_strt_loc_val.equals("null")) || (s_trsport_val == null || s_trsport_val.equals("null")))) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "here");
                int strt_loc_position = tiny.getInt("strt_loc_position");
                int transposrt_position = tiny.getInt("position_trnspt");
                s_strt_loc_val = s_strt_loc.getAdapter().getItem(strt_loc_position).toString();
                s_trsport_val = s_trsprt.getAdapter().getItem(transposrt_position).toString();
                Toast.makeText(Start_End_TripMainActivity.this,
                        "Please Select the end location !!", Toast.LENGTH_SHORT)
                        .show();
                s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());
                SharedPreferences pref = getSharedPreferences("ScanPref", Context.MODE_PRIVATE);
                if (pref != null) {
                    pref.edit().putBoolean("startTrip", false).commit();
                    boolean status = pref.getBoolean("status", false);

                    if (status) {
                        pref.edit().remove("status").commit();
                    }
                }
            } else if (SESSION_FLAG_START == true && ((s_strt_loc_val == null || s_strt_loc_val.equals("null")) && (s_trsport_val == null || s_trsport_val.equals("null"))) && s_end_loc_val.equals("SELECT END LOCATION")) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "here insde");
                Toast.makeText(Start_End_TripMainActivity.this,
                        "Please Select the end location !!", Toast.LENGTH_SHORT)
                        .show();
                s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());
            }
//		 else if(SESSION_FLAG_START==true &&((s_strt_loc_val!=null || !s_strt_loc_val.equals("null")) && (s_trsport_val!=null || !s_trsport_val.equals("null")) && (s_end_loc_val!=null || !s_end_loc_val.equals("null"))))
//		 {
////			 Crashlytics.log(android.util.Log.ERROR,TAG,"flag is true and loc is null");
////			 s_end_loc.setEnabled(true);
////				s_end_loc.setSelection(0);
////				s_end_loc_val=s_end_loc.getAdapter().getItem(0).toString();
//				s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());
//////			  int strt_loc_position=tiny.getInt("strt_loc_position");
//////			  int transposrt_position=tiny.getInt("position_trnspt");
//////			  s_strt_loc_val=s_strt_loc.getAdapter().getItem(strt_loc_position).toString();
//////			  s_trsport_val=s_trsprt.getAdapter().getItem(transposrt_position).toString();
//		 }
            else if (SESSION_FLAG_START == false) {
                Toast.makeText(Start_End_TripMainActivity.this,
                        "Please start the trip !!", Toast.LENGTH_SHORT)
                        .show();
            } else if (s_strt_loc_val.equals("SELECT START LOCATION")) {
                Toast.makeText(Start_End_TripMainActivity.this,
                        "Please Select the start location !!", Toast.LENGTH_SHORT)
                        .show();
            } else if (s_trsport_val.equals("SELECT TRANSPORT")) {

                Toast.makeText(Start_End_TripMainActivity.this,
                        "Please Select the transport !!", Toast.LENGTH_SHORT)
                        .show();
            }
//		 else if(s_end_loc_val == null || s_end_loc_val=="" || s_end_loc_val.equals("null") || s_end_loc_val.equals("") || s_end_loc_val =="null" || s_end_loc_val.equals("SELECT END LOCATION"))

            else if (SESSION_FLAG_START == true && ((s_strt_loc_val != null || !s_strt_loc_val.equals("null")) && (s_trsport_val != null || !s_trsport_val.equals("null"))) && s_end_loc_val.equals("SELECT END LOCATION")) {

                Toast.makeText(Start_End_TripMainActivity.this,
                        "Please Select the end location !!", Toast.LENGTH_SHORT)
                        .show();
                s_end_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_end_loc());
            } else if (SESSION_FLAG_START == true && ((s_strt_loc_val != null || !s_strt_loc_val.equals("null")) && (s_trsport_val != null || !s_trsport_val.equals("null"))) && !s_end_loc_val.equals("SELECT END LOCATION")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

                // Setting Dialog Title
                alertDialog.setTitle("Alert");

                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want to end the trip ?");

                // On pressing Settings button
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
                            session.setStartTrip(false);
                            session.setStartService(false);
                            SESSION_TRANSPORT = tiny.getString("transport_val");
                            Crashlytics.log(android.util.Log.ERROR, TAG, "insde end trip SESSION_TRANSPORT " + SESSION_TRANSPORT);
                            if (SESSION_TRANSPORT != null && SESSION_TRANSPORT != "" && SESSION_TRANSPORT.equals("BIKER")) {
                                strt_cliked = false;
                                tiny.putBoolean("FLAG_START", strt_cliked);
                                btn_endtrip.setEnabled(false);
                                btn_endtrip.setTextColor(Color.WHITE);
                                s_end_loc.setEnabled(false);
                                btn_strt_trip.setEnabled(true);
                                s_strt_loc.setEnabled(true);
                                s_trsprt.setEnabled(true);
                                //	        		 SESSION_TRANSPORT=tiny.getString("transport_val");
                                //	        	        Crashlytics.log(android.util.Log.ERROR,TAG,"insde end trip SESSION_TRANSPORT " + SESSION_TRANSPORT);
                                tiny.putString("transport_val", "SELECT TRANSPORT");

//			        		s_strt_loc.setSelection(0);
                                list = new ArrayList<String>();
                                list.add("OFFICE");
                                list.add("HOME");
                                list.add("FEEDER POINT");
                                list.add("SELECT START LOCATION");
                                final int listsize = list.size() - 1;
                                Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length " + listsize + list);
                                adapter_strt_loc = new ArrayAdapter<String>(Start_End_TripMainActivity.this, android.R.layout.simple_spinner_item, list) {
                                    @Override
                                    public int getCount() {
                                        return (listsize); // Truncate the list
                                    }
                                };
                                adapter_strt_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                s_strt_loc.setAdapter(adapter_strt_loc);
                                s_strt_loc.setSelection(listsize); // Hidden item to appear in the spinner
                                s_strt_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_strt_loc());

                                /***********select transport***********************/
                                list_trsprt = new ArrayList<String>();
                                list_trsprt.add("BIKER");
                                list_trsprt.add("NON-BIKER");
                                list_trsprt.add("SELECT TRANSPORT");
                                final int listsize_trsprt = list_trsprt.size() - 1;
                                Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner listsize_trsprt " + listsize_trsprt + list_trsprt);
                                adapter_trsprt = new ArrayAdapter<String>(Start_End_TripMainActivity.this, android.R.layout.simple_spinner_item, list_trsprt) {
                                    @Override
                                    public int getCount() {
                                        return (listsize_trsprt); // Truncate the list
                                    }
                                };
                                adapter_trsprt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                s_trsprt.setAdapter(adapter_trsprt);
                                s_trsprt.setSelection(listsize_trsprt); // Hidden item to appear in the spinner
                                s_trsprt.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_trsport());
//					        s_trsprt.setSelection(0);
//			        		s_strt_loc_val=s_strt_loc.getAdapter().getItem(3).toString();
//						 	s_strt_loc_val=s_strt_loc.getAdapter();
//			  			    s_trsport_val=s_trsprt.getAdapter().getItem(3).toString();

                                Calendar ci = Calendar.getInstance();
                                String month = ci.get(Calendar.MONTH) + 1 + "";
                                if (month.length() < 2) {
                                    month = "0" + month;
                                }
                                String date = ci.get(Calendar.DAY_OF_MONTH) + "";
                                if (date.length() < 2) {
                                    date = "0" + date;
                                }
                                end_CiDateTime = "" + date + "-" + month + "-" +
                                        ci.get(Calendar.YEAR) + " " +
                                        ci.get(Calendar.HOUR_OF_DAY) + ":" +
                                        ci.get(Calendar.MINUTE) + ":" +
                                        ci.get(Calendar.SECOND);

                                final Calendar cl = Calendar.getInstance();
                                String hour = cl.get(Calendar.HOUR_OF_DAY) + "";
                                if (hour.length() < 2) {
                                    hour = "0" + hour;
                                }
                                String min = cl.get(Calendar.MINUTE) + "";
                                if (min.length() < 2) {
                                    min = "0" + min;
                                }
                                String sec = cl.get(Calendar.SECOND) + "";
                                if (sec.length() < 2) {
                                    sec = "0" + sec;
                                }
			/*currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
			        (c.get(Calendar.MINUTE)) + ":" +
			        (c.get(Calendar.SECOND));*/
                                endcurTime = hour + ":" +
                                        min + ":" +
                                        sec;
			        		/*endcurTime = (cl.get(Calendar.HOUR_OF_DAY)) + ":" +
			                        (cl.get(Calendar.MINUTE)) + ":" +
			                        (cl.get(Calendar.SECOND));*/
                                Log.w("TIME:", String.valueOf(endcurTime) + endcurTime);
                                db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                ContentValues cv = new ContentValues();
                                //	        		cv.put("D_End_TM",String.valueOf(currentTime));
                                try {
                                    //	        		    db.insertOrThrow("Trip_User_MASTER", null, cv);
                                    Cursor cr = db.rawQuery("UPDATE Trip_User_MASTER SET D_End_TM='" + session_CURRENT_DT + " " + endcurTime + "',T_End_Location='" + s_end_loc_val + "'  WHERE IMEI='" + device_id + "' AND T_usr_id='" + session_USER_NAME + "' ", null);
                                    while (cr.moveToNext()) {

                                    }
                                    cr.close();
                                } catch (SQLiteConstraintException ex) {
                                    //what ever you want to do
                                }
                                db.close();
//			        		end_tme.setText(CiDateTime);
                                end_tme.setText(endcurTime);
                                Float dis = session.getDistance();
                                session.removeDistancePref();
                                session.setDistance(0);
                                session.setText("");
                                if (dis > 0) {
                                    total_km_val.setText(String.valueOf(round(dis / 1000, 3)) + "KM");
                                }
                                try {
//								DateTimeUtils obj = new DateTimeUtils();
                                    SimpleDateFormat simpleDateFormat =
                                            new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

                                    try {
                                        SESSION_STARTED_TIME = tiny.getString("strted_time_val");
//									System.out.println("print start end time" + strt_CiDateTime +  end_CiDateTime +"SESSION_STARTED_TIME" + SESSION_STARTED_TIME);
//									Date date1 = simpleDateFormat.parse(strt_CiDateTime);
                                        Date date1 = simpleDateFormat.parse(SESSION_STARTED_TIME);
                                        Date date2 = simpleDateFormat.parse(end_CiDateTime);

                                        printDifference(date1, date2);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                } catch (Exception e) {
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Date Error" + e.getMessage());
                                }

                                strt_trip.setVisibility(View.VISIBLE);
                                end_trip.setVisibility(View.INVISIBLE);

                                //check if any previous task is running, if so then cancel it
                                //it can be cancelled if it is not in FINISHED state
			              /*  if (tripTask != null && tripTask.getStatus() != AsyncTask.Status.FINISHED)
			                	tripTask.cancel(true);
			                tripTask = new LongOperation(); //every time create new object, as AsynTask will only be executed one time.
			                tripTask.execute();*/
                            } else if (SESSION_TRANSPORT != null && SESSION_TRANSPORT != "" && SESSION_TRANSPORT.equals("NON-BIKER")) {
                                strt_cliked = false;
                                tiny.putBoolean("FLAG_START", strt_cliked);
                                btn_endtrip.setEnabled(false);
                                btn_endtrip.setTextColor(Color.WHITE);
                                s_end_loc.setEnabled(false);
                                btn_strt_trip.setEnabled(true);
                                s_strt_loc.setEnabled(true);
                                s_trsprt.setEnabled(true);
                                //	        		 SESSION_TRANSPORT=tiny.getString("transport_val");
                                //	        	        Crashlytics.log(android.util.Log.ERROR,TAG,"insde end trip SESSION_TRANSPORT " + SESSION_TRANSPORT);
                                tiny.putString("transport_val", "SELECT TRANSPORT");

//			        		s_strt_loc.setSelection(0);
                                list = new ArrayList<String>();
                                list.add("OFFICE");
                                list.add("HOME");
                                list.add("FEEDER POINT");
                                list.add("SELECT START LOCATION");
                                final int listsize = list.size() - 1;
                                Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner length " + listsize + list);
                                adapter_strt_loc = new ArrayAdapter<String>(Start_End_TripMainActivity.this, android.R.layout.simple_spinner_item, list) {
                                    @Override
                                    public int getCount() {
                                        return (listsize); // Truncate the list
                                    }
                                };
                                adapter_strt_loc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                s_strt_loc.setAdapter(adapter_strt_loc);
                                s_strt_loc.setSelection(listsize); // Hidden item to appear in the spinner
                                s_strt_loc.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_strt_loc());

                                /***********select transport***********************/
                                list_trsprt = new ArrayList<String>();
                                list_trsprt.add("BIKER");
                                list_trsprt.add("NON-BIKER");
                                list_trsprt.add("SELECT TRANSPORT");
                                final int listsize_trsprt = list_trsprt.size() - 1;
                                Crashlytics.log(android.util.Log.ERROR, TAG, "print spinner listsize_trsprt " + listsize_trsprt + list_trsprt);
                                adapter_trsprt = new ArrayAdapter<String>(Start_End_TripMainActivity.this, android.R.layout.simple_spinner_item, list_trsprt) {
                                    @Override
                                    public int getCount() {
                                        return (listsize_trsprt); // Truncate the list
                                    }
                                };
                                adapter_trsprt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                s_trsprt.setAdapter(adapter_trsprt);
                                s_trsprt.setSelection(listsize_trsprt); // Hidden item to appear in the spinner
                                s_trsprt.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_trsport());
//					        s_trsprt.setSelection(0);
//			        		s_strt_loc_val=s_strt_loc.getAdapter().getItem(3).toString();
//						 	s_strt_loc_val=s_strt_loc.getAdapter();
//			  			    s_trsport_val=s_trsprt.getAdapter().getItem(3).toString();

                                Calendar ci = Calendar.getInstance();
                                String month = ci.get(Calendar.MONTH) + 1 + "";
                                if (month.length() < 2) {
                                    month = "0" + month;
                                }
                                String date = ci.get(Calendar.DAY_OF_MONTH) + "";
                                if (date.length() < 2) {
                                    date = "0" + date;
                                }
                                end_CiDateTime = "" + date + "-" + month + "-" +
                                        ci.get(Calendar.YEAR) + " " +
                                        ci.get(Calendar.HOUR_OF_DAY) + ":" +
                                        ci.get(Calendar.MINUTE) + ":" +
                                        ci.get(Calendar.SECOND);

                                final Calendar cl = Calendar.getInstance();
                                String hour = cl.get(Calendar.HOUR_OF_DAY) + "";
                                if (hour.length() < 2) {
                                    hour = "0" + hour;
                                }
                                String min = cl.get(Calendar.MINUTE) + "";
                                if (min.length() < 2) {
                                    min = "0" + min;
                                }
                                String sec = cl.get(Calendar.SECOND) + "";
                                if (sec.length() < 2) {
                                    sec = "0" + sec;
                                }
			/*currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
			        (c.get(Calendar.MINUTE)) + ":" +
			        (c.get(Calendar.SECOND));*/
                                endcurTime = hour + ":" +
                                        min + ":" +
                                        sec;
								/*endcurTime = (cl.get(Calendar.HOUR_OF_DAY)) + ":" +
										(cl.get(Calendar.MINUTE)) + ":" +
										(cl.get(Calendar.SECOND));*/
                                Log.w("TIME:", String.valueOf(endcurTime) + endcurTime);
                                db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                ContentValues cv = new ContentValues();
                                //	        		cv.put("D_End_TM",String.valueOf(currentTime));
                                try {
                                    //	        		    db.insertOrThrow("Trip_User_MASTER", null, cv);
                                    Cursor cr = db.rawQuery("UPDATE Trip_User_MASTER SET D_End_TM='" + session_CURRENT_DT + " " + endcurTime + "',T_End_Location='" + s_end_loc_val + "'  WHERE IMEI='" + device_id + "' AND T_usr_id='" + session_USER_NAME + "' ", null);
                                    while (cr.moveToNext()) {

                                    }
                                    cr.close();
                                } catch (SQLiteConstraintException ex) {
                                    //what ever you want to do
                                }
                                db.close();
//			        		end_tme.setText(CiDateTime);
                                end_tme.setText(endcurTime);
                                Float dis = session.getDistance();
                                session.removeDistancePref();
                                session.setDistance(0);
                                session.setText("");
                                if (dis > 0) {
                                    total_km_val.setText(String.valueOf(round(dis / 1000, 3)) + "KM");
                                }
                                try {
//								DateTimeUtils obj = new DateTimeUtils();
                                    SimpleDateFormat simpleDateFormat =
                                            new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

                                    try {

                                        SESSION_STARTED_TIME = tiny.getString("strted_time_val");
//									System.out.println("print start end time" + strt_CiDateTime +  end_CiDateTime +"SESSION_STARTED_TIME" + SESSION_STARTED_TIME);
//									Date date1 = simpleDateFormat.parse(strt_CiDateTime);
                                        Date date1 = simpleDateFormat.parse(SESSION_STARTED_TIME);
                                        Date date2 = simpleDateFormat.parse(end_CiDateTime);

                                        printDifference(date1, date2);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                } catch (Exception e) {
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Date Error" + e.getMessage());
                                }

                                strt_trip.setVisibility(View.VISIBLE);
                                end_trip.setVisibility(View.INVISIBLE);

                                //check if any previous task is running, if so then cancel it
                                //it can be cancelled if it is not in FINISHED state
								/*if (tripTask != null && tripTask.getStatus() != AsyncTask.Status.FINISHED)
									tripTask.cancel(true);
								tripTask = new LongOperation(); //every time create new object, as AsynTask will only be executed one time.
								tripTask.execute();*/
                            }
						/*}
	        	        else if(SESSION_TRANSPORT !=null && SESSION_TRANSPORT!="" && SESSION_TRANSPORT.equals("NON-BIKER"))
	        	        {
	        	        	Intent goToNextActivity = new Intent(getApplicationContext(), Conveyance_dynamicActivity.class);
	         			    goToNextActivity.putExtra(END_TRIP_USERID,session_USER_NUMERIC_ID);
//	         			    tiny.putString("FORM_TYPE","MULTIPLE_DELIVERED");//COMMENTED ON 11MAR2015
//	         			   int end_loc_position=tiny.getInt("end_loc_position");
	         			   
	        	        	goToNextActivity.putExtra("End_Location",s_end_loc_val);
//	        	        	goToNextActivity.putExtra("End_Location",end_loc_position);
	         			   Crashlytics.log(android.util.Log.ERROR,TAG,"print end loc position "+ s_end_loc_val);
	         			   goToNextActivity.putExtra("FORM_TYPE","TRIP");
	        	    		startActivity(goToNextActivity);
	        	        }*/
                        } catch (Exception e) {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "Error Exception Onend_trip " + e.getMessage());
                        }
//	            	dialog.cancel();
                        //            	onClickLogOut();

//	        		else
//	        		{
//	        		Intent intent = new Intent(Customer_locateMainActivity.this, Conveyance_CaptureMainActivity.class);
//	        		intent.putExtra(CUST_LOCATE_ASSGN_ID, locte_assgn_id);
//	        		startActivity(intent);
//	        		}
                    }
                });

                // on pressing cancel button
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
                stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
//			}
            }
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Onend_trip" + e.getMessage());
        }
    }


    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    public void printDifference(Date startDate, Date endDate) {
        try {
            //milliseconds
            long different = endDate.getTime() - startDate.getTime();

            Crashlytics.log(android.util.Log.ERROR, TAG, "startDate : " + startDate);
            Crashlytics.log(android.util.Log.ERROR, TAG, "endDate : " + endDate);
            Crashlytics.log(android.util.Log.ERROR, TAG, "different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            if (elapsedHours < 10) {
//				elapsedHours   = "0"+elapsedHours;
                hrs_val.setText("0" + String.valueOf(elapsedHours) + ":");
            } else {
                hrs_val.setText(String.valueOf(elapsedHours) + ":");
            }
            if (elapsedMinutes < 10) {
//				minutes = "0"+minutes;
                min_val.setText("0" + String.valueOf(elapsedMinutes));
            } else {
                min_val.setText(String.valueOf(elapsedMinutes));
            }
            if (elapsedSeconds < 10) {
//				seconds = "0"+seconds;
                sec_val.setText("0" + String.valueOf(elapsedSeconds));
            } else {
                sec_val.setText(String.valueOf(elapsedSeconds));
            }
//			hrs_val.setText(String.valueOf(elapsedHours)+":"+"\n"+"hrs");
//			hrs_val.setText(String.valueOf(elapsedHours)+":");
//			min_val.setText(String.valueOf(elapsedMinutes));
//			sec_val.setText(String.valueOf(elapsedSeconds));
            System.out.printf(
                    "%d days, %d hours, %d minutes, %d seconds%n",
                    elapsedDays,
                    elapsedHours, elapsedMinutes, elapsedSeconds);
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "printDifference error" + e.getMessage());
        }
    }


//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.start__end__trip_main, menu);
//		return true;
//	}

    /**
     * Listener Implementation of Spinner For Selecting Transport Room
     */

//    public static class Listener_Of_Selecting_Room_Spinner_trsport implements OnItemSelectedListener 
    public class Listener_Of_Selecting_Room_Spinner_trsport implements OnItemSelectedListener {
        public String RoomType_trsport;

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            /** By using this you can get the position of item which you
             have selected from the dropdown     **/
            try {
                RoomType_trsport = (parent.getItemAtPosition(pos)).toString();
                tiny.putInt("position_trnspt", pos);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                ((TextView) parent.getChildAt(0)).setTextSize(12);
//        		 adapter_trsprt.remove(s_trsprt.getAdapter().getItem(0).toString());
                s_trsport_val = RoomType_trsport;


                Crashlytics.log(android.util.Log.ERROR, TAG, "selection type" + pos + s_trsport_val + RoomType_trsport);
            } catch (Exception e) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Start_End_TripMainActivity Listener_Of_Selecting_Room_Spinner " + e.getMessage());
//	    		TOMUndeliveredActivity callhmepge = new TOMUndeliveredActivity();
//        		callhmepge.onClickGoToHomePage();
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error Start_End_TripMainActivity UnsatisfiedLinkError ");
//        		TOMUndeliveredActivity callhmepge = new TOMUndeliveredActivity();
//        		callhmepge.onClickGoToHomePage();
            }
        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }

    }

    /**
     * Listener Implementation of Spinner For Selecting strat location Room
     */

//    public static class Listener_Of_Selecting_Room_Spinner_strt_loc implements OnItemSelectedListener
    public class Listener_Of_Selecting_Room_Spinner_strt_loc implements OnItemSelectedListener {
        public String RoomType_strt_loc;

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            /** By using this you can get the position of item which you
             have selected from the dropdown     **/
            try {
                RoomType_strt_loc = (parent.getItemAtPosition(pos)).toString();
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                ((TextView) parent.getChildAt(0)).setTextSize(12);
                tiny.putInt("strt_loc_position", pos);//putString("position",pos);
                s_strt_loc_val = RoomType_strt_loc;
                Crashlytics.log(android.util.Log.ERROR, TAG, "on start loc change " + s_strt_loc_val + pos);

            } catch (Exception e) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception TOMUndeliveredActivity Listener_Of_Selecting_Room_Spinner " + e.getMessage());
//	    		TOMUndeliveredActivity callhmepge = new TOMUndeliveredActivity();
//        		callhmepge.onClickGoToHomePage();
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMUndeliveredActivity UnsatisfiedLinkError ");
//        		TOMUndeliveredActivity callhmepge = new TOMUndeliveredActivity();
//        		callhmepge.onClickGoToHomePage();
            }
        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }

    }

    /**
     * Listener Implementation of Spinner For Selecting end location Room
     */

//    public static class Listener_Of_Selecting_Room_Spinner_end_loc implements OnItemSelectedListener 
    public class Listener_Of_Selecting_Room_Spinner_end_loc implements OnItemSelectedListener {
        //        static
        public String RoomType_end_loc;

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            /** By using this you can get the position of item which you
             have selected from the dropdown     **/
            try {
                RoomType_end_loc = (parent.getItemAtPosition(pos)).toString();
                tiny.putInt("end_loc_position", pos);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                ((TextView) parent.getChildAt(0)).setTextSize(12);
//        		s_end_loc_val="";
                s_end_loc_val = RoomType_end_loc;
                Crashlytics.log(android.util.Log.ERROR, TAG, "on end loc change " + s_end_loc_val + pos);
            } catch (Exception e) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception TOMUndeliveredActivity Listener_Of_Selecting_Room_Spinner " + e.getMessage());
//	    		TOMUndeliveredActivity callhmepge = new TOMUndeliveredActivity();
//        		callhmepge.onClickGoToHomePage();
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMUndeliveredActivity UnsatisfiedLinkError ");
//        		TOMUndeliveredActivity callhmepge = new TOMUndeliveredActivity();
//        		callhmepge.onClickGoToHomePage();
            }
        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }

    } 
    
/* // Class which extends AsyncTask class
    private class LongOperation  extends AsyncTask<String, Void, Void> 
    {
    
   	   ProgressDialog Dialog = new ProgressDialog(Start_End_TripMainActivity.this);
//   	   private String Content;
//   	   private String Error = null;  
//   	   
		 @Override
		 protected void onPreExecute() 
		 {
		       // NOTE: You can call UI Element here.            
		       //UI Element
		
//			btnstepin.setEnabled(false);
//			btnstepin.setBackgroundColor(Color.GRAY);
//			progresstextview.setVisibility(View.VISIBLE); 
			Dialog.setMessage("Please wait..");
			 Dialog.show();
			 Dialog.setCancelable(false);
		 }
   	 
   	 // Call after onPreExecute method
     @Override
	protected Void doInBackground(String... urls) {
    	 
    	
  		try
  		{ 			
  		/*/

    /**
     * When ever you want to check Internet Status in your application call isConnectingToInternet() function and it will return true or false
     ***//*
	   		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

	   		Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false
	   		// get Internet status
	             isInternetPresent = cd.isConnectingToInternet();

	             // check for Internet status
	         if (isInternetPresent) 
	         {
  			db_trip_complete=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
  			c=db_trip_complete.rawQuery("SELECT T_usr_id,T_Mode_transport,T_Cust_Acc_NO,IMEI,D_Start_TM,TOL_KM,D_End_TM,T_Start_Location,T_End_Location from Trip_User_MASTER where T_usr_id='" + session_USER_NAME  + "' and C_Sync IS NOT 1",null);
  			while(c.moveToNext())
  			{
  				trsport_mode=c.getString(c.getColumnIndex("T_Mode_transport"));
  				strt_tme_val=c.getString(c.getColumnIndex("D_Start_TM"));
  				strt_tme_val=strt_tme_val.replace(" ","%20");
  				end_tme_val=c.getString(c.getColumnIndex("D_End_TM"));
  				end_tme_val=end_tme_val.replace(" ","%20");
  				total_km=c.getString(c.getColumnIndex("TOL_KM"));
  				imei_val=c.getString(c.getColumnIndex("IMEI"));
  				strt_loc_val=c.getString(c.getColumnIndex("T_Start_Location"));
  				strt_loc_val=strt_loc_val.replace(" ","%20");
  				end_loc_val=c.getString(c.getColumnIndex("T_End_Location"));
  				end_loc_val=end_loc_val.replace(" ","%20");
  				Crashlytics.log(android.util.Log.ERROR,TAG,"values from db" + trsport_mode+strt_tme_val + end_tme_val +total_km +imei_val + strt_loc_val);
  			
  			String entityString_trip = "MODE_OF_TRANSPORT="+ trsport_mode +"&DSTART_TM="+ strt_tme_val +"&DEND_TM="+ end_tme_val +"&TOL_KM="+ total_km +"&USER_ID="+ session_USER_NAME +"&CUST_ACC_NO="+ session_CUST_ACC_CODE +"&IMEI="+ imei_val +"&ST_LOC="+ strt_loc_val +"&EN_LOC="+ end_loc_val ;

    		Crashlytics.log(android.util.Log.ERROR,TAG,"session_user_id "+ session_user_id +entityString_trip);
//		 	outscn = 1;
		 	int  reqId = 101;
			HttpHandler handler = new HttpHandler(URL_trip_dtls + "/" + METHOD_NAME_trip_dtls + "?" + entityString_trip,null, null, reqId);
			handler.addHttpLisner(Start_End_TripMainActivity.this);								    						
			handler.sendRequest();				
			
			// get user data from session
	        HashMap<String, String> resp_Dts_scan_out = session.gethttpsrespDetails();
	         
	        // session_outscan_resp
	        session_trip_resp = resp_Dts_scan_out.get(SessionManager.KEY_RESPONSE);
	        Crashlytics.log(android.util.Log.ERROR,TAG,"session_trip_resp " +session_trip_resp);
	        if(session_trip_resp!=null && session_trip_resp != "")
	        {

	    	Parse_trip_respXmlResponse tripListParser = new Parse_trip_respXmlResponse(session_trip_resp,getApplicationContext());
			try {
				tripListParser.parse();

			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 if(tripListParser.getparsedtripList().isEmpty())
				{
				 if(session_DB_PATH != null && session_DB_PWD != null)
         		  {
	            	   	db_pickup_complete_resp1=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
	            	   	trip_km=Parse_trip_respXmlResponse.total_kmVal;
	            	   	imei_value=Parse_trip_respXmlResponse.ImeiVal;
	            	   	Crashlytics.log(android.util.Log.ERROR,TAG,"trip_km  imei_value" + trip_km + imei_value);
	            	   	try
	            	   	{
	            	   	if(trip_km !=null || trip_km !="" && imei_value !=null || imei_value !="")
	            	   	{
	            	   		runOnUiThread(new Runnable() {
		      				    @Override
		      				        public void run() 
		      				         {
		      				    		total_km_val.setText(trip_km+"KM");
		      				         }
	            	   		});
	            	   	c_pickup_complete_sync_resp1=db_pickup_complete_resp1.rawQuery("UPDATE Trip_User_MASTER SET C_Sync=0 WHERE IMEI='" + imei_value + "'",null);
//	            	   			new String[]{""+complete_resp_awbid});
	          		 	while(c_pickup_complete_sync_resp1.moveToNext()){}
//	          		 	c_syn_updater.close();
			          		if (c_pickup_complete_sync_resp1 != null) 
			              {
			          		c_pickup_complete_sync_resp1.close();
			              }
	            	   	}
	            	   	}
	            	   	catch(Exception e)
	            	   	{
	            	   		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception trip_km is empty" + e.getMessage());
	            	   	}
	            	  db_pickup_complete_resp1.close();
         		  }
				}
			 else if(!(tripListParser.getparsedtripList().isEmpty()))
               {
				 if(session_DB_PATH != null && session_DB_PWD != null)
        		  {
	            	   	db_pickup_complete_resp2=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
	            	   	trip_km=Parse_trip_respXmlResponse.total_kmVal;
	            	   	imei_value=Parse_trip_respXmlResponse.ImeiVal;
	            		Crashlytics.log(android.util.Log.ERROR,TAG,"trip_km  imei_value" + trip_km + imei_value);
	            	   	try
	            	   	{
	            	   	if(trip_km !=null || trip_km !="" && imei_value !=null || imei_value !="")
	            	   	{
	            	   		runOnUiThread(new Runnable() {
		      				    @Override
		      				        public void run() 
		      				         {
		      				    		total_km_val.setText(trip_km+"KM");
		      				         }
	            	   		});
	            	   	c_pickup_complete_sync_resp2=db_pickup_complete_resp2.rawQuery("UPDATE Trip_User_MASTER SET C_Sync=1 WHERE IMEI='" + imei_value + "'",null);
//	            	   			new String[]{""+complete_resp_awbid});
	          		 	while(c_pickup_complete_sync_resp2.moveToNext()){}
//	          		 	c_syn_updater.close();
			          		if (c_pickup_complete_sync_resp2 != null) 
			              {
			          			c_pickup_complete_sync_resp2.close();
			              }
	            	   	}
	            	   	}
	            	   	catch(Exception e)
	            	   	{
	            	   		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception trip_km is empty" + e.getMessage());
	            	   	}
	            	   	db_pickup_complete_resp2.close();
        		  }
               }
	        }
  			}
  			c.close();
  			db_trip_complete.close();
  			
  		}
  		else
  		{
  			//no internet connection
  			Toast.makeText(Start_End_TripMainActivity.this,"Please Check Your Mobile Internet Connection ", Toast.LENGTH_LONG).show();
  		}
     }
  		catch(Exception e)
  		{
  			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception doInBackground" + e.getMessage());
  		}
  		finally
  		{
  			if(c !=null)
  			{
  				c.close();
  			}
  			if(db_trip_complete != null)
  			{
  				db_trip_complete.close();
  			}
  		}
  		 return null;
     }
      
      
      @Override
 	protected void onPostExecute(Void unused) 
      {
          // NOTE: You can call UI Element here.
     	 try
     	 {
			 runOnUiThread(new Runnable() {
				 @Override
				 public void run()
				 {
					 if (Dialog!=null) {
						 if (Dialog.isShowing()) {
							 Dialog.dismiss();
						 }
					 }
				 }
			 });
     	 }
     	catch(Exception e)
  		{
     		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception onPostExecute" + e.getMessage());
  		}
      }
    }*/
    @Override
    public void onBackPressed() {
    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
            stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AssignedMultipleActivity onDestroy " + e.getMessage());
//	       stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
//	    	stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
//	    	android.os.Process.killProcess(android.os.Process.myPid());
//	    	System.exit(0);

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleActivity UnsatisfiedLinkError ");
//    		stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
//        	stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
//        	android.os.Process.killProcess(android.os.Process.myPid());
//        	System.exit(0);
//        	        	    			finish();
        }
    }

    //**********************modification for menu options(Home & Logout)**************************
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater() ;
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_home:
                onClickGoToHomePage();
                return true;
            case R.id.menu_log_out:
                onClickLogOut();
                return true;
            case android.R.id.home:
//            	Toast.makeText(this, "home pressed", Toast.LENGTH_LONG).show();
            	Intent hmeActivity = new Intent(this, HomeMainActivity.class);
//            	hmeActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(hmeActivity);
            	 return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    private void onClickLogOut() {
        try {
            Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
            logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logOutActivity);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AssignedMultipleActivity onClickLogOut " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleActivity UnsatisfiedLinkError ");
        }
    }

    private void onClickGoToHomePage() {
        try {
            Intent homePageActivity = new Intent(this, HomeMainActivity.class);
//        homePageActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homePageActivity);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AssignedMultipleActivity onClickGoToHomePage " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleActivity UnsatisfiedLinkError ");
        }
    }

    public static double roundToDecimals(double d, int c) {
        int temp = (int) (d * Math.pow(10, c));
        return ((double) temp) / Math.pow(10, c);
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

}

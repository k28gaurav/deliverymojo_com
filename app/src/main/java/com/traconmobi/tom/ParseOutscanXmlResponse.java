/*************************************************************/

/**********CREATED BY : ASHWINI NAIK ************************/
/********CREATED DATE : 15/03/2013 **************************/
/***********PURPOSE   : CLASS FILE TO PARSE THE OUTSCAN WEBSERVICE VALUES******/

/************************************************************/
package com.traconmobi.tom;

import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
//import net.sqlcipher.Cursor;
//import net.sqlcipher.database.SQLiteConstraintException;
//import net.sqlcipher.database.SQLiteException;

//import android.database.sqlite.SQLiteDatabase;
/*********Replaced by ***********/
//import net.sqlcipher.database.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class ParseOutscanXmlResponse
{

	private static ArrayList<HashMap<String, String>> parsedOutscanList;
	public String outscan_awbnoValue=null;
	public String outscan_awbidValue=null;
	public String assignment_typeValue=null;
	public String outscan_dttmeValue=null;
	public String consignee_nmeValue=null;
	public String addr1Value=null;
	public String addr2Value=null;
	public String city_detlValue=null;
	public String pincode_detlValue=null;
	public String consignee_Contact_Number1=null;
	public String iscod1 =null;
	public String cod_Amount1=null;
	public String outscan_locValue=null;
	public String outscan_uidValue=null;
	public String latitude_Value=null;
	public String longitude_Value=null;
	public String outscan_cust_Acc_no_value=null;
	public String outscan_shipper_acc_num_Value=null;
	public String outscan_pickup_NoOfPcs_Value=null;
	public String outscan_shipper_name_Value=null;
	public String outscan_shipper_ph_no_Value=null;
	public String outscan_shipper_email_Value=null;
	public String outscan_Pickup_Time_Value=null;
	private static int countShipment = 0;
	public String outscan_awb_id,assgn_type,outscan_date_time,outscan_date,consignee_name,addr_1,addr_2,cty,pncode,contact_num,iscodvalue,cod_Amountvalue,outscan_locvalue,outscan_uidvalue,outscan_cust_Acc_Num,outscan_shipper_Acc_Num,outscan_pickup_numofpieces,outscan_shipper_nme,outscan_shipper_Num,outscan_shipper_emailid,outscan_Pickup_Time_val;
	double amt;
	boolean flag = false;
	protected SQLiteDatabase db,db_insert_route,db_outscn;
	Cursor c,cur;
	private StringReader xmlReader;
	private final String startxmltag = "ResDO";
	@SuppressWarnings("unused")
	public String  status;
	private final String startLists = "arrAWBStatus";
	private final String starttagList = "ASSIGNMENT";
	private final String outscan_awbid = "ASSIGNMENTID";
	private final String outscan_awbno = "ASSIGNMENTNO";
	private final String assignment_type = "ASSIGNMENTTYPE";
	private final String outscan_dttme = "ASSIGNEDDATETIME";
	private final String consignee_nme = "CONSIGNEENAME";
	private final String addr1 = "CONSIGNEEADDRESS1";
	private final String addr2 = "CONSIGNEEADDRESS2";
	private final String city_detl = "CONSIGNEECITY";
	private final String pincode_detl = "CONSIGNEEPINCODE";
	private final String consignee_Contact_Number ="CONSIGNEECONTACTNO";
	private final String iscod ="ISCOLLECTABLE";
	private final String cod_Amount="COLLECTABLEAMOUNT";
	private final String outscan_loc="ASSIGNEDLOCID";
	private final String outscan_uid="ASSIGNEDUSERID";
	private final String latitude = "LATITUDE";
	private final String longitude = "LONGITUDE";
	private final String outscan_cust_Acc_no="COMPANYID";
	private final String outscan_shipper_acc_num="SHIPPERACCNO";
	private final String outscan_pickup_NoOfPcs="NOOFPIECES";
	private final String outscan_Pickup_Time="EXPECTEDDELIVERY_DATETIME";
	private final String outscan_shipper_name="SHIPPERNAME";
	private final String outscan_shipper_ph_no="SHIPPERCONTACTNO";
	private final String outscan_shipper_email="SHIPPEREMAIL";
//	@SuppressWarnings("unused")


	public static String awbno;
	Cursor c_show_assigned_cnt;
	public static int assigned_count,size;
	public static String u_status;

	// Session Manager Class
	SessionManager session;
	public String session_DB_PATH,session_DB_PWD,session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME;
	// variable to hold context
	public Context context;
	public String TAG="ParseOutscanXmlResponse";

	public ParseOutscanXmlResponse(String xml,Context context)
	{
		try
		{
			xmlReader = new StringReader(xml);
			this.context=context;
			//Intializing Fabric
			Fabric.with(context, new Crashlytics());
			// Session class instance
			session = new SessionManager(context);

			/**
			 * GETTING SESSION VALUES
			 * */

			// get AuthenticateDb data from session
			HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

			// DB_PATH
			session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

			// DB_PWD
			session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);

			// get user data from session
			HashMap<String, String> login_Dts = session.getLoginDetails();

			// Userid
			session_user_id = login_Dts.get(SessionManager.KEY_UID);

			// pwd
			session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


			// get user data from session
			HashMap<String, String> user = session.getUserDetails();

			// session_USER_LOC
			session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);

			// session_USER_NUMERIC_ID
			session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

			// session_CURRENT_DT
			session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);

			// session_CUST_ACC_CODE
			session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);

			// session_USER_NAME
			session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);

		}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception ParseOutscanXmlResponse" + e.getMessage());
		}
	}

	public void parse() throws XmlPullParserException, IOException
	{
		try
		{
			TreeMap<String, String> outscanListObj = null;
			XmlPullParser parser = Xml.newPullParser();
			parser.setInput(xmlReader);
			int eventType = parser.getEventType();
			outscanListObj = new TreeMap<String, String>();
			parsedOutscanList = new ArrayList<HashMap<String, String>>();
			while (eventType != XmlPullParser.END_DOCUMENT)
			{
				String xmlNodeName = parser.getName();
				if (XmlPullParser.START_TAG == eventType)
				{
					xmlNodeName = parser.getName();
					if (xmlNodeName.equalsIgnoreCase(outscan_awbid))
					{
						outscan_awbidValue = parser.nextText().toString();
//			Log.d("outscan_awbidValue",outscan_awbidValue);
						outscanListObj.put("outscan_awbid", outscan_awbidValue);

					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_awbno))
					{
						outscan_awbnoValue = parser.nextText().toString();
						outscanListObj.put("outscan_awbno", outscan_awbnoValue);
//			Log.d("AWB_NUM",outscan_awbnoValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(assignment_type))
					{
						assignment_typeValue = parser.nextText().toString();
						outscanListObj.put("assignment_type",assignment_typeValue);
//		Log.d("assignment_typeValue",assignment_typeValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_dttme))
					{
						outscan_dttmeValue = parser.nextText().toString();
						outscanListObj.put("outscan_dttme",outscan_dttmeValue);
//			Log.d("OUTSCAN_DTTME",outscan_dttmeValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(consignee_nme))
					{
						consignee_nmeValue = parser.nextText().toString();
						outscanListObj.put("consignee_nme", consignee_nmeValue);
//			Log.d("Consignee_Name",consignee_nmeValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(addr1))
					{
						addr1Value = parser.nextText().toString();
						outscanListObj.put("addr1", addr1Value);
//			Log.d("Address_Line_1",addr1Value);
					}

					else if (xmlNodeName.equalsIgnoreCase(addr2))
					{
						addr2Value = parser.nextText().toString();
						outscanListObj.put("addr2", addr2Value);
//			Log.d("Address_Line_2",addr2Value);
					}

					else if (xmlNodeName.equalsIgnoreCase(city_detl))
					{
						city_detlValue = parser.nextText();
						outscanListObj.put("city_detl", city_detlValue);
//			Log.d("City",city_detlValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(pincode_detl))
					{
						pincode_detlValue = parser.nextText();
						outscanListObj.put("pincode_detl", pincode_detlValue);
//			Log.d("Pincode",pincode_detlValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(consignee_Contact_Number))
					{
						consignee_Contact_Number1 = parser.nextText().toString();
						outscanListObj.put("consignee_Contact_Number", consignee_Contact_Number1);
//			Log.d("consignee_Contact_Number",consignee_Contact_Number1);
					}

					else if (xmlNodeName.equalsIgnoreCase(iscod))
					{
						iscod1 = parser.nextText().toString();
						outscanListObj.put("iscod",iscod1);
//			Log.d("iscod1",iscod1);
					}

					else if (xmlNodeName.equalsIgnoreCase(cod_Amount))
					{
						cod_Amount1 = parser.nextText().toString();
						outscanListObj.put("cod_Amount",cod_Amount1);
//			Log.d("cod_Amount1",cod_Amount1);
					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_loc))
					{
						outscan_locValue = parser.nextText().toString();
						outscanListObj.put("outscan_loc", outscan_locValue);
//			Log.d("outscan_loc",outscan_locValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_uid))
					{
						outscan_uidValue = parser.nextText().toString();
						outscanListObj.put("outscan_uid", outscan_uidValue);
//			Log.d("outscan_uid",outscan_uidValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(latitude))					{

						latitude_Value = parser.nextText().toString();
						outscanListObj.put("latitude", latitude_Value);
//			Log.d("latitude_Value",latitude_Value);
					}

					else if (xmlNodeName.equalsIgnoreCase(longitude))
					{
						longitude_Value = parser.nextText();
						outscanListObj.put("longitude", longitude_Value);
//			Log.d("longitude_Value",longitude_Value);
					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_cust_Acc_no))
					{
						outscan_cust_Acc_no_value = parser.nextText();
						outscanListObj.put("outscan_cust_Acc_no", outscan_cust_Acc_no_value);
//		Log.d("outscan_cust_Acc_no_value",outscan_cust_Acc_no_value);
					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_shipper_acc_num))
					{
						outscan_shipper_acc_num_Value = parser.nextText();
						outscanListObj.put("outscan_shipper_acc_num", outscan_shipper_acc_num_Value);
//		Log.d("City",city_detlValue);
					}
					else if (xmlNodeName.equalsIgnoreCase(outscan_pickup_NoOfPcs))
					{
						outscan_pickup_NoOfPcs_Value = parser.nextText();
						outscanListObj.put("outscan_pickup_NoOfPcs", outscan_pickup_NoOfPcs_Value);
//		Log.d("outscan_pickup_NoOfPcs ",outscan_pickup_NoOfPcs_Value);
					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_Pickup_Time))
					{
						outscan_Pickup_Time_Value = parser.nextText();
						outscanListObj.put("outscan_Pickup_Time", outscan_Pickup_Time_Value);
//		Log.d("outscan_pickup_NoOfPcs ",outscan_pickup_NoOfPcs_Value);
					}
					else if (xmlNodeName.equalsIgnoreCase(outscan_shipper_name))
					{
						outscan_shipper_name_Value = parser.nextText();
						outscanListObj.put("outscan_shipper_name", outscan_shipper_name_Value);
//		Log.d("City",city_detlValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_shipper_ph_no))
					{
						outscan_shipper_ph_no_Value = parser.nextText();
						outscanListObj.put("outscan_shipper_ph_no", outscan_shipper_ph_no_Value);
//		Log.d("City",city_detlValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(outscan_shipper_email))
					{
						outscan_shipper_email_Value = parser.nextText();
						outscanListObj.put("outscan_shipper_email", outscan_shipper_email_Value);
//		Log.d("City",city_detlValue);
					}
				} else if (XmlPullParser.END_TAG == eventType)
				{
					if (xmlNodeName.equalsIgnoreCase(starttagList))
					{

						parsedOutscanList.add(new HashMap<String, String>(outscanListObj));

						for (Entry<String, String> entry : outscanListObj.entrySet())
						{
							outscan_awb_id =outscanListObj.get("outscan_awbid");
//			    Log.i("awbid", outscan_awbid);
							awbno = outscanListObj.get("outscan_awbno");
							assgn_type =outscanListObj.get("assignment_type");
							outscan_date_time=outscanListObj.get("outscan_dttme");
							outscan_date=outscan_date_time.substring(0, 10);
							consignee_name=outscanListObj.get("consignee_nme");
							addr_1=outscanListObj.get("addr1");
							addr_2=outscanListObj.get("addr2");
							cty=outscanListObj.get("city_detl");
							pncode=outscanListObj.get("pincode_detl");
							contact_num=outscanListObj.get("consignee_Contact_Number");
							iscodvalue=outscanListObj.get("iscod");
							cod_Amountvalue=outscanListObj.get("cod_Amount");
							amt =Double.parseDouble(cod_Amountvalue);
							DecimalFormat dec = new DecimalFormat("###.##");

//			    Crashlytics.log(android.util.Log.ERROR,TAG,dec.format(amt));
//			    System.out.printf("%.2f", amt);
							NumberFormat formatter = new DecimalFormat("###.00");
							String str = formatter.format(amt);
//			    Crashlytics.log(android.util.Log.ERROR,TAG,"show the amount in decimal" + str);
							outscan_locvalue=outscanListObj.get("outscan_loc");
							outscan_uidvalue=outscanListObj.get("outscan_uid");
							String u_lat=outscanListObj.get("latitude");
							String u_long=outscanListObj.get("longitude");
							outscan_cust_Acc_Num=outscanListObj.get("outscan_cust_Acc_no");
							outscan_shipper_Acc_Num=outscanListObj.get("outscan_shipper_acc_num");
							outscan_pickup_numofpieces=outscanListObj.get("outscan_pickup_NoOfPcs");
							outscan_shipper_nme=outscanListObj.get("outscan_shipper_name");
							outscan_shipper_Num=outscanListObj.get("outscan_shipper_ph_no");
							outscan_shipper_emailid=outscanListObj.get("outscan_shipper_email");
							outscan_Pickup_Time_val=outscanListObj.get("outscan_Pickup_Time");
							/**code to insert values in TOM_Del Table**/

							ContentValues cv =new ContentValues();
							cv.put("T_Assignment_Id",outscan_awb_id);
							cv.put("T_Assignment_Number",outscan_awbnoValue);
							cv.put("T_Assignment_Type", assgn_type);
							cv.put("D_OutScan_Date",outscan_date);
							cv.put("T_Consignee_Name",consignee_name);
							cv.put("T_Address_Line1",addr_1);
							cv.put("T_Address_Line2",addr_2);
							cv.put("T_City",cty);
							cv.put("T_Contact_number",contact_num);
							cv.put("T_Pincode",pncode);
							cv.put("B_is_Amountable",iscodvalue);
							cv.put("F_Amount",amt);

							if( Double.parseDouble(cod_Amountvalue)== 0)
							{
								status="PREPAID";
//								Crashlytics.log(android.util.Log.ERROR,TAG,"check amt status"+ outscan_awbnoValue+"PREPAID");
							}
							if( Double.parseDouble(cod_Amountvalue) > 0)
							{
								status="COD";
//								Crashlytics.log(android.util.Log.ERROR,TAG,"check amt status"+outscan_awbnoValue +"COD");
							}
							cv.put("T_Amount_Type",status);
							cv.put("T_Out_Scan_Location",outscan_locvalue);
							cv.put("T_OUT_Scan_U_ID",outscan_uidvalue);
							cv.put("T_Cust_Acc_NO",outscan_cust_Acc_Num);
							cv.put("T_Shipper_Acc_No",outscan_shipper_Acc_Num);
							cv.put("T_NoOfPcs",outscan_pickup_numofpieces);
							cv.put("T_Pickup_Time",outscan_Pickup_Time_val);
							cv.put("T_Shipper_name",outscan_shipper_nme);
							cv.put("T_Shipper_contact_number",outscan_shipper_Num);
							cv.put("T_Shipper_email",outscan_shipper_emailid);


//			    Crashlytics.log(android.util.Log.ERROR,TAG,"cv outsde" + cv + awbno + outscan_uidvalue +  session_CUST_ACC_CODE + outscan_awbnoValue);
							try{

								if(session_DB_PATH != null && session_DB_PWD != null)
								{
//				    	db_outscn=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
									db_outscn=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
									//db_outscn.beginTransaction();
//				    	Crashlytics.log(android.util.Log.ERROR,TAG,"awbno outsde " + awbno + outscan_uidvalue +  session_CUST_ACC_CODE);

									c= db_outscn.rawQuery("select _id,T_Assignment_Id,T_Assignment_Number,T_Assignment_Type from TOM_Assignments where T_Assignment_Number='"+ outscan_awbnoValue +"' and T_OUT_Scan_U_ID='"+ outscan_uidvalue +"' and T_Cust_Acc_NO='"+ session_CUST_ACC_CODE +"'", null);
//				    	Crashlytics.log(android.util.Log.ERROR,TAG,"awbno insde " + awbno + outscan_uidvalue + session_CUST_ACC_CODE);
									while(c.moveToNext())
									{
										String a_no=c.getString(c.getColumnIndex("T_Assignment_Type"));
									}
									int count = c.getCount();

									if(count == 0)
									{
										flag = true;
//				    		Crashlytics.log(android.util.Log.ERROR,TAG,"count 0 ");

//				    	db_outscn.insertWithOnConflict("TOM_Assignments", BaseColumns._ID, cv, SQLiteDatabase.CONFLICT_IGNORE);
										db_outscn.insertWithOnConflict("TOM_Assignments", BaseColumns._ID, cv, SQLiteDatabase.CONFLICT_REPLACE);/////////dsosajdoisaoiduoiusadoisaud
//				    	Crashlytics.log(android.util.Log.ERROR,TAG,"cv " + cv + awbno + outscan_uidvalue +  session_CUST_ACC_CODE + outscan_awbnoValue);
//				    	db_outscn.insertWithOnConflict("TBL_route_lan_lon", BaseColumns._ID, cv1, SQLiteDatabase.CONFLICT_IGNORE);//commented on 31jul2015
									}
									else if(count >0)
									{

//				    		Crashlytics.log(android.util.Log.ERROR,TAG,"count > 0 " + outscan_pickup_NoOfPcs_Value);

//										cur = db_outscn.rawQuery("UPDATE TOM_Assignments SET T_Consignee_Name='"+ consignee_nmeValue +"', WHERE T_Assignment_Number='"+ awbno +"'", null);
										String status="TRUE";
										String strFilter = "T_Assignment_Number='"+ awbno +"' and T_OUT_Scan_U_ID='"+ outscan_uidvalue +"' and T_Cust_Acc_NO='"+ session_CUST_ACC_CODE +"' and B_is_Completed IS NOT '"+ status +"' ";
										cur = db_outscn.rawQuery("SELECT _id,T_Consignee_Name,T_Address_Line1 FROM TOM_Assignments WHERE T_Assignment_Number='"+ awbno +"' and B_is_Completed IS NOT '"+ status +"' ", null);
										while(cur.moveToNext())
										{
											ContentValues cv1 =new ContentValues();
											cv1.put("T_Consignee_Name",consignee_name);
											cv1.put("T_Address_Line1",addr_1);
											cv1.put("T_Address_Line2",addr_2);
											cv1.put("T_City",cty);
											cv1.put("T_Contact_number",contact_num);
											cv1.put("T_Pincode",pncode);
											cv1.put("B_is_Amountable", iscodvalue);
											cv1.put("F_Amount",amt);

											if( Double.parseDouble(cod_Amountvalue)== 0)
											{
												status="PREPAID";
//								Crashlytics.log(android.util.Log.ERROR,TAG,"check amt status"+ outscan_awbnoValue+"PREPAID");
											}
											if( Double.parseDouble(cod_Amountvalue) > 0)
											{
												status="COD";
//								Crashlytics.log(android.util.Log.ERROR,TAG,"check amt status"+outscan_awbnoValue +"COD");
											}
											cv1.put("T_Amount_Type",status);
											cv1.put("T_Shipper_Acc_No",outscan_shipper_Acc_Num);
											cv1.put("T_NoOfPcs",outscan_pickup_numofpieces);
											cv1.put("T_Pickup_Time",outscan_Pickup_Time_val);
											cv1.put("T_Shipper_name",outscan_shipper_nme);
											cv1.put("T_Shipper_contact_number",outscan_shipper_Num);
											cv1.put("T_Shipper_email",outscan_shipper_emailid);
											db_outscn.update("TOM_Assignments", cv1, strFilter, null);
										}
//				    		if(cur !=null)
//				    		{
//				    			cur.close();
//				    		}
									}
									//db_outscn.endTransaction();
									cur.close();
									db_outscn.close();
								}
								else
								{
									Crashlytics.log(android.util.Log.ERROR, TAG, "Error outscan parser session_DB_PATH is null ");
								}
							}

							catch(SQLiteConstraintException ex){

								//what ever you want to do
								Crashlytics.log(android.util.Log.ERROR, TAG, "outscan parser SQLiteConstraintException " + ex.getMessage());
							}
							catch (SQLiteException e)
							{
								// TODO: handle exception
								e.printStackTrace();
								Crashlytics.log(android.util.Log.ERROR, TAG, "outscan parser SQLiteException" + e.getMessage());
							}
							catch (Exception e)
							{
								// TODO: handle exception
								e.printStackTrace();
								Crashlytics.log(android.util.Log.ERROR, TAG, "Exception outscan parser " + e.getMessage());
							}
							finally
							{
								// This block contains statements that are ALWAYS executed
								// after leaving the try clause, regardless of whether we leave it:
								// 1) normally after reaching the bottom of the block;
								// 2) because of a break, continue, or return statement;
								// 3) with an exception handled by a catch clause above; or
								// 4) with an uncaught exception that has not been handled.
								// If the try clause calls System.exit(), however, the interpreter
								// exits before the finally clause can be run.
								if(cur !=null && !cur.isClosed())
								{
									cur.close();
								}
								if(c !=null && !c.isClosed())
								{
									c.close();
								}
								if(db_outscn !=null && db_outscn.isOpen())
								{
									//db_outscn.endTransaction();
									db_outscn.close();
								}
							}

						}
						outscanListObj.clear();
					}

				}
				eventType = parser.next();
			}
			if(flag) {
				Log.e(TAG, "Check flag" + flag);
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception outscan Treemap " + e.getMessage());
		}
		finally
		{
			// This block contains statements that are ALWAYS executed
			// after leaving the try clause, regardless of whether we leave it:
			// 1) normally after reaching the bottom of the block;
			// 2) because of a break, continue, or return statement;
			// 3) with an exception handled by a catch clause above; or
			// 4) with an uncaught exception that has not been handled.
			// If the try clause calls System.exit(), however, the interpreter
			// exits before the finally clause can be run.
//		    	if(db_outscn !=null)
//		    	{
//		    		db_outscn.close();
//		    	}

		}
	}

	@SuppressWarnings("unused")
	private String readText(XmlPullParser parser) throws IOException,XmlPullParserException
	{

		String result = "";

		try
		{
			if (parser.next() == XmlPullParser.TEXT)
			{

				result = parser.getText();

//		    Log.d("wat is the result",result);

				parser.nextTag();

			}
		}
		catch(Exception e)
		{
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception outscan readText" + e.getMessage());
		}
		return result;

	}

	public ArrayList getparsedOutscanList() {

		return parsedOutscanList;


	}

}
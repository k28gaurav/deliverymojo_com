package com.traconmobi.tom.http;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.Parse_CompleteXmlResponse;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by kumargaurav on 5/24/16.
 */

public class OkHttpHandlerPost {
    String response1 = null;
    String url;
    String status;
    RequestBody parameter;
    String TAG = "OkHttpHandler for Delivery";
    String session_DB_PATH;
    String session_DB_PWD;
    String del_awb_id, undel_awb_id;
    Cursor c_del_complete_sync_resp1, c_del_complete_sync_resp2,c_undel_sync_resp1,c_undel_sync_resp2,c_undelsync_resp1,c_undelsync_resp2;
    protected SQLiteDatabase db_del_complete_resp1,db_del_complete_resp2, db_undel_resp1, db_undel_resp2;

    public OkHttpHandlerPost(RequestBody formdata, String url, String status, String dbpath, String dbpwd) {
        parameter = formdata;
        this.url = url;
        this.status = status;
        session_DB_PATH = dbpath;
        session_DB_PWD = dbpwd;
    }

    public String sendRequest () {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .post(parameter)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if(response.isSuccessful()) {
                        response1 = response.body().string();
                        Log.e(TAG , "sending "  + status + "sync request....\n" + response1);

                        if(response1 != null) {
                            if(status.equals("DEL")) {
                                Parse_CompleteXmlResponse Complete_parser = new Parse_CompleteXmlResponse(response1);
                                try {
                                    Complete_parser.parse();
                                } catch (XmlPullParserException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                /*...........................................  */
                                if (Complete_parser.getparsedCompleteList().isEmpty()) {
                                    try {
                                        if (session_DB_PATH != null && session_DB_PWD != null) {
                                            //    				            		   db_del_complete_resp1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                            db_del_complete_resp1 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                            del_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                            String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                            if (del_awb_id != null || del_awb_id != "") {
                                                c_del_complete_sync_resp1 = db_del_complete_resp1.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status +
                                                        "' WHERE T_Assignment_Id = ?", new String[]{"" + del_awb_id});
                                                while (c_del_complete_sync_resp1.moveToNext()) {
                                                }
                                                //    					          		 	if(c_del_complete_sync_resp1 != null)
                                                //    					          		 	{
                                                //    					          		 		c_del_complete_sync_resp1.close();
                                                //    					          		 	}
                                            }
                                            //    					            	   	db_del_complete_resp1.close();
                                        } else {
                                            Crashlytics.log(Log.ERROR, TAG, "error : step4 SampleSchedulingService delsync");
                                            //onClickLogOut();
                                        }
                                    } catch (Exception e) {
                                        Crashlytics.log(Log.ERROR, TAG, "Exception Expense " + e.getMessage());
                                    } finally {
                                        if (c_del_complete_sync_resp1 != null && !c_del_complete_sync_resp1.isClosed()) {
                                            c_del_complete_sync_resp1.close();
                                        }
                                        if (db_del_complete_resp1 != null && db_del_complete_resp1.isOpen()) {
                                            db_del_complete_resp1.close();
                                        }
                                    }
//    				          		 syncdelcnt=cursor.getCount();
                                } else if (!(Complete_parser.getparsedCompleteList().isEmpty())) {
                                    try {
                                        if (session_DB_PATH != null && session_DB_PWD != null) {
                                            //    				            		   db_del_complete_resp2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                            db_del_complete_resp2 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                            del_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                            String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                            //    					            	   Crashlytics.log(android.util.Log.ERROR,TAG,"delsync "+ pod_awb);
                                            if (del_awb_id != null || del_awb_id != "") {
                                                c_del_complete_sync_resp2 = db_del_complete_resp2.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status +
                                                        "'  WHERE T_Assignment_Id = ?", new String[]{"" + del_awb_id});
                                                while (c_del_complete_sync_resp2.moveToNext()) {
                                                }
//	    					            	   if(c_del_complete_sync_resp2 != null)
//	    					            	   {
//	    					            		   c_del_complete_sync_resp2.close();
//	    					            	   }
                                            }
//	    					            	 db_del_complete_resp2.close();
                                        } else {
                                            Crashlytics.log(Log.ERROR, TAG, "error : step5 SampleSchedulingService delsync ");
                                            //onClickLogOut();
                                        }
                                    } catch (Exception e) {
                                        Crashlytics.log(Log.ERROR, TAG, "Exception Expense " + e.getMessage());
                                    } finally {
                                        if (c_del_complete_sync_resp2 != null && !c_del_complete_sync_resp2.isClosed()) {
                                            c_del_complete_sync_resp2.close();
                                        }
                                        if (db_del_complete_resp2 != null && db_del_complete_resp2.isOpen()) {
                                            db_del_complete_resp2.close();
                                        }
                                    }
//    				       		    syncdelcnt=cursor.getCount();
                                }


                            }else if(status.equals("UNDEL")){

                                Parse_CompleteXmlResponse UD_parser = new Parse_CompleteXmlResponse(response1);
                                try {
                                    UD_parser.parse();
                                } catch (XmlPullParserException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }


                                if (UD_parser.getparsedCompleteList().isEmpty()) {
                                    if (session_DB_PATH != null && session_DB_PWD != null) {
                                        try {
//			                	db_del_incomplete_resp1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH,
// session_DB_PWD, null);
                                            db_undel_resp1 = SQLiteDatabase.openDatabase(session_DB_PATH, null,
                                                    SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                            undel_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                            String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                            Crashlytics.log(Log.ERROR, TAG, "print undel_awbid empty" + undel_awb_id);
                                            if (undel_awb_id != null || undel_awb_id != "") {
                                                c_undel_sync_resp1 = db_undel_resp1.rawQuery("UPDATE Tom_Assignments_In_Complete SET C_is_Sync='" +
                                                        sync_status +
                                                        "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//			                			new String[]{""+undel_awbid});
                                                while (c_undel_sync_resp1.moveToNext()) {

                                                }
                                                c_undelsync_resp1 = db_undel_resp1.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status +
                                                        "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//			                			new String[]{""+undel_awbid});
                                                while (c_undelsync_resp1.moveToNext()) {

                                                }
                                            }
//			                	db_undel_resp1.close();
                                        } catch (Exception e) {
                                            Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService undelsync isEmpty Exception" + e.getMessage());
                                        } finally {
                                            if (c_undel_sync_resp1 != null && !(c_undel_sync_resp1.isClosed())) {
                                                c_undel_sync_resp1.close();
                                            }
                                            if (c_undelsync_resp1 != null && !(c_undelsync_resp1.isClosed())) {
                                                c_undelsync_resp1.close();
                                            }
                                            if (db_undel_resp1 != null && db_undel_resp1.isOpen()) {
                                                db_undel_resp1.close();
                                            }
                                        }
                                    } else {
                                        Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService db_undel_resp1 session_DB_PATH is null");
                                        //onClickLogOut();
                                    }
//			        	        syncundelcnt=cr.getCount();
                                } else if (!(UD_parser.getparsedCompleteList().isEmpty())) {
                                    if (session_DB_PATH != null && session_DB_PWD != null) {
                                        try {
//			                		db_del_incomplete_resp2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                            db_undel_resp2 = SQLiteDatabase.openDatabase(session_DB_PATH, null,
                                                    SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                            undel_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                            String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                            Crashlytics.log(Log.ERROR, TAG, "print undel_awb_id not empty" + undel_awb_id);
                                            if (undel_awb_id != null || undel_awb_id != "") {
                                                c_undel_sync_resp2 = db_undel_resp2.rawQuery("UPDATE Tom_Assignments_In_Complete SET C_is_Sync='" +
                                                        sync_status + "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//				                			new String[]{""+undel_awb_id});
                                                while (c_undel_sync_resp2.moveToNext()) {

                                                }
                                                c_undelsync_resp2 = db_undel_resp2.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" +
                                                        sync_status + "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//			                			new String[]{""+undel_awbid});
                                                while (c_undelsync_resp2.moveToNext()) {

                                                }
                                            }
//				                	db_undel_resp2.close();
                                        } catch (Exception e) {
                                            Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService undelsync isnotEmpty Exception" + e.getMessage());
                                        } finally {
                                            if (c_undel_sync_resp2 != null && !(c_undel_sync_resp2.isClosed())) {
                                                c_undel_sync_resp2.close();
                                            }
                                            if (c_undelsync_resp2 != null && !(c_undelsync_resp2.isClosed())) {
                                                c_undelsync_resp2.close();
                                            }
                                            if (db_undel_resp2 != null && db_undel_resp2.isOpen()) {
                                                db_undel_resp2.close();
                                            }
                                        }
                                    } else {
                                        Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService db_undel_resp2 session_DB_PATH is null ");
                                        //onClickLogOut();
                                    }
//			        	        syncundelcnt=cr.getCount();
                                }

                            }
                        }
                    }

                }
            });
        }catch(Exception e) {

        }
        return  response1;
    }
}

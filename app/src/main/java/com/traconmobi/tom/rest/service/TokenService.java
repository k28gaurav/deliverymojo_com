package com.traconmobi.tom.rest.service;

import com.traconmobi.tom.rest.model.TokenParser;

import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kumargaurav on 3/23/16.
 */
public interface TokenService {

    @FormUrlEncoded
    @POST("/User-Details")
    Call<TokenParser> getToken(@FieldMap Map<String, String> names);

}

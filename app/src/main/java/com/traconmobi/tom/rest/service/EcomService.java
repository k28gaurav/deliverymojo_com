package com.traconmobi.tom.rest.service;

import com.traconmobi.tom.rest.model.ApiResponse;
import com.traconmobi.tom.rest.model.Escan;
import com.traconmobi.tom.rest.model.TokenParser;

import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by kumargaurav on 2/6/16.
 */
public interface EcomService {

    @FormUrlEncoded
    @POST("/Assignment-Ecom-Details")
    Call<Escan> getEcomService(@FieldMap Map<String, String> names);
}

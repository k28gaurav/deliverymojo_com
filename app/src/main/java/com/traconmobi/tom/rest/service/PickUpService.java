package com.traconmobi.tom.rest.service;

import com.traconmobi.tom.rest.model.ApiResponse;

import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.http.Url;

/**
 * Created by kumargaurav on 12/8/15.
 */
public interface PickUpService {
    @GET
    Call<ApiResponse> getPickUp(@Url String url, @QueryMap Map<String, String> data);
}
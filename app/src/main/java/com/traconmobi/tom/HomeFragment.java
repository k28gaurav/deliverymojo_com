package com.traconmobi.tom;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class HomeFragment extends Fragment {

    LinearLayout ll_assgn_shw,ll_report_shw,ll_complete_shw,ll_incomplete_shw,ll_transfer_shw,ll_hndovr_shw;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public static final String PREF_NAME = "Pager";

	public HomeFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
         
//      Button btn_assgnd_clk = (Button)rootView.findViewById(R.id.btnDeliveries);
      ll_assgn_shw = (LinearLayout)rootView.findViewById(R.id.ll_assgn);
      ll_assgn_shw.setOnClickListener(new OnClickListener() 
      {
          @Override
  		public void onClick(View v)
          {

            /*  Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedListMainActivity.class);
//
              startActivity(goToNextActivity);*/
            /*  pref = getActivity().getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
              editor = pref.edit();
              editor.clear();
              editor.putString("position", String.valueOf(0));
              // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
              editor.commit();*/
              /*Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Assigned_Delivery_MainActivity.class);*/
              Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedListMainActivity.class);
//          	goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
              startActivity(goToNextActivity);

          }
      });

        ll_report_shw = (LinearLayout)rootView.findViewById(R.id.ll_report);
        ll_report_shw.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              /*Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Assigned_Delivery_MainActivity.class);*/
                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), ReportsMainActivity.class);
//          	goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(goToNextActivity);
            }
        });

        ll_complete_shw = (LinearLayout)rootView.findViewById(R.id.ll_complete);
        ll_complete_shw.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              /*Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Assigned_Delivery_MainActivity.class);*/
                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignmentCompleteStatus.class);
//          	goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(goToNextActivity);
            }
        });
        ll_incomplete_shw = (LinearLayout)rootView.findViewById(R.id.ll_incomplete);
        ll_incomplete_shw.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              /*Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Assigned_Delivery_MainActivity.class);*/
                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignmentInCompleteStatus.class);
//          	goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(goToNextActivity);
            }
        });

    /*    ll_transfer_shw = (LinearLayout)rootView.findViewById(R.id.ll_transfer);
        ll_transfer_shw.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              *//*Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Assigned_Delivery_MainActivity.class);*//*
                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedTransferActivity.class);
//          	goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(goToNextActivity);
            }
        });

        ll_hndovr_shw = (LinearLayout)rootView.findViewById(R.id.ll_hndovr);
        ll_hndovr_shw.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              *//*Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Assigned_Delivery_MainActivity.class);*//*
                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), HandOverMainActivity.class);
//          	goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(goToNextActivity);
            }
        });*/
        return rootView;
    }
}

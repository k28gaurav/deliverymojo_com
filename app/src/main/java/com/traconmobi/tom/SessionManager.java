package com.traconmobi.tom;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.gson.Gson;
import com.traconmobi.tom.rest.model.TokenParser;
import com.traconmobi.tom.singeleton.UserParser;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    public static final String PREF_NAME = "AndroidHivePref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_UID = "uid";

    // Email address (make variable public to access from outside)
    public static final String KEY_PWD = "pwd";

    public static final String KEY_PATH = "file";

    public static final String KEY_DB_PWD = "dbpwd";

    public static final String KEY_SINGLE_AMOUNT ="single_amt";

    public static final String KEY_AWB_COUNT = "awb_count";

    public static final String KEY_COD_AMT = "amt";

    public static final String KEY_RESPONSE ="response";

    public static final String KEY_RESPONSE_EMAIL="email";

    public static final String KEY_GENERATE_PICKUP="awbno";

    public static final String KEY_RESPONSE_AWB_NUM  = "total_awb_num";
    public static final String KEY_RESPONSE_RECEVR="total_recevr";

    public static final String KEY_PICKUP_AWB_NUM ="get_total_awb_num";
    public static final String KEY_USER_LOC = "user_loc";
    public static final String KEY_USER_NUMERIC_ID = "user_numeric_id";
    public static final String KEY_CURRENT_DT = "curnt_dt";
    public static final String KEY_CUST_ACC_CODE = "cust_acc_code";
    public static final String KEY_USER_NAME = "User_name";
    public static final String KEY_COMPANY_NAME ="Company_name";
    public static final String KEY_USER_LOC_ID = "user_loc_id";

    public static final String KEY_HOME_INITIALIZATION = "first_initialization";
    public static final String KEY_STARTMAP = "start_map";
    public static final String KEY_ENDMAP = "end_map";
    public static final String KEY_START_SERVICE = "start_service";
    public static final String KEY_LOC_ID = "locationId";
    public static final String KEY_DOCS = "docs";
    public static final String KEY_SCAN_ITEMS = "scan_data";
    public static final String KEY_DB_INIT = "DB_Init";
    public static final String KEY_COMPLETE_STATUS = "complete_status";
    public static final String KEY_WAYPONTS = "waypoints";
    public static final String TAG = "SessionManager";
    public static final String KEY_REASON = "reason";
    public static final String KEY_DISTANCE = "distance";
    public static final String KEY_PICKUP = "pickupListdata";
    public static final String KEY_SIZE = "size";
    public static final String KEY_TEXT = "latlngdistext";
    public static final String KEY_INIT_LAT = "initiallat";
    public static final String KEY_INIT_LNG = "initiallng";
    public static final String KEY_NO_PCS = "piece";
    public static final String KEY_WAYBILL_NO = "waybill";
    public static final String KEY_ITEM_DES = "itemdescr";
    public static final String KEY_REASON_RETURN = "reason_return";
    public static final String KEY_DESC_VALUE = "descvalue";
    public static final String KEY_TYPE = "assign_type";
    public static final String KEY_SHIPPER_CITY = "shipper_city";
    public static final String KEY_ADDRESS1 = "address1";
    public static final String KEY_ADDRESS2 = "address2";
    public static final String KEY_PICKTIME = "pickTime";
    public static final String KEY_SHIPPER_PIN = "shipper_pin";
    public static final String KEY_ASSIGNMENTID = "assignId";
    public static final String KEY_USERID = "userid";
    public static final String KEY_CONSIGNEE_NAME = "consignee_name";
    public static final String KEY_ORDERNO = "orderNo";
    public static final String KEY_NAVVAL = "positionnavval";
    public static final String KEY_SCANREF_NO = "scan_ref";
    public static final String KEY_DOCTYPE = "doc_type";
    public static final String KEY_ECOM_SCAN = "ecom_scan";
    public static final String KEY_TOKEN = "getToken";
    public static final String KEY_IMEI = "getIMEI";
    public static final String KEY_BASE_URL = "KEY_BASE_URL";
    public static final String KEY_RVP_URL = "KEY_RVP_URL";
    public static final String KEY_PICKUP_URL = "KEY_PICKUP_URL";
    public static final String KEY_ECOM_URL = "KEY_ECOM_URL";
    public static final String KEY_NOTIFY_SHIPPMENT = "key_notify_shippment";
    public static final String KEY_SHIPPMENT_SIZE = "key_shippment_size";
    public static final String KEY_ENDMAP_COUNT = "KEY_ENDMAP_COUNT";
    public static final String KEY_TIMEZONE = "KEY_TIMEZONE";
    public static final String KEY_TOKENPARSER = "KEY_TOKENPARSER";


    // Constructor
    public SessionManager(Context context)
    {
        try
        {
            this._context = context;
            pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor = pref.edit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception SessionManager " + e.getMessage());
        }
    }

    public void setFirstInitialization(boolean flag) {
        try {
            editor.putBoolean(KEY_HOME_INITIALIZATION, flag);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "FirstInialization " + e.getMessage());
        }
    }

    public boolean getFirstInitialization() {
        return pref.getBoolean(KEY_HOME_INITIALIZATION, true);
    }

    public void setStartTrip(boolean flag) {
        try {
            editor.putBoolean(KEY_STARTMAP, flag);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "StartTrip " + e.getMessage());
        }
    }

    public boolean getStartTrip() {
        return pref.getBoolean(KEY_STARTMAP, false);
    }

    public void setDBInit(boolean flag) {
        try {
            editor.putBoolean(KEY_DB_INIT, flag);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "FirstInialization " + e.getMessage());
        }
    }

    public float getDistance() {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        String username = getUserDetails().get(KEY_USER_NAME);
        return pref.getFloat(username + KEY_DISTANCE + mLastUpdateTime, 0);
    }

    public void setDistance(float distance) {
        try {
            String username = getUserDetails().get(KEY_USER_NAME);
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putFloat(username + KEY_DISTANCE + mLastUpdateTime, distance);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "FirstInialization " + e.getMessage());
        }
    }

    public String getKeyToken() {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        String username = getUserDetails().get(KEY_USER_NAME);
        return pref.getString(username + KEY_TOKEN + mLastUpdateTime, "");
    }

    public void setKeyToken(String token, String username) {
        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putString(username + KEY_TOKEN + mLastUpdateTime, token);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, " " + e.getMessage());
        }
    }

    public String getTimezone() {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        return pref.getString(KEY_TIMEZONE + mLastUpdateTime, "");
    }

    public void setTimezone(String timezone) {
        try {

            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putString(KEY_TIMEZONE + mLastUpdateTime, timezone);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, " " + e.getMessage());
        }
    }

    public void setKeyReasonId(String reason, String id ) {
        editor.putString(reason, id);
        editor.commit();
    }

    public String getKeyReasonId(String reason) {
        return pref.getString(reason, null);
    }

    public String getKeyIMEI() {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        String username = getUserDetails().get(KEY_USER_NAME);
        Log.e(TAG, "Username" + username);
        return pref.getString(username + KEY_IMEI + mLastUpdateTime, "");
    }

    public void setKeyIMEI(String token, String username) {
        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putString(username + KEY_IMEI + mLastUpdateTime, token);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, " " + e.getMessage());
        }
    }

    public void setNotify(boolean flag) {
        editor.putBoolean(KEY_NOTIFY_SHIPPMENT, flag);
        editor.commit();
    }

    public boolean getNotify() {
        return pref.getBoolean(KEY_NOTIFY_SHIPPMENT, false);
    }

    public void setAssignedShippmentSize(int size) {
        String username = getUserDetails().get(KEY_USER_NAME);
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        editor.putInt(username + KEY_SHIPPMENT_SIZE + mLastUpdateTime, size);
        editor.commit();
    }

    public int getAssignedShippmentSize() {
        String username = getUserDetails().get(KEY_USER_NAME);
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        return pref.getInt(username + KEY_SHIPPMENT_SIZE + mLastUpdateTime, 0);
    }


    public String getBaseUrl() {
        return pref.getString(KEY_BASE_URL, "");
    }

    public void setBaseUrl(String baseUrl) {
        try {
            editor.putString(KEY_BASE_URL, baseUrl);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, " " + e.getMessage());
        }
    }

    public String getPickupUrl() {
        return pref.getString(KEY_PICKUP_URL, "");
    }

    public void setPickupUrl(String baseUrl) {
        try {
            editor.putString(KEY_PICKUP_URL, baseUrl);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, " " + e.getMessage());
        }
    }

    public String getRVPUrl() {
        return pref.getString(KEY_RVP_URL, "");
    }

    public void setRVPUrl(String baseUrl) {
        try {
            editor.putString(KEY_RVP_URL, baseUrl);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, " " + e.getMessage());
        }
    }

    public String getEcomUrl() {
        return pref.getString(KEY_ECOM_URL, "");
    }

    public void setEcomUrl(String baseUrl) {
        try {
            editor.putString(KEY_ECOM_URL, baseUrl);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, " " + e.getMessage());
        }
    }

    public String getLat() {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        String username = getUserDetails().get(KEY_USER_NAME);
        return pref.getString(username + KEY_INIT_LAT + mLastUpdateTime, "");
    }

    public void setLat(double currentLat) {
        try {
            String username = getUserDetails().get(KEY_USER_NAME);
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putString(username + KEY_INIT_LAT + mLastUpdateTime, String.valueOf(currentLat));
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "FirstInialization " + e.getMessage());
        }
    }

    public String getLng() {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        String username = getUserDetails().get(KEY_USER_NAME);
        return pref.getString(username + KEY_INIT_LNG + mLastUpdateTime, "");
    }

    public void setLng(double currentLng) {
        try {
            String username = getUserDetails().get(KEY_USER_NAME);
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putString(username + KEY_INIT_LNG + mLastUpdateTime, String.valueOf(currentLng));
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "FirstInialization " + e.getMessage());
        }
    }

    public void removeDistancePref() {
        try {
            editor.remove(KEY_DISTANCE);
            editor.commit();
        }catch (Exception e) {
            Log.e( TAG, "DistancePart " + e.getMessage());
        }
    }

    public void setDocs(String name, Set<String> data) {
        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putStringSet(name + KEY_DOCS + mLastUpdateTime, data);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "DocsPref " + e.getMessage());
        }
    }

    public Set<String> getPickUpId(String name) {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        return pref.getStringSet(name + KEY_PICKUP + mLastUpdateTime, null);
    }

    public void setPickUpId(String name, Set<String> data) {
        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putStringSet(name + KEY_PICKUP + mLastUpdateTime, data);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "DocsPref " + e.getMessage());
        }
    }

    public Set<String> getScanRef(String name) {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        return pref.getStringSet(name + KEY_SCANREF_NO + mLastUpdateTime, null);
    }

    public void setScanRef(String name, Set<String> data) {
        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putStringSet(name + KEY_SCANREF_NO + mLastUpdateTime, data);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "ScanRef " + e.getMessage());
        }
    }

    public Set<String> getDocs(String name) {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        return pref.getStringSet(name + KEY_DOCS + mLastUpdateTime, null);
    }

    public boolean getDBInit() {
        return pref.getBoolean(KEY_DB_INIT, true);
    }

    public void setLocId(String locId) {
        try {
            editor.putString(KEY_LOC_ID, locId);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "LocationId " + e.getMessage());
        }
    }

    public void setUserId(String userId) {
        try {
            editor.putString(KEY_USERID, userId);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "LocationId " + e.getMessage());
        }
    }

    public String getText() {
        String username = getUserDetails().get(KEY_USER_NAME);
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        return pref.getString(username + KEY_LOC_ID + mLastUpdateTime, "");
    }

    public void setText(String text) {
        try {
            String username = getUserDetails().get(KEY_USER_NAME);
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putString(username + KEY_LOC_ID + mLastUpdateTime, text);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public UserParser getParser() {
        String username = getUserDetails().get(KEY_USER_NAME);
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        Gson gson = new Gson();
        String json = pref.getString(username + KEY_TOKENPARSER + mLastUpdateTime, "");
        UserParser parser = gson.fromJson(json, UserParser.class);
        return parser;
    }

    public void setParser(UserParser parser) {
        try {
            String username = getUserDetails().get(KEY_USER_NAME);
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            Gson gson = new Gson();
            String json = gson.toJson(parser);
            editor.putString(username + KEY_TOKENPARSER + mLastUpdateTime, json);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "Parser " + e.getMessage());
        }
    }

    public String getLocId() {
        return pref.getString(KEY_LOC_ID, "");
    }
    public String getuserId() {
        return pref.getString(KEY_USERID, "");
    }

    public void setReason(String key, String reason) {
        try {
            editor.putString(KEY_REASON + key, reason);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e("Reason", e.getMessage());
        }
    }
    public String getReason(String key) {
        return pref.getString(KEY_REASON + key, "");
    }

    public String getNoPcs(String assignId) {

        return pref.getString(KEY_NO_PCS + assignId, "");
    }

    public void setNoPcs(String assignId, String pcs) {
        try {
            editor.putString(KEY_NO_PCS + assignId, pcs);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public int getPosition() {
        return pref.getInt(KEY_NAVVAL, 0);
    }

    public void setPosition(int i) {
        try {
            editor.putInt(KEY_NAVVAL, i);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getConsigneeName(String assignId) {
        return pref.getString(KEY_CONSIGNEE_NAME + assignId, "");
    }

    public void setConsigneeName(String assignId, String pcs) {
        try {
            editor.putString(KEY_CONSIGNEE_NAME + assignId, pcs);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getOrderNo(String assignId) {
        return pref.getString(KEY_ORDERNO + assignId, "");
    }

    public void setOrderNo(String assignId, String pcs) {
        try {
            editor.putString(KEY_ORDERNO + assignId, pcs);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getDocType(String assignId) {
        return pref.getString(KEY_DOCTYPE + assignId, "");
    }

    public void setDocType(String assignId, String pcs) {
        try {
            editor.putString(KEY_DOCTYPE + assignId, pcs);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }


    public String getAssignmentid(String assignId) {
        return pref.getString(KEY_ASSIGNMENTID + assignId, "");
    }

    public void setAssignmentid(String assignId, String value) {
        try {
            editor.putString(KEY_ASSIGNMENTID + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getAssignType(String assignId) {
        return pref.getString(KEY_TYPE + assignId, "");
    }

    public void setAssignType(String assignId, String value) {
        try {
            editor.putString(KEY_TYPE + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getWayBillNo(String assignId) {
        return pref.getString(KEY_WAYBILL_NO + assignId, "");
    }

    public void setWayBillNo(String assignId, String value) {
        try {
            editor.putString(KEY_WAYBILL_NO + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getShipperCity(String assignId) {
        return pref.getString(KEY_SHIPPER_CITY + assignId, "");
    }

    public void setShipperCity(String assignId, String value) {
        try {
            editor.putString(KEY_SHIPPER_CITY + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getShipperPin(String assignId) {
        return pref.getString(KEY_SHIPPER_PIN + assignId, "");
    }

    public void setShipperPin(String assignId, String value) {
        try {
            editor.putString(KEY_SHIPPER_PIN + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getItemDescription(String assignId) {
        return pref.getString(KEY_ITEM_DES + assignId, "");
    }

    public void setItemDescription(String assignId, String value) {
        try {
            editor.putString(KEY_ITEM_DES + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getDescribedValue(String assignId) {
        return pref.getString(KEY_DESC_VALUE + assignId, "");
    }

    public void setDescribedValue(String assignId, String value) {
        try {
            editor.putString(KEY_DESC_VALUE + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getReasonOfReturn(String assignId) {
        return pref.getString(KEY_REASON_RETURN + assignId, "");
    }

    public void setReasonOfReturn(String assignId, String value) {
        try {
            editor.putString(KEY_REASON_RETURN + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public String getAddress2(String assignId) {
        return pref.getString(KEY_ADDRESS2 + assignId, "");
    }

    public void setAddress2(String assignId, String value) {
        try {
            editor.putString(KEY_ADDRESS2 + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }
    public String getPickTime(String assignId) {
        return pref.getString(KEY_PICKTIME + assignId, "");
    }

    public void setPickTime(String assignId, String value) {
        try {
            editor.putString(KEY_PICKTIME + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }
    public String getAddress1(String assignId) {
        return pref.getString(KEY_ADDRESS1 + assignId, "");
    }

    public void setAddress1(String assignId, String value) {
        try {
            editor.putString(KEY_ADDRESS1 + assignId, value);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "TEXT " + e.getMessage());
        }
    }

    public void setWaypoint(Set<String> data, String key) {
        try {
            String username = getUserDetails().get(KEY_USER_NAME);
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.remove(KEY_WAYPONTS + key + mLastUpdateTime);
            editor.commit();
            editor.putStringSet(username + KEY_WAYPONTS + key + mLastUpdateTime, data);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "Waypoints " + e.getMessage());
        }
    }

    public Set<String> getWaypoint(String key) {
        String username = getUserDetails().get(KEY_USER_NAME);
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        return pref.getStringSet(username + KEY_WAYPONTS + key + mLastUpdateTime, null);
    }

  /*  public void setScannedData(Set<String> data, String key) {
        try {
            editor.remove(key);
            editor.commit();
            editor.putStringSet(key, data);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "DocsPref " + e.getMessage());
        }
    }*/
 /* public Set<String> getScannedData(String key) {
      return pref.getStringSet(key, null);
  }*/

    public void setScannedData(Set<String> data, String key) {
        try {
            editor.remove(key);
            editor.commit();
            editor.putStringSet(KEY_ECOM_SCAN + key, data);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "DocsPref " + e.getMessage());
        }
    }

    public Set<String> getScannedData(String key) {
        return pref.getStringSet(KEY_ECOM_SCAN + key, null);
    }


    public int getScannedDataSize(String key) {
        String username = getUserDetails().get(KEY_USER_NAME);
        return pref.getInt(username + KEY_SIZE + key, 0);
    }

    public void setScannedDataSize(int data, String key) {
        try {
            String username = getUserDetails().get(KEY_USER_NAME);
            editor.putInt(username + KEY_SIZE + key, data);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "DocsPref " + e.getMessage());
        }
    }

    public void clearWaypoint() {
        try {
            editor.remove(KEY_WAYPONTS);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "WayPointPref " + e.getMessage());
        }
    }
    /*public Set<String> getScannedData(String key) {
        return pref.getStringSet(key, null);
    }

    public void setDocs(Set<String> data) {
        try {
            editor.putStringSet(KEY_DOCS, data);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "DocsPref " + e.getMessage());
        }
    }*/
    public Set<String> getDocs() {
        return pref.getStringSet(KEY_DOCS, null);
    }


    public void setStartService(boolean flag) {
        try {
            editor.putBoolean(KEY_START_SERVICE, flag);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "StartTrip " + e.getMessage());
        }
    }
    public boolean getStartService() {
        return pref.getBoolean(KEY_START_SERVICE, true);
    }

    public boolean getEndTrip() {
        return pref.getBoolean(KEY_ENDMAP, false);
    }

    public void setEndTrip(boolean flag) {
        try {
            editor.putBoolean(KEY_ENDMAP, flag);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "EndTrip " + e.getMessage());
        }
    }

    public int getEndTripCount() {
        String username = getUserDetails().get(KEY_USER_NAME);
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        return pref.getInt(username + KEY_ENDMAP_COUNT + mLastUpdateTime, 0);
    }

    public void setEndTripCount(int count) {
        try {
            String username = getUserDetails().get(KEY_USER_NAME);
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            editor.putInt(username + KEY_ENDMAP_COUNT + mLastUpdateTime, count);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
            Log.e( TAG, "EndTrip " + e.getMessage());
        }
    }

    public void setCompletestatus(Set<String> completestatus) {
        try {
            editor.putStringSet(KEY_COMPLETE_STATUS, completestatus);
            editor.commit();
        }catch (Exception e) {
            e.getStackTrace();
        }
    }

    public Set<String> getCompleteStatus() {
        return pref.getStringSet(KEY_COMPLETE_STATUS, null);
    }
    /**
     * Create login session
     * */
    public void createLoginSession(String uid, String pwd)
    {
        try
        {
            // Storing login value as TRUE
            editor.putBoolean(IS_LOGIN, true);

            // Storing name in pref
            editor.putString(KEY_UID, uid);

            // Storing email in pref
            editor.putString(KEY_PWD, pwd);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createLoginSession " + e.getMessage());
        }
    }

    /**
     * Create createAuthenticateDbSession session
     * */
    public void createAuthenticateDbSession(String path, String dbpwd)
    {
        try
        {
            // Storing login value as TRUE
//        editor.putBoolean(IS_LOGIN, true);

            // Storing name in pref
            editor.putString(KEY_PATH, path);

            // Storing email in pref
            editor.putString(KEY_DB_PWD, dbpwd);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createAuthenticateDbSession " + e.getMessage());
        }
    }

    /**
     * Create createSingleCODSession session
     * */
    public void createSingleCODSession(String single_amt)
    {
        try
        {
            // Storing login value as TRUE
            editor.putBoolean(IS_LOGIN, true);

            // Storing name in pref
            editor.putString(KEY_SINGLE_AMOUNT, single_amt);

//        // Storing email in pref
//        editor.putString(KEY_PWD, pwd);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createhttpsLoginSession " + e.getMessage());
        }
    }


    /**
     * Create createAuthenticateDbSession session
     * */
    public void createAssgnMultipleSession(String awb_count, String amt)
    {
        try
        {
            // Storing login value as TRUE
//        editor.putBoolean(IS_LOGIN, true);

            // Storing name in pref
            editor.putString(KEY_AWB_COUNT, awb_count);

            // Storing email in pref
            editor.putString(KEY_COD_AMT, amt);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createAssgnMultipleSession " + e.getMessage());
        }
    }

    /**
     * Create createAssgnMultiple_num_Session session
     * */
    public void createAssgnMultiple_num_Session(String total_awb_num)
    {
        try
        {
            // TODO Auto-generated method stub

            // Storing KEY_RESPONSE_AWB_NUM in pref
            editor.putString(KEY_RESPONSE_AWB_NUM, total_awb_num);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createAssgnMultiple_num_Session " + e.getMessage());
        }
    }


    /**
     * Create createAssgnMultiple_num_Session session
     * */
    public void createAssgnMultiple_recevr_Session(String total_recevr)
    {
        try
        {
            // TODO Auto-generated method stub

            // Storing KEY_RESPONSE_AWB_NUM in pref
            editor.putString(KEY_RESPONSE_RECEVR, total_recevr);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createAssgnMultiple_recevr_Session " + e.getMessage());
        }
    }
    /**
     * Create createAssgnMultiple_num_Session session
     * */
    public void createpickupAssgnMultiple_num_Session(String get_total_awb_num)
    {
        try
        {
            // TODO Auto-generated method stub

            // Storing KEY_RESPONSE_AWB_NUM in pref
            editor.putString(KEY_PICKUP_AWB_NUM, get_total_awb_num);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createpickupAssgnMultiple_num_Session " + e.getMessage());
        }
    }

    /**
     * Create httpsLogin session
     * */
    public void createhttpsLoginSession(String response)
    {
        try
        {
            // Storing login value as TRUE
            editor.putBoolean(IS_LOGIN, true);

            // Storing name in pref
            editor.putString(KEY_RESPONSE, response);

//        // Storing email in pref
//        editor.putString(KEY_PWD, pwd);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createhttpsLoginSession " + e.getMessage());
        }
    }

    /**
     * Create httpsLogin session
     * */
    public void createhttpsemailSession(String email)
    {
        try
        {
            // Storing login value as TRUE
//        editor.putBoolean(IS_LOGIN, true);

            // Storing name in pref
            editor.putString(KEY_RESPONSE_EMAIL, email);

//        // Storing email in pref
//        editor.putString(KEY_PWD, pwd);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createhttpsLoginSession " + e.getMessage());
        }
    }


    /**
     * Create httpsLogin session
     * */
    public void createautopickuprefNoSession(String awbno)
    {
        try
        {
            // Storing login value as TRUE
//        editor.putBoolean(IS_LOGIN, true);

            // Storing name in pref
            editor.putString(KEY_GENERATE_PICKUP, awbno);

//        // Storing email in pref
//        editor.putString(KEY_PWD, pwd);

            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createautopickuprefNoSession " + e.getMessage());
        }
    }


    /**
     * Create createuserdetailsSession
     * */
    public void createUserDetailsSession(String user_loc, String user_numeric_id ,String curnt_dt, String cust_acc_code,String User_name,String Company_name,String user_loc_id)
    {
        try
        {
//        // Storing login value as TRUE
//        editor.putBoolean(IS_LOGIN, true);

            // Storing user_loc in pref
            editor.putString(KEY_USER_LOC, user_loc);

            // Storing user_numeric_id in pref
            editor.putString(KEY_USER_NUMERIC_ID, user_numeric_id);

            // Storing curnt_dt in pref
            editor.putString(KEY_CURRENT_DT, curnt_dt);

            // Storing cust_acc_code in pref
            editor.putString(KEY_CUST_ACC_CODE, cust_acc_code);

            // Storing User_name in pref
            editor.putString(KEY_USER_NAME, User_name);

            // Storing Company_name in pref
            editor.putString(KEY_COMPANY_NAME, Company_name);

            // Storing Company_name in pref
            editor.putString(KEY_USER_LOC_ID,user_loc_id);
            // commit changes
            editor.commit();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception createUserDetailsSession " + e.getMessage());
        }
    }


    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin()
    {
        try
        {
            // Check login status
            if(!this.isLoggedIn()){
                // user is not logged in redirect him to Login Activity
                Intent i = new Intent(_context, TOMLoginUserActivity.class);
                // Closing all the Activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // Staring Login Activity
                _context.startActivity(i);
            }
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e( TAG, "Exception checkLogin " + e.getMessage());
        }

    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getLoginDetails()
    {

        HashMap<String, String> login_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_UID
            login_Dts.put(KEY_UID, pref.getString(KEY_UID, null));

            // user KEY_PWD
            login_Dts.put(KEY_PWD, pref.getString(KEY_PWD, null));

        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getLoginDetails " + e.getMessage());
        }
        // return login_Dts
        return login_Dts;

    }

    /**
     * Get stored DBAuthenticate session data
     * */
    public HashMap<String, String> getAuthenticateDbDetails()
    {
        HashMap<String, String> authenticate_db_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_PATH
            authenticate_db_Dts.put(KEY_PATH, pref.getString(KEY_PATH, null));

            // user KEY_DB_PWD
            authenticate_db_Dts.put(KEY_DB_PWD, pref.getString(KEY_DB_PWD, null));
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getAuthenticateDbDetails " + e.getMessage());
        }
        // return authenticate_db_Dts
        return authenticate_db_Dts;
    }

    /**
     * Get stored getSingleCODDetails session data
     * */
    public HashMap<String, String> getSingleCODDetails()
    {
        HashMap<String, String> SingleCOD_Dts = new HashMap<String, String>();
        try
        {
//        // user KEY_AWB_COUNT
//        	SingleCOD_Dts.put(KEY_AWB_COUNT, pref.getString(KEY_AWB_COUNT, null));

            // user KEY_COD_AMT
            SingleCOD_Dts.put(KEY_SINGLE_AMOUNT, pref.getString(KEY_SINGLE_AMOUNT, null));
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getAssgnMultipleDetails " + e.getMessage());
        }
        // return AssgnMultiple_Dts
        return SingleCOD_Dts;
    }


    /**
     * Get stored AssgnMultiple session data
     * */
    public HashMap<String, String> getAssgnMultipleDetails()
    {
        HashMap<String, String> AssgnMultiple_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_AWB_COUNT
            AssgnMultiple_Dts.put(KEY_AWB_COUNT, pref.getString(KEY_AWB_COUNT, null));

            // user KEY_COD_AMT
            AssgnMultiple_Dts.put(KEY_COD_AMT, pref.getString(KEY_COD_AMT, null));
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getAssgnMultipleDetails " + e.getMessage());
        }
        // return AssgnMultiple_Dts
        return AssgnMultiple_Dts;
    }

    /**
     * Get stored AssgnMultiple session data
     * */
    public HashMap<String, String> getAssgnMultiple_num_Details()
    {
        HashMap<String, String> AssgnMultiple_num_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_AWB_COUNT
            AssgnMultiple_num_Dts.put(KEY_RESPONSE_AWB_NUM, pref.getString(KEY_RESPONSE_AWB_NUM, null));

        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getAssgnMultiple_num_Details " + e.getMessage());
        }
        // return AssgnMultiple_Dts
        return AssgnMultiple_num_Dts;
    }

    /**
     * Get stored AssgnMultiple session data
     * */
    public HashMap<String, String> getAssgnMultiple_recevr_Details()
    {
        HashMap<String, String> AssgnMultiple_recevr_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_AWB_COUNT
            AssgnMultiple_recevr_Dts.put(KEY_RESPONSE_RECEVR, pref.getString(KEY_RESPONSE_RECEVR, null));

        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getAssgnMultiple_recevr_Details " + e.getMessage());
        }
        // return AssgnMultiple_Dts
        return AssgnMultiple_recevr_Dts;
    }

    /**
     * Get stored pickupAssgnMultiple session data
     * */
    public HashMap<String, String> getpickupAssgnMultiple_num_Details()
    {
        HashMap<String, String> PickupAssgnMultiple_num_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_PICKUP_AWB_NUM
            PickupAssgnMultiple_num_Dts.put(KEY_PICKUP_AWB_NUM, pref.getString(KEY_PICKUP_AWB_NUM, null));

        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getpickupAssgnMultiple_num_Details " + e.getMessage());
        }
        // return AssgnMultiple_Dts
        return PickupAssgnMultiple_num_Dts;
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> gethttpsrespDetails()
    {
        HashMap<String, String> resp_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_UID
            resp_Dts.put(KEY_RESPONSE, pref.getString(KEY_RESPONSE, null));
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception gethttpsrespDetails " + e.getMessage());
        }
        // return login_Dts
        return resp_Dts;
    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> gethttpsemailDetails()
    {
        HashMap<String, String> resp_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_UID
            resp_Dts.put(KEY_RESPONSE_EMAIL, pref.getString(KEY_RESPONSE_EMAIL, null));
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception gethttpsemailDetails " + e.getMessage());
        }
        // return login_Dts
        return resp_Dts;
    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getautopickupgenerateDetails()
    {
        HashMap<String, String> resp_Dts = new HashMap<String, String>();
        try
        {
            // user KEY_UID
            resp_Dts.put(KEY_GENERATE_PICKUP, pref.getString(KEY_GENERATE_PICKUP, null));
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getautopickupgenerateDetails " + e.getMessage());
        }
        // return login_Dts
        return resp_Dts;
    }


    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails()
    {
        HashMap<String, String> user = new HashMap<String, String>();
        try
        {
            //KEY_USER_LOC
            user.put(KEY_USER_LOC, pref.getString(KEY_USER_LOC, null));

            //KEY_USER_NUMERIC_ID
            user.put(KEY_USER_NUMERIC_ID, pref.getString(KEY_USER_NUMERIC_ID, null));

            //KEY_CURRENT_DT
            user.put(KEY_CURRENT_DT, pref.getString(KEY_CURRENT_DT, null));

            //KEY_CUST_ACC_CODE
            user.put(KEY_CUST_ACC_CODE, pref.getString(KEY_CUST_ACC_CODE, null));

            //KEY_USER_NAME
            user.put(KEY_USER_NAME, pref.getString(KEY_USER_NAME, null));

            //KEY_COMPANY_NAME
            user.put(KEY_COMPANY_NAME, pref.getString(KEY_COMPANY_NAME, null));
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Log.e(TAG, "Exception getUserDetails " + e.getMessage());
        }
        // return user
        return user;
    }


    /**
     * Clear session details
     * */
    public void logoutUser()
    {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
//        Intent i = new Intent(_context, LoginActivity.class);
//        // Closing all the Activities
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//         
//        // Add new Flag to start new Activity
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//         
//        // Staring Login Activity
//        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}



/**
 * Created by Ashwini Naik on 11/10/15.
 */
package com.traconmobi.tom;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class AssignedTransferActivity extends Activity {

	TinyDB tinydb;
	 // Session Manager Class
    SessionManager session;    
    
    TextView version_name;
	
    public String FORM_TYPE_VAL;
    
    protected SQLiteDatabase db;
    
    protected Cursor cursor_outscan_dt,cur,cr,cursor_transfr_list,c;
    
    MyCustomAdapter dataAdapter = null;
    
    protected ListView filterList;
    
    Button btn_submit;
    
    static String RoomType;
    
    public String session_RESPONSE_AWB_NUM,session_userid,session_DB_PATH,session_DB_PWD,session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,gettomnoti_response,session_noti_resp,session_outscan_resp,getoutscan_response;
    
    public String complted_type,Assignment_Type,fltr_type,awb_outscantime,awb_outscan_dt,cod_amt_val;
    
    public String trsfr_usr_name,trnsfr_usr_id,num,spnr_reason,spnr_trnsfr_usr,transfr_usr_id;
    
    StringBuffer responseText;
    public String result;
    
    RelativeLayout rl_reason;
    
    Spinner s_reason,s_trsfr;

	private PopupWindow pwindo;
	Button btnClosePopup;
	TextView title;
	public String TAG="AssignedTransferActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		try
		{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		//Intializing Fabric
		Fabric.with(this, new Crashlytics());	
		setContentView(R.layout.activity_assigned_transfer);
		getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_assigned_transfer);

		title=(TextView)findViewById(R.id.txt_title);
		title.setText("TRANSFER LIST");
		
		 session = new SessionManager(getApplicationContext());

		 tinydb = new TinyDB(getApplicationContext());
		 /**
	         * GETTING SESSION VALUES
	         * */
	        
	        // get AuthenticateDb data from session
	        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
	         
	        // DB_PATH
	        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
	       
	        // DB_PWD
	        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
	       	
	         
	        // get user data from session
	        HashMap<String, String> login_Dts = session.getLoginDetails();
	         
	        // Userid
	        session_user_id = login_Dts.get(SessionManager.KEY_UID);
	         
	        // pwd
	        session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);
	        
	        
	     // get user data from session
	        HashMap<String, String> user = session.getUserDetails();
	         
	        // session_USER_LOC
	        session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);	      
	         
	        // session_USER_NUMERIC_ID
	        session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);
	      
	        // session_CURRENT_DT
	        session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);
	       
	        // session_CUST_ACC_CODE
	        session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);
	        
	        // session_USER_NAME
	        session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);

		
	        rl_reason = (RelativeLayout)findViewById(R.id.rl_popup_reasn);
	        
	        filterList = (ListView) findViewById (R.id.list);
	        btn_submit =(Button)findViewById(R.id.btnsubmit_trnfr);
	        
	        
	        // get AssgnMultiple_num_Dts data from session
	        HashMap<String, String> AssgnMultiple_num_Dts = session.getAssgnMultiple_num_Details();
	         
	        // session_RESPONSE_AWB_NUM
	        session_RESPONSE_AWB_NUM = AssgnMultiple_num_Dts.get(SessionManager.KEY_RESPONSE_AWB_NUM);

	        
			FORM_TYPE_VAL=tinydb.getString("FORM_TYPE");

			FORM_TYPE_VAL="DELIVERED";

			spinnerloadtrsfr_usr();
			spinnerloadtrsfr_reason();
			
			try
			{
				if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("DELIVERED") ))
	        	{
					shw_del_list();
//	        		Intent goToNextActivity = new Intent(getApplicationContext(), TOMContactActivity.class);
//	        		startActivity(goToNextActivity);
	        	}
	        	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("UNDELIVERED")))
	        	{
	        		shw_undel_list();
//	        		Intent goToNextActivity = new Intent(getApplicationContext(), TOMContactActivity.class);
//	        		startActivity(goToNextActivity);
	        	}
	        	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("PAYMENT_DELIVERED")))
	        	{
	        		shw_paymnt_del_list();
//	        		Intent goToNextActivity = new Intent(getApplicationContext(), PaymentAssigned_NewActivity.class);
//	        		startActivity(goToNextActivity);
	        	}
	        	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("PAYMENT_UNDELIVERED")))
	        	{
	        		shw_paymnt_undel_list();
//	        		Intent goToNextActivity = new Intent(getApplicationContext(), PaymentAssigned_NewActivity.class);
//	        		startActivity(goToNextActivity);
	        	}
			}
			catch(Exception e)
			{
				 Crashlytics.log(android.util.Log.ERROR, TAG, "Exception FORM_TYPE_VAL AssignedTransferActivity" + e.getMessage());
			}
			Button btn_cancel=(Button)findViewById(R.id.btncancel);
	        btn_cancel.setOnClickListener(new View.OnClickListener() 
	        {            
	            @Override
	            public void onClick(View v) {
	                // TODO Auto-generated method stub   
	            	if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("DELIVERED") ))
	            	{
//	            		Intent goToNextActivity = new Intent(getApplicationContext(), Assigned_Delivery_MainActivity.class);
						Intent goToNextActivity = new Intent(getApplicationContext(), HomeMainActivity.class);
	            		startActivity(goToNextActivity);
	            	}
	            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("UNDELIVERED")))
	            	{
//	            		Intent goToNextActivity = new Intent(getApplicationContext(), Assigned_Undelivery_MainActivity.class);
//	            		startActivity(goToNextActivity);
	            	}
	            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("PAYMENT_DELIVERED")))
	            	{
//	            		Intent goToNextActivity = new Intent(getApplicationContext(), PaymentAssigned_NewActivity.class);
//	            		startActivity(goToNextActivity);
	            	}
	            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("PAYMENT_UNDELIVERED")))
	            	{
//	            		Intent goToNextActivity = new Intent(getApplicationContext(), ShowPayment_failure_details.class);
//	            		startActivity(goToNextActivity);
	            	}
	            	
           }
            });
		}
		catch(Exception e)
		{
			Crashlytics.log(android.util.Log.ERROR, TAG, "onCreate" + e.getMessage());
		}
	}


	public void shw_del_list()
    {
    	try
    	{
    		if(session_DB_PATH != null && session_DB_PWD != null)
  		  {	
    			db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
    	    	 Assignment_Type="D";
    	    	 fltr_type="A";
//    	        cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Consignee_Name LIKE  '%"+ searchText.getText().toString().trim() +"%' AND T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
//    	    	 cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
			  cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND D_OutScan_Date='" + session_CURRENT_DT + "'", null);
    	        while(cursor_outscan_dt.moveToNext()){
    	            awb_outscantime=cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("D_OutScan_Date"));
    	            awb_outscan_dt=awb_outscantime.substring(0, 10);
    	            cod_amt_val = cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("F_Amount"));
    	    	    session.createSingleCODSession(cod_amt_val);
    	        }
//    	        cursor_outscan_dt.close();
//    	        db.close();
    	        if(awb_outscan_dt == null)
    	        {
    	            Toast.makeText(AssignedTransferActivity.this,"No outscan Assignments to transfer,\n please contact Authorized Executive", Toast.LENGTH_SHORT).show();
    	            btn_submit.setEnabled(false);
    	        }
    	        else if(awb_outscan_dt != null)
//        	if(awb_outscan_dt.equals(session_CURRENT_DT))
		        {
    	      /* if(session_RESPONSE_AWB_NUM != null && session_RESPONSE_AWB_NUM !="")
    	       {*/
//        		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
        		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
        		ArrayList<Country> countryList = new ArrayList<Country>();
        		Assignment_Type="D";
       	 	 	fltr_type="A";
//        		cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter IS NULL AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
       	 	/*String shw[] = session_RESPONSE_AWB_NUM.toString().split(",");
	        for(int k = 0 ; k< shw.length ; k++)
	        {
	        String result = shw[k];*/
//       	 	 	cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Number='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed IS NULL AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
//				cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Number='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
				cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND D_OutScan_Date='" + session_CURRENT_DT + "' ", null);
		            //                                              new String[]{"%" + searchText.getText().toString() + "%"});
//		            final MyCursorAdapter adapter  = new  MyCursorAdapter(
//				                        this, 
//				                        R.layout.del_filter_accept, 
//				                        cur, 
//				                        new String[] {"T_Assignment_Number","T_Consignee_Name","T_City" ,"T_Pincode","F_Amount","T_Shipper_name"},
//				                      //  new String[] {"OS_AWB_No","Consignee_Contact_Number","Consignee_Name", "Address_Line_1", "Address_Line_2","City" ,"Pincode"},
//				                        new int[] {R.id.awb,R.id.firstName,R.id.city,R.id.pincode,R.id.COD_Amount,R.id.shpr_val},0);
        		while(cur.moveToNext())
				 {
        		String awb_num= cur.getString(cur.getColumnIndex("T_Assignment_Number"));
		        String cname= cur.getString(cur.getColumnIndex("T_Consignee_Name"));
		        String cty= cur.getString(cur.getColumnIndex("T_City"));
		        String pncode= cur.getString(cur.getColumnIndex("T_Pincode"));
		        String amt= cur.getString(cur.getColumnIndex("F_Amount"));
		        String shpr_name= cur.getString(cur.getColumnIndex("T_Shipper_name"));
		        
//        		Country country = new Country(awb_num,cname,cty,pncode,amt,shpr_name,false);commented on 24apr2015
					 Country country = new Country(awb_num,cname,false);
		 		  countryList.add(country);
		        }
//	        }
        		dataAdapter = new MyCustomAdapter(this,
    				    R.layout.del_filter_accept, countryList);
        		
		    //***********************VIEW BINDER IS USED TO DISPLAY THE AMOUNT IN LISTVIEW***********************/
//		            adapter.setViewBinder(new ViewBinder() {
//		               @Override
//					public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
//		                    if (columnIndex == 8) {
//		                        Double preco = cursor.getDouble(columnIndex);
//		                        TextView textView = (TextView) view;
//				                      textView.setText(String.format("%.2f", preco));
//				                      return true;
//		                    }
//		                    return false;
//		               }
//		            });
		            filterList.setAdapter(dataAdapter);
		            filterList.setCacheColorHint(Color.WHITE);
		            btn_submit.setEnabled(true);
		        }
		        else{
		            Toast.makeText(AssignedTransferActivity.this,"No outscan Assignments to transfer,\n please contact Authorized Executive", Toast.LENGTH_LONG).show();
		            btn_submit.setEnabled(false);
		        }
  		}
    		else
    		{
    			Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity step3 shwsinglelist TOMLoginUserActivity.file is null ");
//    			onClickGoToHomePage();
    		}
    	}

    	catch(SQLiteException e){
    		e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "onCreate SQLiteException" + e.getMessage());
//    		onClickGoToHomePage();
    	}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "onCreate UnsatisfiedLinkError" + err.getMessage());
//    		onClickGoToHomePage();
    	}
		catch (Exception e)
		{
			Crashlytics.log(android.util.Log.ERROR, TAG, "onCreate" + e.getMessage());
		}
    	  finally 
    	  {
    		  if(cur !=null && !cur.isClosed())
    		  {
    			  cur.close();
    		  }
    		  if(cursor_outscan_dt !=null && !cursor_outscan_dt.isClosed())
    		  {
    			  cursor_outscan_dt.close();
    		  }
    	      if (db != null && db.isOpen()) 
    	      {
    	    	  db.close();
    	      }
    	  }	 

    }
	
	
	public void shw_undel_list()
    {
    	try
    	{
    		if(session_DB_PATH != null && session_DB_PWD != null)
  		  {	
    			db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
    	    	 Assignment_Type="D";
    	    	 fltr_type="A";
    	    	  complted_type="FALSE";
//    	        cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Consignee_Name LIKE  '%"+ searchText.getText().toString().trim() +"%' AND T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
    	    	 cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed='" + complted_type + "' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
    	        while(cursor_outscan_dt.moveToNext()){
    	            awb_outscantime=cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("D_OutScan_Date"));
    	            awb_outscan_dt=awb_outscantime.substring(0, 10);
    	            cod_amt_val = cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("F_Amount"));
    	    	    session.createSingleCODSession(cod_amt_val);
    	        }
//    	        cursor_outscan_dt.close();
//    	        db.close();
    	        if(awb_outscan_dt == null)
    	        {
    	            Toast.makeText(AssignedTransferActivity.this,"No outscan Deliveries for today,\n please contact Authorized Executive", Toast.LENGTH_SHORT).show();
    	            btn_submit.setEnabled(false);
    	        }
    	        else if(awb_outscan_dt != null)
//        	if(awb_outscan_dt.equals(session_CURRENT_DT))
		        {
    	        	 if(session_RESPONSE_AWB_NUM != null && session_RESPONSE_AWB_NUM !="") 
    	    	       {
//        		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
        		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
        		ArrayList<Country> countryList = new ArrayList<Country>();
        		Assignment_Type="D";
       	 	 	fltr_type="A";
       	 	complted_type="FALSE";
       	 	String shw[] = session_RESPONSE_AWB_NUM.toString().split(",");
	        for(int k = 0 ; k< shw.length ; k++)
	        {
	        String result = shw[k];
//        		cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter IS NULL AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
        		cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Number='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed='" + complted_type + "' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
		            //                                              new String[]{"%" + searchText.getText().toString() + "%"});
//		            final MyCursorAdapter adapter  = new  MyCursorAdapter(
//				                        this, 
//				                        R.layout.del_filter_accept, 
//				                        cur, 
//				                        new String[] {"T_Assignment_Number","T_Consignee_Name","T_City" ,"T_Pincode","F_Amount","T_Shipper_name"},
//				                      //  new String[] {"OS_AWB_No","Consignee_Contact_Number","Consignee_Name", "Address_Line_1", "Address_Line_2","City" ,"Pincode"},
//				                        new int[] {R.id.awb,R.id.firstName,R.id.city,R.id.pincode,R.id.COD_Amount,R.id.shpr_val},0);
        		while(cur.moveToNext())
				 {
        		String awb_num= cur.getString(cur.getColumnIndex("T_Assignment_Number"));
		        String cname= cur.getString(cur.getColumnIndex("T_Consignee_Name"));
		        String cty= cur.getString(cur.getColumnIndex("T_City"));
		        String pncode= cur.getString(cur.getColumnIndex("T_Pincode"));
		        String amt= cur.getString(cur.getColumnIndex("F_Amount"));
		        String shpr_name= cur.getString(cur.getColumnIndex("T_Shipper_name"));
		        
//        		Country country = new Country(awb_num,cname,cty,pncode,amt,shpr_name,false);
		Country country=new Country(awb_num,cname,true);
		 		  countryList.add(country);
		        }
	        }
        		dataAdapter = new MyCustomAdapter(this,
    				    R.layout.del_filter_accept, countryList);
        		
		    //***********************VIEW BINDER IS USED TO DISPLAY THE AMOUNT IN LISTVIEW***********************/
//		            adapter.setViewBinder(new ViewBinder() {
//		               @Override
//					public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
//		                    if (columnIndex == 8) {
//		                        Double preco = cursor.getDouble(columnIndex);
//		                        TextView textView = (TextView) view;
//				                      textView.setText(String.format("%.2f", preco));
//				                      return true;
//		                    }
//		                    return false;
//		               }
//		            });
		            filterList.setAdapter(dataAdapter);
		            filterList.setCacheColorHint(Color.WHITE);
//		            employeeList.setOnItemClickListener(new OnItemClickListener(){
//		                @Override
//						public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
//		                    // TODO Auto-generated method stub
//				 			Intent intent = new Intent(getApplicationContext(), TOMConDetailsActivity.class);			
//				 			Cursor cursor = (Cursor) adapter.getItem(position);
//		                    intent.putExtra("EMPLOYEE_ID", cursor.getInt(cursor.getColumnIndex("_id")));
//				 			index = cur.getString(cur.getColumnIndex("T_Assignment_Number"));
//				 			indx=cur.getString(cur.getColumnIndex("_id"));
//				 			Crashlytics.log(android.util.Log.ERROR,TAG,"indx " + indx);
//				 			screen_id="ContactsActivity";
//				 			 Bundle bundle = new Bundle();
//				 			 keyword ="from_delpage";
//				 	         bundle.putString("frm_page", keyword);		 	        
//				 	        intent.putExtras(bundle);          
//				            intent.putExtra(EXTRA_MESSAGE, index);
//				            startActivity(intent);
//						} 			 		
//		            });
//		            db.close();  
		            btn_submit.setEnabled(true);
    	    	       }
    	    	       else{
    			            Toast.makeText(AssignedTransferActivity.this,"No outscan Assignments to transfer", Toast.LENGTH_LONG).show();
    			            btn_submit.setEnabled(false);
    			        }
		        }
		        else{
		            Toast.makeText(AssignedTransferActivity.this,"No outscan Deliveries for today,\n please contact Authorized Executive", Toast.LENGTH_LONG).show();
		            btn_submit.setEnabled(false);
		        }
//  		  }
//        else if(count_shw_filtr == 0)
//        {
//        	Intent goToNextActivity = new Intent(getApplicationContext(),TOMWelcomeActivity.class);
//			startActivity(goToNextActivity);
//        }
//        db.close();
  		}
    		else
    		{
    			Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity step3 shwsinglelist TOMLoginUserActivity.file is null ");
//    			onClickGoToHomePage();
    		}
    	}
    	catch(SQLiteException e){
    		e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "shw_undel_list SQLiteException" + e.getMessage());
//    		onClickGoToHomePage();
    	}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "shw_undel_list UnsatisfiedLinkError" + err.getMessage());
//    		onClickGoToHomePage();
    	}
		catch (Exception e)
		{
			Crashlytics.log(android.util.Log.ERROR, TAG, "shw_undel_list" + e.getMessage());
		}
    	  finally 
    	  {
    		  if(cur !=null && !cur.isClosed())
    		  {
    			  cur.close();
    		  }
    		  if(cursor_outscan_dt !=null && !cursor_outscan_dt.isClosed())
    		  {
    			  cursor_outscan_dt.close();
    		  }
    	      if (db != null && db.isOpen()) 
    	      {
    	    	  db.close();
    	      }
    	  }	 

    }
	
	public void shw_paymnt_del_list()
    {
    	try
    	{
    		if(session_DB_PATH != null && session_DB_PWD != null)
  		  {	
    			db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
    	    	 Assignment_Type="C";
    	    	 fltr_type="A";
//    	        cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Consignee_Name LIKE  '%"+ searchText.getText().toString().trim() +"%' AND T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
    	    	 cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
    	        while(cursor_outscan_dt.moveToNext()){
    	            awb_outscantime=cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("D_OutScan_Date"));
    	            awb_outscan_dt=awb_outscantime.substring(0, 10);
    	            cod_amt_val = cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("F_Amount"));
    	    	    session.createSingleCODSession(cod_amt_val);
    	        }
//    	        cursor_outscan_dt.close();
//    	        db.close();
    	        if(awb_outscan_dt == null)
    	        {
    	            Toast.makeText(AssignedTransferActivity.this,"No outscan Deliveries for today,\n please contact Authorized Executive", Toast.LENGTH_SHORT).show();
    	            btn_submit.setEnabled(false);
    	        }
    	        else if(awb_outscan_dt != null)
//        	if(awb_outscan_dt.equals(session_CURRENT_DT))
		        {
    	        	 if(session_RESPONSE_AWB_NUM != null && session_RESPONSE_AWB_NUM !="") 
  	    	       {
//        		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
        		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
        		ArrayList<Country> countryList = new ArrayList<Country>();
        		Assignment_Type="C";
       	 	 	fltr_type="A";
       	 	String shw[] = session_RESPONSE_AWB_NUM.toString().split(",");
	        for(int k = 0 ; k< shw.length ; k++)
	        {
	        String result = shw[k];
//        		cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter IS NULL AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
        		cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Number='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
		            //                                              new String[]{"%" + searchText.getText().toString() + "%"});
//		            final MyCursorAdapter adapter  = new  MyCursorAdapter(
//				                        this, 
//				                        R.layout.del_filter_accept, 
//				                        cur, 
//				                        new String[] {"T_Assignment_Number","T_Consignee_Name","T_City" ,"T_Pincode","F_Amount","T_Shipper_name"},
//				                      //  new String[] {"OS_AWB_No","Consignee_Contact_Number","Consignee_Name", "Address_Line_1", "Address_Line_2","City" ,"Pincode"},
//				                        new int[] {R.id.awb,R.id.firstName,R.id.city,R.id.pincode,R.id.COD_Amount,R.id.shpr_val},0);
        		while(cur.moveToNext())
				 {
        		String awb_num= cur.getString(cur.getColumnIndex("T_Assignment_Number"));
		        String cname= cur.getString(cur.getColumnIndex("T_Consignee_Name"));
		        String cty= cur.getString(cur.getColumnIndex("T_City"));
		        String pncode= cur.getString(cur.getColumnIndex("T_Pincode"));
		        String amt= cur.getString(cur.getColumnIndex("F_Amount"));
		        String shpr_name= cur.getString(cur.getColumnIndex("T_Shipper_name"));
		        
//        		Country country = new Country(awb_num,cname,cty,pncode,amt,shpr_name,false);
		Country country = new Country(awb_num,cname,true);
		 		  countryList.add(country);
		        }
	        }
        		dataAdapter = new MyCustomAdapter(this,
		R.layout.del_filter_accept,countryList);

		//***********************VIEW BINDER IS USED TO DISPLAY THE AMOUNT IN LISTVIEW***********************/
//		            adapter.setViewBinder(new ViewBinder() {
//		               @Override
//					public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
//		                    if (columnIndex == 8) {
//		                        Double preco = cursor.getDouble(columnIndex);
//		                        TextView textView = (TextView) view;
//				                      textView.setText(String.format("%.2f", preco));
//				                      return true;
//		                    }
//		                    return false;
//		               }
//		            });
		            filterList.setAdapter(dataAdapter);
		            filterList.setCacheColorHint(Color.WHITE);

		            btn_submit.setEnabled(true);
		        }
     	       else{
 		            Toast.makeText(AssignedTransferActivity.this,"No outscan Assignments to transfer", Toast.LENGTH_LONG).show();
 		            btn_submit.setEnabled(false);
 		        }
		        }
		        else{
		            Toast.makeText(AssignedTransferActivity.this,"No outscan Deliveries for today,\n please contact Authorized Executive", Toast.LENGTH_LONG).show();
		            btn_submit.setEnabled(false);
		        }
//  		  }
//        else if(count_shw_filtr == 0)
//        {
//        	Intent goToNextActivity = new Intent(getApplicationContext(),TOMWelcomeActivity.class);
//			startActivity(goToNextActivity);
//        }
//        db.close();
  		}
    		else
    		{
    			Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity step3 shwsinglelist TOMLoginUserActivity.file is null ");
//    			onClickGoToHomePage();
    		}
    	}
    	catch(SQLiteException e){
    		e.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity SQLiteException shwsinglelist ");
//    		onClickGoToHomePage();
    	}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity UnsatisfiedLinkError shwsinglelist ");
//    		onClickGoToHomePage();
    	}
    	  finally 
    	  {
    		  if(cur !=null && !cur.isClosed())
    		  {
    			  cur.close();
    		  }
    		  if(cursor_outscan_dt !=null && !cursor_outscan_dt.isClosed())
    		  {
    			  cursor_outscan_dt.close();
    		  }
    	      if (db != null && db.isOpen()) 
    	      {
    	    	  db.close();
    	      }
    	  }	 

    }
	
	
	public void shw_paymnt_undel_list()
    {
    	try
    	{
    		if(session_DB_PATH != null && session_DB_PWD != null)
  		  {	
    			db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
    	    	 Assignment_Type="C";
    	    	 fltr_type="A";
    	    	 complted_type="FALSE";
//    	        cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Consignee_Name LIKE  '%"+ searchText.getText().toString().trim() +"%' AND T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
    	    	 cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number, T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed='" + complted_type + "' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
    	        while(cursor_outscan_dt.moveToNext()){
    	            awb_outscantime=cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("D_OutScan_Date"));
    	            awb_outscan_dt=awb_outscantime.substring(0, 10);
    	            cod_amt_val = cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("F_Amount"));
    	    	    session.createSingleCODSession(cod_amt_val);
    	        }
//    	        cursor_outscan_dt.close();
//    	        db.close();
    	        if(awb_outscan_dt == null)
    	        {
    	            Toast.makeText(AssignedTransferActivity.this,"No outscan Deliveries for today,\n please contact Authorized Executive", Toast.LENGTH_SHORT).show();
    	            btn_submit.setEnabled(false);
    	        }
    	        else if(awb_outscan_dt != null)
//        	if(awb_outscan_dt.equals(session_CURRENT_DT))
		        {
    	        	 if(session_RESPONSE_AWB_NUM != null && session_RESPONSE_AWB_NUM !="") 
    	    	       {
//        		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
        		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
        		ArrayList<Country> countryList = new ArrayList<Country>();
        		Assignment_Type="C";
       	 	 	fltr_type="A";
       	 	complted_type="FALSE";
       	 	String shw[] = session_RESPONSE_AWB_NUM.toString().split(",");
	        for(int k = 0 ; k< shw.length ; k++)
	        {
	        String result = shw[k];
//        		cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter IS NULL AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
        		cur = db.rawQuery("SELECT _id , T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Assignment_Number='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND T_Filter='" + fltr_type + "' AND B_is_Completed='" + complted_type + "' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
		            //                                              new String[]{"%" + searchText.getText().toString() + "%"});
//		            final MyCursorAdapter adapter  = new  MyCursorAdapter(
//				                        this, 
//				                        R.layout.del_filter_accept, 
//				                        cur, 
//				                        new String[] {"T_Assignment_Number","T_Consignee_Name","T_City" ,"T_Pincode","F_Amount","T_Shipper_name"},
//				                      //  new String[] {"OS_AWB_No","Consignee_Contact_Number","Consignee_Name", "Address_Line_1", "Address_Line_2","City" ,"Pincode"},
//				                        new int[] {R.id.awb,R.id.firstName,R.id.city,R.id.pincode,R.id.COD_Amount,R.id.shpr_val},0);
        		while(cur.moveToNext())
				 {
        		String awb_num= cur.getString(cur.getColumnIndex("T_Assignment_Number"));
		        String cname= cur.getString(cur.getColumnIndex("T_Consignee_Name"));
		        String cty= cur.getString(cur.getColumnIndex("T_City"));
		        String pncode= cur.getString(cur.getColumnIndex("T_Pincode"));
		        String amt= cur.getString(cur.getColumnIndex("F_Amount"));
		        String shpr_name= cur.getString(cur.getColumnIndex("T_Shipper_name"));
		        
//        		Country country = new Country(awb_num,cname,cty,pncode,amt,shpr_name,false);
		Country country= new Country(awb_num,cname,true);
		 		  countryList.add(country);
		        }
	        }
        		dataAdapter = new MyCustomAdapter(this,
		R.layout.del_filter_accept,countryList);

		//***********************VIEW BINDER IS USED TO DISPLAY THE AMOUNT IN LISTVIEW***********************/
//		            adapter.setViewBinder(new ViewBinder() {
//		               @Override
//					public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
//		                    if (columnIndex == 8) {
//		                        Double preco = cursor.getDouble(columnIndex);
//		                        TextView textView = (TextView) view;
//				                      textView.setText(String.format("%.2f", preco));
//				                      return true;
//		                    }
//		                    return false;
//		               }
//		            });
		            filterList.setAdapter(dataAdapter);
		            filterList.setCacheColorHint(Color.WHITE);
//		            employeeList.setOnItemClickListener(new OnItemClickListener(){
//		                @Override
//						public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
//		                    // TODO Auto-generated method stub
//				 			Intent intent = new Intent(getApplicationContext(), TOMConDetailsActivity.class);			
//				 			Cursor cursor = (Cursor) adapter.getItem(position);
//		                    intent.putExtra("EMPLOYEE_ID", cursor.getInt(cursor.getColumnIndex("_id")));
//				 			index = cur.getString(cur.getColumnIndex("T_Assignment_Number"));
//				 			indx=cur.getString(cur.getColumnIndex("_id"));
//				 			Crashlytics.log(android.util.Log.ERROR,TAG,"indx " + indx);
//				 			screen_id="ContactsActivity";
//				 			 Bundle bundle = new Bundle();
//				 			 keyword ="from_delpage";
//				 	         bundle.putString("frm_page", keyword);		 	        
//				 	        intent.putExtras(bundle);          
//				            intent.putExtra(EXTRA_MESSAGE, index);
//				            startActivity(intent);
//						} 			 		
//		            });
//		            db.close();  
		            btn_submit.setEnabled(true);
		        }
      	       else{
  		            Toast.makeText(AssignedTransferActivity.this,"No outscan Assignments to transfer", Toast.LENGTH_LONG).show();
  		            btn_submit.setEnabled(false);
  		        }
		        }
		        else{
		            Toast.makeText(AssignedTransferActivity.this,"No outscan Deliveries for today,\n please contact Authorized Executive", Toast.LENGTH_LONG).show();
		btn_submit.setEnabled(false);
		}
//  		  }
//        else if(count_shw_filtr == 0)
//        {
//        	Intent goToNextActivity = new Intent(getApplicationContext(),TOMWelcomeActivity.class);
//			startActivity(goToNextActivity);
//        }
//        db.close();
  		}
    		else
    		{
    			Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity step3 shwsinglelist TOMLoginUserActivity.file is null ");
//    			onClickGoToHomePage();
    		}
    	}
    	catch(SQLiteException e){
    		e.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity SQLiteException shwsinglelist ");
//    		onClickGoToHomePage();
    	}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity UnsatisfiedLinkError shwsinglelist ");
//    		onClickGoToHomePage();
    	}
    	  finally 
    	  {
    		  if(cur !=null && !cur.isClosed())
    		  {
    			  cur.close();
		}
		if(cursor_outscan_dt!=null&&!cursor_outscan_dt.isClosed())
		{
    			  cursor_outscan_dt.close();
    		  }
    	      if (db != null && db.isOpen()) 
    	      {
    	    	  db.close();
    	      }
    	  }	 

    }
	
	
	 public void spinnerloadtrsfr_reason()
	 {
		 try
		 {
//			 Spinner s = (Spinner)findViewById(R.id.spinnerreason);
			 s_reason = (Spinner)findViewById(R.id.spinnerreason);

		ArrayList<String>trnsfr_reason_list=new ArrayList<String> ();
//			 reject_reason_list.add("ORDER CANCELLED");
			 trnsfr_reason_list.add("WRONGLY ASSIGNED");
//			 reject_reason_list.add("INSTRUCTED BY DISPATCH IN-CHARGE");
//			 reject_reason_list.add("INSTRUCTED BY W/H IN-CHARGE");
//			 reject_reason_list.add("INSTRUCTED BY BRANCH IN-CHARGE");
			 trnsfr_reason_list.add("SELECT TRANSFER REASON");
			 
	         final int trnsfr_reason_listsize = trnsfr_reason_list.size() - 1;
	         Crashlytics.log(android.util.Log.ERROR,TAG,"print spinner length "+ trnsfr_reason_listsize + trnsfr_reason_list);
//			ArrayAdapter mAdapter;
//			 mAdapter = new ArrayAdapter<String>(this,
//		        	    android.R.layout.simple_spinner_item, user_labels);
		       
		        
		        /**************ArrayAdapter for spinner********************/
		ArrayAdapter mAdapter = new ArrayAdapter(AssignedTransferActivity.this, android.R.layout.simple_spinner_item, trnsfr_reason_list)
		        		{
		        @Override
		        public int getCount() {
		            return(trnsfr_reason_listsize); // Truncate the list
		        }
		    };
		        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		        s_reason.setAdapter(mAdapter); 
		        s_reason.setSelection(trnsfr_reason_listsize); // Hidden item to appear in the spinner        
		        
		        s_reason.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_reason());
		 }
		 catch(Exception e)
		 {
			 Crashlytics.log(android.util.Log.ERROR,TAG,"Exception spinnerloadtrsfr_usr " + e.getMessage());
		 }
		 finally
		 {
			 if(db != null && db.isOpen())
			 {
				 db.close();
			 }
		 }
	 }
	
	
	//loading the transfer users to spinner
	 public void spinnerloadtrsfr_usr()
	 {
		 try
		 {
//		 Spinner s = (Spinner)findViewById(R.id.spinnerwhom);
			 s_trsfr =(Spinner)findViewById(R.id.spinnerwhom);
			 
			ArrayList<String> user_labels = new ArrayList<String> ();
			
			 db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
			 cr=db.rawQuery("select Distinct T_trnsfr_usr_nme,T_trnsfr_usr_id,_id from Transfer_User_MASTER where T_Cust_Acc_NO='" + session_CUST_ACC_CODE + "' group by T_trnsfr_usr_nme ", null);
		        //ORDER BY T_Hold_Reason_Code DESC
		        while(cr.moveToNext())
		        {
		        	trsfr_usr_name=cr.getString(cr.getColumnIndex("T_trnsfr_usr_nme"));
		        	trnsfr_usr_id=cr.getString(cr.getColumnIndex("T_trnsfr_usr_id"));
		        	 Crashlytics.log(android.util.Log.ERROR,TAG,"trsfr_usr_name "+ trsfr_usr_name  + trnsfr_usr_id);
		        	user_labels.add(trsfr_usr_name);
		        }
		        user_labels.add("SELECT ASSOCIATE NAME");
//			ArrayAdapter mAdapter;
//			 mAdapter = new ArrayAdapter<String>(this,
//		        	    android.R.layout.simple_spinner_item, user_labels);
		        final int listsize = user_labels.size() - 1;
		        Crashlytics.log(android.util.Log.ERROR,TAG,"print spinner length "+ listsize + user_labels);
		        /**************ArrayAdapter for spinner********************/
		        ArrayAdapter mAdapter = new ArrayAdapter(AssignedTransferActivity.this, android.R.layout.simple_spinner_item, user_labels)
		        		{
		        @Override
		        public int getCount() {
		            return(listsize); // Truncate the list
		        }
		    };
		mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s_trsfr.setAdapter(mAdapter);
		        
		        s_trsfr.setSelection(listsize); // Hidden item to appear in the spinner

		s_trsfr.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner_origin());
		 }
		 catch(Exception e)
		 {
			 Crashlytics.log(android.util.Log.ERROR,TAG,"Exception spinnerloadtrsfr_usr " + e.getMessage());
		 }
		 finally
		 {
			 if(cr !=null && !cr.isClosed())
	   		  {
				 cr.close();
	   		  }
			 if (db != null && db.isOpen()) 
	   	      {
	   	    	  db.close();
	   	      }
		 }
	 }
	
	 public class Listener_Of_Selecting_Room_Spinner_reason implements OnItemSelectedListener 
	 {
//	     static String RoomType;

	     @Override
	 	public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) 
	     {   
	         /** By using this you can get the position of item which you
	             have selected from the dropdown     **/ 
	     	try
	     	{
	         RoomType = (parent.getItemAtPosition(pos)).toString();
	         ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
	         spnr_reason=RoomType;
////	         Crashlytics.log(android.util.Log.ERROR,TAG,"spinner num " + num);
//	         spinner=RoomType.replace(" ", "%20");
//	         if(num.equals("Select") || num.equals("SELECT"))  
//	 	         {  
////	        	 dest_labels.removeAll(dest_labels);
////	 			 s_dest_code.setAdapter(mAdapter_dest);
//	 	         } 
//	         else if(!num.equals("Select") && !num.equals("SELECT"))
//	         {
//	 	         try
//	 			{
//	 	        	 db_insert=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH,session_DB_PWD, null);	
//	 	        	  c =db_insert.rawQuery("select _id,T_Dest_Centers from TBL_Destination_Centers",null);
//	 	        	 while(c.moveToNext())
//	 	        	 {
//	 	        		 String d_code=c.getString(c.getColumnIndex("T_Dest_Centers"));
////	 	        	    	Crashlytics.log(android.util.Log.ERROR,TAG,"d_code" + d_code);
//	 	        	 }
//	 	        	 int count=c.getCount();
//	 	        	 
////	 	        	 Crashlytics.log(android.util.Log.ERROR,TAG,"count " + count);
//	 	        	 c.close();
//	 	        	 db_insert.close();
//	 	        	 if(count == 0)
//	 	        	 {
//	 	 				if (loginTask != null && loginTask.getStatus() != AsyncTask.Status.FINISHED)
//	 	 	                loginTask.cancel(true);
//	 	 	            loginTask = new LongOperation(); //every time create new object, as AsynTask will only be executed one time.
//	 	 	            loginTask.execute();
//	 	        	 }
//	 	        	 else if(count > 0)
//	 	        	 {
//	 	        		 shw_dest_list();
//	 	        	 }
//	 	           
//	 			}
//	 			catch(Exception e)
//	 			{
//	 				Crashlytics.log(android.util.Log.ERROR,TAG,"OutboundBaggingMainActivity Exception loginTask " + e.getMessage());
//	 			}
//	     	}
	         }
	     	catch(Exception e)
	     	{
	     		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception OutboundBaggingMainActivity Listener_Of_Selecting_Room_Spinner " + e.getMessage());

	     	}
	     	catch(UnsatisfiedLinkError err) {
				err.getStackTrace();
				Crashlytics.log(android.util.Log.ERROR,TAG,"Error OutboundBaggingMainActivity UnsatisfiedLinkError ");

	     	}
	     	finally
	     	{
//	     		if(c !=null)
//	     		{
//	     			 c.close();
//	 	        	
//	     		}
//	     		if( db_insert!=null)
//	     		{
//	     			 db_insert.close();
//	     		}

	     	}
	     }
	     @Override
	 	public void onNothingSelected(AdapterView<?> parent) 
	     {           
	         // Do nothing.
	     }
	     
	 }
	 
	 public class Listener_Of_Selecting_Room_Spinner_origin implements OnItemSelectedListener 
	 {
//	     static String RoomType;

	     @Override
	 	public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) 
	     {   
	         /** By using this you can get the position of item which you
			  have selected from the dropdown     **/
			 try {
				 RoomType = (parent.getItemAtPosition(pos)).toString();
	         ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
	         spnr_trnsfr_usr=RoomType;

	         }
	     	catch(Exception e)
	     	{
	     		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception OutboundBaggingMainActivity Listener_Of_Selecting_Room_Spinner " + e.getMessage());

	     	}
	     	catch(UnsatisfiedLinkError err)
	     	{
	     		err.getStackTrace();
	     		Crashlytics.log(android.util.Log.ERROR,TAG,"Error OutboundBaggingMainActivity UnsatisfiedLinkError ");

	     	}
	     	finally
	     	{


	     	}
	     }
	     @Override
	 	public void onNothingSelected(AdapterView<?> parent) 
	     {           
	         // Do nothing.
	     }
	     
	 }
	 
		//*********************submit button**************************************************/
		public void OnSubmitClicked(View view) 
		{    	
			try
			{				
					gettransferlist();
			}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity submitClicked " + e.getMessage());
			//onClickGoToHomePage();
		}
		catch(UnsatisfiedLinkError err)
		{
			err.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");
			//onClickGoToHomePage();
//		        	    			finish();
		}
	    }
	 
		
		 public void gettransferlist()
		 {
				try
				{
			       
					if(dataAdapter.countryList != null)
				    {
				    	try
					    {

						    String str_amt = null ;
//						    String result;
						    int j=0;
						    double total_cod =0;
						    String cod=null;
						    responseText = new StringBuffer();
							 ArrayList<Country> countryList = dataAdapter.countryList;
//							    use for loop i=0 to < countryList.size()
							    for(int i=0;i<countryList.size();i++)
							    {
						    	 Country country = countryList.get(i);
							     if(country.isSelected())
							     {
							    	 responseText.append(country.getawb_num()+",");
							     }
							    }

								    session.createAssgnMultiple_num_Session(responseText.toString());

				    if(responseText.toString().isEmpty() || responseText.toString() == null)
				    {
				    	Toast.makeText(AssignedTransferActivity.this,"Please select data", Toast.LENGTH_LONG).show();
					} else if (!(responseText.toString().isEmpty()) && responseText.toString() != null )
				    {
//						initiatePopupWindow();
//				    	 if(num.equals("Select") || num.equals("SELECT USER"))  
//			   	         {  
//				        	Toast.makeText(AssignedTransferActivity.this,"Please select user", Toast.LENGTH_LONG).show();
//			   	         } 
//				        else if(!num.equals("Select") && !num.equals("SELECT USER"))  
//			   	         {
				    	Crashlytics.log(android.util.Log.ERROR,TAG,"selected spnr_trnsfr_usr "+spnr_trnsfr_usr + spnr_reason);
				    	if(spnr_trnsfr_usr.equals("SELECT ASSOCIATE NAME"))  
			   	         {  
				        	Toast.makeText(AssignedTransferActivity.this,"Please select associate name", Toast.LENGTH_LONG).show();
			   	         } 
				    	 else if(spnr_reason.equals("Select") || spnr_reason.equals("SELECT TRANSFER REASON"))  
			   	         {  
				        	Toast.makeText(AssignedTransferActivity.this,"Please select transfer reason", Toast.LENGTH_LONG).show();
			   	         } 
				        else if(!spnr_reason.equals("SELECT TRANSFER REASON") && !spnr_trnsfr_usr.equals("SELECT ASSOCIATE NAME"))  
			   	         {
				    	
				    	AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
					      
				        // Setting Dialog Title
				        alertDialog.setTitle("Alert");
				  
				        // Setting Dialog Message
				        alertDialog.setMessage("Are you sure you want to confirm ?");
				  
				        // On pressing Settings button
				        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,int which) 
				            {
				            	if(session_DB_PATH != null && session_DB_PWD != null)
						    	{
					    		Crashlytics.log(android.util.Log.ERROR,TAG,"selected paymnt trsfr val "+num);
				//		    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
						    		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
							    	 c =db.rawQuery("select T_trnsfr_usr_nme,T_trnsfr_usr_id,_id from Transfer_User_MASTER where T_trnsfr_usr_nme='" + spnr_trnsfr_usr + "'", null);
							    	 while(c.moveToNext())
							    	 {
							    		 transfr_usr_id=c.getString(c.getColumnIndex("T_trnsfr_usr_id"));
							    	 }
//							    	 c.close();
						    		String shw[] = responseText.toString().split(",");
								        for(int k = 0 ; k< shw.length ; k++)
								        {
								        	 result = shw[k];
								        	 Crashlytics.log(android.util.Log.ERROR,TAG,"result" +result +transfr_usr_id);
				//				        	Crashlytics.log(android.util.Log.ERROR,TAG,"prnt reslt "+ result + "responseText.toString() " + responseText.toString());
											String flg = "T";
//								        	 Assignment_Type="D";
								        	 Crashlytics.log(android.util.Log.ERROR,TAG,"trsfr_usr_name "+ trsfr_usr_name  + transfr_usr_id + "num" + num + "result" + result);
//								        	cursor_transfr_list=db.rawQuery("update TOM_Assignments set T_Filter='" + flg + "',T_trnsfr_usr_nme='" + num + "',T_trnsfr_usr_id='" + transfr_usr_id + "' WHERE T_Assignment_Number ='" + result +"' AnD T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
//								        	 cursor_transfr_list=db.rawQuery("update TOM_Assignments set T_Filter='" + flg + "',T_trnsfr_usr_nme='" + num + "',T_trnsfr_usr_id='" + transfr_usr_id + "' WHERE T_Assignment_Number ='" + result +"' AnD T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"'", null);
								        	 cursor_transfr_list=db.rawQuery("update TOM_Assignments set B_is_Completed='" + flg + "',T_trnsfr_usr_nme='" + spnr_trnsfr_usr + "',T_trnsfr_usr_id='" + transfr_usr_id + "',T_Filter_Reason='" + spnr_reason + "'  WHERE T_Assignment_Number ='" + result +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"'", null);
											 while(cursor_transfr_list.moveToNext())
											 {
				
											 }
				//							 int cnt = cursor_shippr_list.getCount();
//											 cursor_transfr_list.close();
				
								        }
				
									 
//									 db.close();
						    	}
						    	else
								{
									Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity session_DB_PATH is null ");
									//onClickGoToHomePage();
								}
								Intent goToNextActivity = new Intent(getApplicationContext(),AssignedTransferActivity.class);
				            	 Toast.makeText(AssignedTransferActivity.this, "Selected data is transfered", Toast.LENGTH_LONG).show();
								startActivity(goToNextActivity);
				            }
				        });
				  
				        // on pressing cancel button
				        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				            @Override
							public void onClick(DialogInterface dialog, int which) {
//			            	Intent goToNextActivity = new Intent(getApplicationContext(),TOMDel_pickup_screenActivity.class);
//			    			 startActivity(goToNextActivity);
				            dialog.cancel();
				            }
				        });
				  
				        // Showing Alert Message
				        alertDialog.show();
				    	
				  	    }
//				    }
				    }
				    }
				    catch(SQLiteException ex)
				    {
				    	ex.getStackTrace();
				    	//onClickGoToHomePage();
				    }
			    	catch(UnsatisfiedLinkError err)
			    	{
			    		err.getStackTrace();
			    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");
			    		//onClickGoToHomePage();
//				            	    			finish();
			    	}
				    	catch(Exception e)
						{
							e.getStackTrace();
							Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity submitClicked " + e.getMessage());
							//onClickGoToHomePage();
						}
				    	finally
				    	{
				    		if(cursor_transfr_list != null && !cursor_transfr_list.isClosed())
				    		{
				    			cursor_transfr_list.close();
				    		}
				    		if(c != null && !c.isClosed())
				    		{
				    			c.close();
				    		}
				    		if(db!=null && db.isOpen())
				    		{
				    			db.close();
				    		}
				    	}
				    }
				    else
				    {
				    	
					    
					    	Toast.makeText(AssignedTransferActivity.this,"No data to deliver", Toast.LENGTH_LONG).show();
					    
				    }
//				}
			}
			catch(Exception e)
			{
				e.getStackTrace();
				Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity submitClicked " + e.getMessage());
				//onClickGoToHomePage();
			}
			catch(UnsatisfiedLinkError err)
			{
				err.getStackTrace();
				Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");
				//onClickGoToHomePage();
//			        	    			finish();
			}
		 }

private class MyCustomAdapter extends ArrayAdapter<Country>
	 {
		 
		  private ArrayList<Country> countryList;

		 public MyCustomAdapter(Context context, int textViewResourceId,
								ArrayList<Country> countryList)
		  {
			  
		   super(context, textViewResourceId, countryList);
		     try
		      {
			   this.countryList = new ArrayList<Country>();
			   this.countryList.addAll(countryList);
			  }
				catch(Exception e)
				{
					e.getStackTrace();
					Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity ArrayList " + e.getMessage());
				}
		     catch(UnsatisfiedLinkError err)
		    	{
		    		err.getStackTrace();
		    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");
		    		//onClickGoToHomePage();
//		            	    			finish();
		    	}
		  }
		  
		  private class ViewHolder {
		   TextView awb_code,cname,pncde,cty,shpr_name,amt;
		   CheckBox name;
		  }
		  
		  @Override
		  public View getView(int position, View convertView, ViewGroup parent) {
		  
		   ViewHolder holder = null;
		   try
		   {
		   Log.v("ConvertView", String.valueOf(position));
		  
		   if (convertView == null) {
		   LayoutInflater vi = (LayoutInflater)getSystemService(
		     Context.LAYOUT_INFLATER_SERVICE);
		   convertView = vi.inflate(R.layout.del_filter_accept, null);
		  
		   holder = new ViewHolder();
		   holder.awb_code = (TextView) convertView.findViewById(R.id.awb);
		   holder.cname = (TextView) convertView.findViewById(R.id.firstName);
//		   holder.cty = (TextView) convertView.findViewById(R.id.city);
//		   holder.pncde = (TextView) convertView.findViewById(R.id.pincode);
//		   holder.amt = (TextView) convertView.findViewById(R.id.COD_Amount);
			   holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
//		   holder.shpr_name = (TextView) convertView.findViewById(R.id.shpr_nme);
		   convertView.setTag(holder);
		  
		    holder.name.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
		      CheckBox cb = (CheckBox) v ;
		      Country country = (Country) cb.getTag();
		      country.setSelected(cb.isChecked());
		     }
		    });
		   }
		   else {
		    holder = (ViewHolder) convertView.getTag();
		   }		  
		   Country country = countryList.get(position);
//		   holder.amt.setText(country.getamt());
		   holder.cname.setText(country.getcname());
//		   holder.cty.setText(country.getcty());
//		   holder.pncde.setText(country.getpncode());
//		   holder.shpr_name.setText(country.getshpr_name());
//		   holder.code.setText(" (" +  country.getcod() + ")" );
//		   holder.code.setTextColor(Color.RED);
		   holder.awb_code.setText(country.getawb_num());
		   holder.name.setChecked(country.isSelected());
		   holder.name.setTag(country);
		  
		  }
			catch(Exception e)
			{
				e.getStackTrace();
				Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity MyCustomAdapter " + e.getMessage());
			}
		   catch(UnsatisfiedLinkError err)
	    	{
	    		err.getStackTrace();
	    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");	    	
	    	}
		   return convertView;
		  
		  }
	 
		 }
	
	 public void OnClickedskip(View view) 
	    {	
	    	try
	    	{
	    		if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("DELIVERED") ))
            	{
//            		Intent goToNextActivity = new Intent(getApplicationContext(), TOMContactActivity.class);
//	    			Intent goToNextActivity = new Intent(getApplicationContext(), Assigned_Delivery_MainActivity.class);
//            		startActivity(goToNextActivity);
            	}
            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("UNDELIVERED")))
            	{
//            		Intent goToNextActivity = new Intent(getApplicationContext(), Assigned_Undelivery_MainActivity.class);
//            		startActivity(goToNextActivity);
            	}
            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("PAYMENT_DELIVERED")))
            	{
//            		Intent goToNextActivity = new Intent(getApplicationContext(), PaymentAssigned_NewActivity.class);
//            		startActivity(goToNextActivity);
            	}
            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("PAYMENT_UNDELIVERED")))
            	{
//            		Intent goToNextActivity = new Intent(getApplicationContext(), ShowPayment_failure_details.class);
//            		startActivity(goToNextActivity);
            	}
//	    	Intent goToNextActivity = new Intent(getApplicationContext(), TOMWelcomeActivity.class);
//			startActivity(goToNextActivity);

	    	}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception TOMDel_FilterMainActivity UnDeliverClicked " + e.getMessage());
			//onClickGoToHomePage();
		}
		catch(UnsatisfiedLinkError err)
		{
			err.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity UnsatisfiedLinkError ");
		}
	    }

	public void toggleMenu(View v)
	{
		Intent homeActivity = new Intent(this, HomeMainActivity.class);
		startActivity(homeActivity);
	}

	@Override
	public void onBackPressed() {

	}

	public void onclk_noti(View v)
	{
		Intent homeActivity = new Intent(this, NotificationActivity.class);
		startActivity(homeActivity);
	}

	public void onclk_trip(View v)
	{
		Intent homeActivity = new Intent(this, Start_End_TripMainActivity.class);
		startActivity(homeActivity);
	}
/*	 @Override
		public void onBackPressed() 
	    {
	        // do something on back.
	    	try
	    	{
	    		if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("DELIVERED") ))
            	{
//            		Intent goToNextActivity = new Intent(getApplicationContext(), TOMContactActivity.class);
	    			Intent goToNextActivity = new Intent(getApplicationContext(), Assigned_Delivery_MainActivity.class);
            		startActivity(goToNextActivity);
            	}
            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("UNDELIVERED")))
            	{
            		Intent goToNextActivity = new Intent(getApplicationContext(), Assigned_Undelivery_MainActivity.class);
            		startActivity(goToNextActivity);
            	}
            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("PAYMENT_DELIVERED")))
            	{
            		Intent goToNextActivity = new Intent(getApplicationContext(), PaymentAssigned_NewActivity.class);
            		startActivity(goToNextActivity);
            	}
            	else  if(FORM_TYPE_VAL != null && (FORM_TYPE_VAL.equals("PAYMENT_UNDELIVERED")))
            	{
            		Intent goToNextActivity = new Intent(getApplicationContext(), ShowPayment_failure_details.class);
            		startActivity(goToNextActivity);
            	}
//	    	Intent goToNextActivity = new Intent(getApplicationContext(), TOMDel_pickup_screenActivity.class);
//			startActivity(goToNextActivity);
	    	}
			catch (Exception e)
			{           
		       e.printStackTrace();
		       Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity onBackPressed " + e.getMessage());
//		       Intent goToNextActivity = new Intent(getApplicationContext(), TOMContactActivity.class);
//				startActivity(goToNextActivity);	       
			}
	    	catch(UnsatisfiedLinkError err)
	    	{
	    		err.getStackTrace();
	    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");
//	    		Intent goToNextActivity = new Intent(getApplicationContext(), TOMContactActivity.class);
//	    		startActivity(goToNextActivity);
//	        	        	    			finish();
	    	}
	    }*/
	    
	    @Override
		public void onDestroy() {
			try {
				super.onDestroy();
				stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
	    	stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
	    	android.os.Process.killProcess(android.os.Process.myPid());
	    	System.exit(0);
	    	}
	    	catch (Exception e)
			{           
		       e.printStackTrace();
		       Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity onDestroy " + e.getMessage());
//		       stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
//		    	stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
//		    	android.os.Process.killProcess(android.os.Process.myPid());
//		    	System.exit(0);
		       
			}
	    	catch(UnsatisfiedLinkError err)
	    	{
	    		err.getStackTrace();
	    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");
//	    		stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
//	        	stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
//	        	android.os.Process.killProcess(android.os.Process.myPid());
//	        	System.exit(0);
//	        	        	    			finish();
	    	}
	    }

	  //**********************modification for menu options(Home & Logout)**************************
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu){
	        MenuInflater menuInflater = getMenuInflater() ;
	        menuInflater.inflate(R.menu.menu, menu);
//			getMenuInflater().inflate(R.menu.main, menu);
	        return true;
	    }

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item){
			// Handle action bar actions click
			/*switch (item.getItemId()) {
				case R.id.action_settings:
					return true;
				case R.id.trip_started:
					Intent intent = new Intent(AssignedTransferActivity.this, Start_End_TripMainActivity.class);
					startActivity(intent);
					return true;
				default:
					return super.onOptionsItemSelected(item);*/
	        switch (item.getItemId()){
	            case R.id.menu_home:
	                onClickGoToHomePage();
	                return true;
	            case R.id.menu_log_out:
	                onClickLogOut();
	                return true;
	            default:
	                return super.onOptionsItemSelected(item);
	        }
	    }

	    private void onClickLogOut()
	    {
	    	try
	    	{
	        Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
	        logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivity(logOutActivity);
		    }
			catch (Exception e)
			{           
		       e.printStackTrace();
		       Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity onClickLogOut " + e.getMessage());       
			}
	    	catch(UnsatisfiedLinkError err)
	    	{
	    		err.getStackTrace();
	    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");
	    	}
	    }

	    private void onClickGoToHomePage()
	    {
	    	try
	    	{
//	        Intent homePageActivity = new Intent(this,TOMDel_pickup_screenActivity.class);
////	        homePageActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//	        startActivity(homePageActivity);
		    }
			catch (Exception e)
			{           
		       e.printStackTrace();
		       Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedTransferActivity onClickGoToHomePage " + e.getMessage());	       
			}
	    	catch(UnsatisfiedLinkError err)
	    	{
	    		err.getStackTrace();
	    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedTransferActivity UnsatisfiedLinkError ");
	    	}
	    }
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.assigned_transfer, menu);
//		return true;
//	}

}

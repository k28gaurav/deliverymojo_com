package com.traconmobi.tom;

public class Country 
{	
	 String Assign_num = null;
	 String scan_num = null;
	 String awb_num=null;
	 String cname = null;
	 String cty = null;
	 String pncode = null;
	 String amt = null;
	 String shpr_name = null;
	 String cod = null;
//	 String consgn_nme = null;
	 String shw_nme = null;
	 String awb_numbr=null;
	 String congsnee_name=null;
	 String cod_collected=null;
	 String recvr_name=null;
	 
	 
	 boolean selected = false;
	   
	 public Country(String Assign_num, String cod, String shw_nme, boolean selected) 
	 {		 
	  super();
	  try
		{
		  this.Assign_num = Assign_num;
		  this.cod = cod;
		  this.shw_nme = shw_nme;
//		  this.shpr_nme = shpr_nme;
		  this.selected = selected;
		}
		 catch(Exception e)
		 {
		 	e.getStackTrace();
		 	System.out.println("Exception Country " + e.getMessage());
		 }
	 }
	   
	 public Country(String scan_num) {
		// TODO Auto-generated constructor stub
		 super();
		  try
			{
			  this.scan_num = scan_num;
			 
			}
			 catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception Country " + e.getMessage());
			 }
	}

	 public Country(String awb_num, String cname, String cty, String pncode,
			String amt, String shpr_name, boolean selected) 
	 {
		// TODO Auto-generated constructor stub
		 super();
		  try
			{
			  this.awb_num = awb_num;
			  this.cname = cname;
			  this.cty = cty;
			  this.pncode = pncode;
			  this.amt = amt;
			  this.shpr_name = shpr_name;
			  this.selected = selected;
			}
			 catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception Country " + e.getMessage());
			 }
	}

	 public Country(String awb_num, String cname,boolean selected) 
		 {
			// TODO Auto-generated constructor stub
			 super();
			  try
				{
				  this.awb_num = awb_num;
				  this.cname = cname;
				  this.cty = cty;
				  this.pncode = pncode;
				  this.amt = amt;
				  this.shpr_name = shpr_name;
				  this.selected = selected;
				}
				 catch(Exception e)
				 {
				 	e.getStackTrace();
				 	System.out.println("Exception Country " + e.getMessage());
				 }
		}

	 
	 public Country(String awb_numbr, String congsnee_name, String cod_collected,
			String recvr_name, boolean selected) 
	 {
		// TODO Auto-generated constructor stub
		 super();
		  try
			{
			  this.awb_numbr = awb_numbr;
			  this.congsnee_name = congsnee_name;
//			  this.cty = cty;
//			  this.pncode = pncode;
			  this.cod_collected = cod_collected;
			  this.recvr_name = recvr_name;
			  this.selected = selected;
			}
			 catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception Country " + e.getMessage());
			 }
	 }

	/********************************/
	 
	 public String getawb_num() 
	 {
	  return awb_num;
	 }
	 public void setawb_num(String awb_num) 
	 {
		 try
			{
			 this.awb_num = awb_num;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setAssign_num " + e.getMessage());
			 }
	 }
	 
	 public String getcname() 
	 {
	  return cname;
	 }
	 public void setcname(String cname) 
	 {
		 try
			{
			 this.cname = cname;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setAssign_num " + e.getMessage());
			 }
	 }
	 
	 public String getcty() 
	 {
	  return cty;
	 }
	 public void setcty(String cty) 
	 {
		 try
			{
			 this.cty = cty;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setAssign_num " + e.getMessage());
			 }
	 }
	 
	 public String getpncode() 
	 {
	  return pncode;
	 }
	 public void setpncode(String pncode) 
	 {
		 try
			{
			 this.pncode = pncode;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setAssign_num " + e.getMessage());
			 }
	 }
	 
	 public String getamt() 
	 {
	  return amt;
	 }
	 public void setamt(String amt) 
	 {
		 try
			{
			 this.amt = amt;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setAssign_num " + e.getMessage());
			 }
	 }
	 
	 public String getshpr_name() 
	 {
	  return shpr_name;
	 }
	 public void setshpr_name(String shpr_name) 
	 {
		 try
			{
			 this.shpr_name = shpr_name;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setAssign_num " + e.getMessage());
			 }
	 }
	 
	 /************************************/
	 public String getawb_numbr() 
	 {
	  return awb_numbr;
	 }
	 public void setawb_numbr(String awb_numbr) 
	 {
		 try
			{
			 this.awb_numbr = awb_numbr;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setawb_numbr " + e.getMessage());
			 }
	 }
	 
	 public String getcongsnee_name() 
	 {
	  return congsnee_name;
	 }
	 public void setcongsnee_name(String congsnee_name) 
	 {
		 try
			{
			 this.congsnee_name = congsnee_name;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setcongsnee_name " + e.getMessage());
			 }
	 }
	 
	 public String getcod_collected() 
	 {
	  return cod_collected;
	 }
	 public void setcod_collected(String cod_collected) 
	 {
		 try
			{
			 this.cod_collected = cod_collected;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setcod_collected " + e.getMessage());
			 }
	 }
	 
	 public String getrecvr_name() 
	 {
	  return recvr_name;
	 }
	 public void setrecvr_name(String recvr_name) 
	 {
		 try
			{
			 this.recvr_name = recvr_name;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setrecvr_name " + e.getMessage());
			 }
	 }
	 /**************************************/
	 
	public String getscan_num() 
	 {
	  return scan_num;
	 }
	 public void setscan_num(String scan_num) 
	 {
		 try
			{
			 this.scan_num = scan_num;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setAssign_num " + e.getMessage());
			 }
	 }
	public String getAssign_num() 
	 {
	  return Assign_num;
	 }
	 public void setAssign_num(String Assign_num) 
	 {
		 try
			{
			 this.Assign_num = Assign_num;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception setAssign_num " + e.getMessage());
			 }
	 }
	 public String getcod() {
	  return cod;
	 }
	 public void setcod(String cod) 
	 {
		 try
			{
			 this.cod = cod;
			}
		 catch(Exception e)
		 {
		 	e.getStackTrace();
		 	System.out.println("Exception setcod " + e.getMessage());
		 }
	 }
	  
	 public String getshw_nme() 
	 {
	  return shw_nme;
	 }
	 public void setshw_nme(String shw_nme) 
	 {
		 try
			{
			 this.shw_nme = shw_nme;
			}
		  catch(Exception e)
			 {
			 	e.getStackTrace();
			 	System.out.println("Exception consgn_nme " + e.getMessage());
			 }
	 }
	 
//	 public String getshpr_nme() 
//	 {
//	  return shpr_nme;
//	 }
//	 public void setshpr_nme(String shpr_nme) 
//	 {
//		 try
//			{
//			 this.shpr_nme = shpr_nme;
//			}
//		  catch(Exception e)
//			 {
//			 	e.getStackTrace();
//			 	System.out.println("Exception shpr_nme " + e.getMessage());
//			 }
//	 }
	 
	 public boolean isSelected() {
	  return selected;
	 }
	 public void setSelected(boolean selected) 
	 {
		 try
		 {
			 this.selected = selected;
		 }
		 catch(Exception e)
		 {
		 	e.getStackTrace();
		 	System.out.println("Exception setSelected " + e.getMessage());
		 }
	 }

	}
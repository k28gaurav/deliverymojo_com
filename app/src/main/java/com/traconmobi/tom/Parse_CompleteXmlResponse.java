package com.traconmobi.tom;

/**
 * Created by LENOVO on 11/19/2015.
 */
/*public class Parse_CompleteXmlResponse {
}*/

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Xml;

public class Parse_CompleteXmlResponse {

    private ArrayList<HashMap<String,String>> parsedCompleteList;
    protected SQLiteDatabase db;
    private StringReader xmlReader;

    public String returnd_assgnidValue=null;
//    public String DEL_returnd_assgn_no_Value=null;
    public String issyncedValue=null;

    public static String ASSIGNID,DEL_ASSIGN_NUM,IS_SYNCED;

    private final String starttagList = "POD";
    private final String issynced = "SYNC";
//    private final String returnd_assgnid = "AWB NO";
    private final String returnd_assgnid = "AWB_ID";
//    private final String returnd_assgn_rsn="REASON";

    public String TAG="Parse_CompleteXmlResponse";

    public Parse_CompleteXmlResponse(String xml)
    {

        xmlReader = new StringReader(xml);

    }

    public void parse() throws XmlPullParserException, IOException
    {


        TreeMap<String, String> CompleteListObj = null;
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(xmlReader);
        int eventType = parser.getEventType();

        CompleteListObj = new TreeMap<String, String>();

        parsedCompleteList = new ArrayList<HashMap<String, String>>();
        while (eventType != XmlPullParser.END_DOCUMENT)
        {
            String xmlNodeName = parser.getName();
            if (XmlPullParser.START_TAG == eventType)
            {
                xmlNodeName = parser.getName();
                if (xmlNodeName.equalsIgnoreCase(issynced))
                {
                    issyncedValue = parser.nextText().toString();
                    CompleteListObj.put("issynced", issyncedValue);
                    Log.d("issyncedValue", issyncedValue);
                }
                else if (xmlNodeName.equalsIgnoreCase(returnd_assgnid))
                {
                    returnd_assgnidValue = parser.nextText().toString();
                    CompleteListObj.put("returnd_assgnid", returnd_assgnidValue);
                    Log.e(TAG, "returnd_assgnidValue " + returnd_assgnidValue);
//				Log.d("OUTSCAN_DTTME",outscan_dttmeValue);
                }

               

            } else if (XmlPullParser.END_TAG == eventType)
            {
                if (xmlNodeName.equalsIgnoreCase(starttagList))
                {


                    parsedCompleteList.add(new HashMap<String, String>(

                            CompleteListObj));


                    for (Map.Entry<String, String> entry : CompleteListObj.entrySet())
                    {

//                        DEL_ASSIGN_NUM=CompleteListObj.get("DEL_returnd_assgn_no");
                        IS_SYNCED=CompleteListObj.get("issynced");
                        ASSIGNID =CompleteListObj.get("returnd_assgnid");
                        Log.e(TAG, "ASSIGNID " + ASSIGNID + IS_SYNCED);
//			    Log.i("DEL_AWBID", DEL_AWBID);
//			    POD_awbno=DELListObj.get("outscan_awbno");

                    }


                    CompleteListObj.clear();


                }

            }
            eventType = parser.next();
        }
    }

    @SuppressWarnings("unused")
    private String readText(XmlPullParser parser) throws IOException,

            XmlPullParserException {

        String result = "";

        if (parser.next() == XmlPullParser.TEXT) {

            result = parser.getText();

            Log.d("wat is the result",result);

            parser.nextTag();

        }

        return result;

    }

    public ArrayList getparsedCompleteList() {

        return parsedCompleteList;

    }
}

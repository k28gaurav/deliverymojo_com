package com.traconmobi.tom;

//import org.eclipse.jdt.annotation.Nullable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by hp1 on 21-01-2015.
 */
public class DeliveryDtlsFragment<EmployeeAction>  extends Fragment {

    SessionManager session;
    public String SESSION_TRANSPORT, employeeId, session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME, session_DB_PATH, session_DB_PWD;
    TinyDB tiny;
    protected SQLiteDatabase db;
    protected Cursor cur, cursor, sync_count, c_amt_val, cursor_outscan_dt, cr_unattempt_del, cr, cursor_srch, c_chk_consgnee, c_chk_custmr, cursor_shippr_list;
    public static String sync_cnt, sync_cnt_shw, cnt, keyword, keyword_val, Page_Name, screen_id, Assignment_Type, fltr_type, Assgn_type, t_u_id, Assgn_flag, cod_amt_val;
    String strFilter = "", index, address1, address2, uname, addr1, addr2, city_detail, celphno, pincode, awb_outscantime, awb_outscan_dt, cod_flag, cod_amt, str1;
    TextView consignee_Name, consignee_address, codval;
    protected List<TOMTransactAction> actions;
    protected EmployeeActionAdapter adapter;
    public String consignee_Nme, consignee_addr, consgnee;
    ListView lv;
    Button btn_undel, btn_del;
    StringBuffer responseText;
    public String TAG = "DeliveryDtlsFragment";
    public static int total_awbs;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_assgncod, container, false);
        try {
            //Intializing Fabric
            Fabric.with(getActivity(), new Crashlytics());
            // Session class instance
            session = new SessionManager(getActivity().getApplicationContext());
            //session.setPosition(0);

            // Session class instance
            tiny = new TinyDB(getActivity().getApplicationContext());

            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);

            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);

            SESSION_TRANSPORT = tiny.getString("transport_val");
            Crashlytics.log(android.util.Log.ERROR, TAG, "SESSION_TRANSPORT " + SESSION_TRANSPORT);

            employeeId = this.getArguments().getString("orderid");//getIntent().getStringExtra(TOMContactActivity.EXTRA_MESSAGE);
//        Toast.makeText(getActivity(),"hello"+ employeeId, Toast.LENGTH_LONG).show();
            consignee_Name = (TextView) v.findViewById(R.id.employeeName);
            consignee_address = (TextView) v.findViewById(R.id.address);
            codval = (TextView) v.findViewById(R.id.txtrs);
            lv = (ListView) v.findViewById(R.id.list);
            shwsinglelist();

            btn_undel = (Button) v.findViewById(R.id.btnundel);
            btn_undel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG, "Undel called");
                    try {
                        String str_amt = null;
                        String result;
                        int j = 0;
                        double total_cod = 0;
                        String cod = null;
                        responseText = new StringBuffer();
                        responseText.append(employeeId + ",");
                        Crashlytics.log(android.util.Log.ERROR, TAG, "shw accepted assigned list " + responseText.toString());
                        session.createAssgnMultiple_num_Session(responseText.toString());
                        if (responseText.toString().isEmpty() || responseText.toString() == null) {
                            Toast.makeText(getActivity(), "Please select data", Toast.LENGTH_LONG).show();
                        } else if (!(responseText.toString().isEmpty()) && responseText.toString() != null) {
                            try {
                               // btn_undel.setEnabled(false);
//                            btn_undel.setBackgroundColor(Color.GRAY);
                                if (session_DB_PATH != null && session_DB_PWD != null) {
                                    if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {
                                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                                        // Setting Dialog Title
                                        alertDialog.setTitle("Alert");
                                        alertDialog.setCancelable(false);
                                        // Setting Dialog Message
                                        alertDialog.setMessage("Please start the trip");
                                        // On pressing Settings button
                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                                goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_DEL");
                                                btn_undel.setEnabled(true);
                                                dialog.dismiss();
                                                startActivity(goToNextActivity);
                                            }
                                        });

                                // Showing Alert Message
                                        alertDialog.create().show();
                                    } else {
                                        if (SESSION_TRANSPORT.equals("BIKER")) {
                                            Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleUndelActivity.class);
                                            session.createAssgnMultiple_num_Session(responseText.toString());
                                            total_awbs = j;
                                            str_amt = "0";
                                            session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);

                                            startActivity(goToNextActivity);
                                        } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                                            Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleUndelActivity.class);
                                            session.createAssgnMultiple_num_Session(responseText.toString());
                                            total_awbs = j;
                                            str_amt = "0";
                                            session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);

                                            startActivity(goToNextActivity);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Assigned_Delivery_MainActivity customer locate " + e.getMessage());
                            } finally {
                                if (c_chk_consgnee != null && !c_chk_consgnee.isClosed()) {
                                    c_chk_consgnee.close();
                                }
                                if (c_chk_custmr != null && !c_chk_custmr.isClosed()) {
                                    c_chk_custmr.close();
                                }
                                if (db != null && db.isOpen()) {
                                    db.close();
                                }
                            }
                    }
                    } catch (SQLiteException ex) {
                        ex.getStackTrace();
                    } catch (UnsatisfiedLinkError err) {
                        err.getStackTrace();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity UnsatisfiedLinkError ");
                  }
                }
            });

            btn_del = (Button) v.findViewById(R.id.btndel);
            btn_del.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG, "Deldel called");
                    try {
                       try {
                            String str_amt = null;
                            String result;
                            int j = 0;
                            double total_cod = 0;
                            String cod = null;
                            responseText = new StringBuffer();
                         responseText.append(employeeId + ",");
                            Crashlytics.log(android.util.Log.ERROR, TAG, "shw accepted assigned list " + responseText.toString());
                            session.createAssgnMultiple_num_Session(responseText.toString());
                            if (responseText.toString().isEmpty() || responseText.toString() == null) {
                                Toast.makeText(getActivity(), "Please select data", Toast.LENGTH_LONG).show();
                            } else if (!(responseText.toString().isEmpty()) && responseText.toString() != null) {
                                try {
                                   // btn_del.setEnabled(false);
                                    if (session_DB_PATH != null && session_DB_PWD != null) {
                                        if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {
                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                                            // Setting Dialog Title
                                            alertDialog.setTitle("Alert");

                                            alertDialog.setCancelable(false);
                                            // Setting Dialog Message
                                            alertDialog.setMessage("Please start the trip");

                                            // On pressing Settings button
                                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                                    //goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_UNDEL");
//								Toast.makeText(AssignedMultipleUndelActivity.this,"Please locate the customer", Toast.LENGTH_LONG).show();
                                                    btn_del.setEnabled(true);
                                                    dialog.dismiss();
//						 saveButton.setBackgroundResource(R.drawable.button);
                                                    startActivity(goToNextActivity);
                                                }
                                            });

                                            // Showing Alert Message
                                            alertDialog.create().show();
                                        } else {
                                            if (SESSION_TRANSPORT.equals("BIKER")) {
                                                if (session_DB_PATH != null && session_DB_PWD != null) {
//		    			    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                    String shw_res[] = responseText.toString().split(",");
                                                    for (int k = 0; k < shw_res.length; k++) {
                                                        result = shw_res[k];
                                                        Crashlytics.log(android.util.Log.ERROR, TAG, "prnt reslt " + result + "responseText.toString() " + responseText.toString());
                                                        Assignment_Type = "D";
//		    					        	cursor_shippr_list=db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
//		    					        	 cursor_shippr_list=db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
                                                        cursor_shippr_list = db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result + "' AND T_Assignment_Type='" + Assignment_Type + "' AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID + "' ", null);
                                                        while (cursor_shippr_list.moveToNext()) {
                                                            String awb_num = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Assignment_Number"));
                                                            cod = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("F_Amount"));
                                                            String get_consgnee = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Consignee_Name"));
                                                        }
                                                        int cnt = cursor_shippr_list.getCount();
                                                        total_awbs = cursor_shippr_list.getCount();
                                                        cursor_shippr_list.close();
                                                        total_cod = total_cod + Double.valueOf(cod);
                                                        NumberFormat formatter = new DecimalFormat("###.00");
                                                        str_amt = formatter.format(total_cod);
                                                    }


                                                    db.close();
                                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleDelActivity.class);
                                                    goToNextActivity.putExtra("FORM_TYPE4_VAL", "FORM_MULTIPLE_DEL");
                                                    session.createAssgnMultiple_num_Session(responseText.toString());

                                                    total_awbs = j;

                                                    session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                                                    startActivity(goToNextActivity);

                                                } else {
                                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity session_DB_PATH is null ");
                                                    //onClickGoToHomePage();
                                                }
                                            } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                                                if (session_DB_PATH != null && session_DB_PWD != null) {
//		    			    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                    String shw_res[] = responseText.toString().split(",");
                                                    for (int k = 0; k < shw_res.length; k++) {
                                                        result = shw_res[k];
                                                        Crashlytics.log(android.util.Log.ERROR, TAG, "prnt reslt " + result + "responseText.toString() " + responseText.toString());
                                                        Assignment_Type = "D";
//		    					        	cursor_shippr_list=db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                                                        cursor_shippr_list = db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result + "' AND T_Assignment_Type='" + Assignment_Type + "' AND B_is_Completed IS NULL  AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID + "' ", null);
                                                        while (cursor_shippr_list.moveToNext()) {
                                                            String awb_num = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Assignment_Number"));
                                                            cod = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("F_Amount"));
                                                            String get_consgnee = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Consignee_Name"));
                                                        }
                                                        int cnt = cursor_shippr_list.getCount();
                                                        total_awbs = cursor_shippr_list.getCount();
                                                        cursor_shippr_list.close();
                                                        total_cod = total_cod + Double.valueOf(cod);
                                                        NumberFormat formatter = new DecimalFormat("###.00");
                                                        str_amt = formatter.format(total_cod);
                                                    }


                                                    db.close();
                                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleDelActivity.class);
                                                    goToNextActivity.putExtra("FORM_TYPE4_VAL", "FORM_MULTIPLE_DEL");
                                                    session.createAssgnMultiple_num_Session(responseText.toString());

                                                    total_awbs = j;

                                                    session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                                                    startActivity(goToNextActivity);

                                                } else {
                                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity session_DB_PATH is null ");
                                                    //onClickGoToHomePage();
                                                }
                                            }

                                        }
                                    }
                                } catch (Exception e) {
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Assigned_Delivery_MainActivity customer locate " + e.getMessage());
                                } finally {
                                    if (c_chk_consgnee != null && !c_chk_consgnee.isClosed()) {
                                        c_chk_consgnee.close();
                                    }
                                    if (c_chk_custmr != null && !c_chk_custmr.isClosed()) {
                                        c_chk_custmr.close();
                                    }
                                    if (db != null && db.isOpen()) {
                                        db.close();
                                    }
                                }
                            }
                        } catch (SQLiteException ex) {
                            ex.getStackTrace();
                            //onClickGoToHomePage();
                        } catch (UnsatisfiedLinkError err) {
                            err.getStackTrace();
                            Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity UnsatisfiedLinkError ");
                            //onClickGoToHomePage();
//		            	    			finish();
                        }
                    } catch (Exception e) {
                        e.getStackTrace();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Assigned_Delivery_MainActivity submitClicked " + e.getMessage());
                        //onClickGoToHomePage();
                    } catch (UnsatisfiedLinkError err) {
                        err.getStackTrace();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity UnsatisfiedLinkError ");
                        //onClickGoToHomePage();
//	        	    			finish();
                    }
                }
            });
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());
        }
        return v;
    }

    class EmployeeActionAdapter extends ArrayAdapter<TOMTransactAction> {
        EmployeeActionAdapter() {
            super(getActivity(), R.layout.activity_tomcontacts_action, actions);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            TOMTransactAction action = actions.get(position);
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.activity_tomcontacts_action, parent, false);
            TextView label = (TextView) view.findViewById(R.id.label);
            label.setText(action.getLabel());
            TextView data = (TextView) view.findViewById(R.id.data);
            data.setText(action.getData());
            return view;
        }
    }

    public void shwsinglelist() {
        try {
            /****************showing the details of the consignee**************************/
            if (session_DB_PATH != null && session_DB_PWD != null) {
                db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                cursor = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Assignment_Type,emp.T_Consignee_Name, emp.T_Address_Line1, emp.T_Address_Line2, emp.T_City , emp.T_Contact_number , emp.T_Pincode,emp.F_Amount FROM TOM_Assignments   emp LEFT OUTER JOIN TOM_Assignments  mgr ON emp._id = mgr._id  where emp.T_Assignment_Number = ?",
                        new String[]{"" + employeeId});

                String cnt = String.valueOf(cursor.getCount());
                cursor.moveToFirst();
                actions = new ArrayList<TOMTransactAction>();
                String awb = cursor.getString(cursor.getColumnIndex("T_Assignment_Number"));
                String assignmnt_type = cursor.getString(cursor.getColumnIndex("T_Assignment_Type"));
//                float cod_amt_val=0;
                cod_amt_val = cursor.getString(cursor.getColumnIndex("F_Amount"));
                Crashlytics.log(android.util.Log.ERROR, TAG, "print cod_amt_val" + cod_amt_val);
                consignee_Nme = cursor.getString(cursor.getColumnIndex("T_Consignee_Name"));
                consignee_addr = cursor.getString(cursor.getColumnIndex("T_Address_Line1"))//
                        + " " + cursor.getString(cursor.getColumnIndex("T_Address_Line2")) + "\n" + cursor.getString(cursor.getColumnIndex("T_City")) + " " + cursor.getString(cursor.getColumnIndex("T_Pincode"));
                if (consignee_Name != null && consignee_addr != null && cod_amt_val != null) {
                    consignee_Name.setText(consignee_Nme);
                    consignee_address.setText(consignee_addr);
                    codval.setText("Rs." + cod_amt_val);
                } else {

                }

                Log.d("ED", consignee_Name.toString());
                cursor.close();
                db.close();
            } else {
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMConDetailsActivity step 1 TOMLoginUserActivity.file is null ");
            }
        } catch (SQLiteException e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMContactActivity SQLiteException shwsinglelist ");
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMContactActivity UnsatisfiedLinkError shwsinglelist ");
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    @Override
    public void onAttach(Activity act){
        super.onAttach(act);
        Log.e(TAG, "onAttach called");
    }
     @Override
    public void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         Log.e(TAG, "onCreate called");
         /*tiny = new TinyDB(getActivity().getApplicationContext());
         SESSION_TRANSPORT = tiny.getString("transport_val");*/
     }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(TAG, "onActivityCreated called");
    }

    @Override
    public void onStart(){
        super.onStart();
        Log.e(TAG, "on Start called");
        SESSION_TRANSPORT = tiny.getString("transport_val");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "on Resume called");
    }
}
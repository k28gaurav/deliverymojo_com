package com.traconmobi.tom;


import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.ReplicationFilter;
import com.couchbase.lite.SavedRevision;
import com.couchbase.lite.UnsavedRevision;
import com.couchbase.lite.View;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.replicator.Replication;
import com.couchbase.lite.util.Log;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.app.App;
import com.traconmobi.tom.http.HttpHandler;
import com.traconmobi.tom.rest.RestClient;
import com.traconmobi.tom.rest.model.ApiResponse;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

public class CouchBaseDBHelper implements Replication.ChangeListener {
    private static CouchBaseDBHelper instance;
    Database database;
    boolean previousData = false;
//    String TAG = "CouchDBHelper";
    Manager manager;
    Document doc;
    String DB_NAME = "deliverymojocom";
    SessionManager sManager = null;
    Context ctx = null;
    private static HashMap<String, String> storeDocId = new HashMap<>();
    double lat, lng;
    public String TAG="CouchBaseDBHelper";
    private static int countShipment = 0;
    private static String scanItemsKey="ScanItems";

    public CouchBaseDBHelper(Context context) {
        ctx = context;
        try {
            try {
                //Intializing Fabric
                Fabric.with(ctx, new Crashlytics());
                sManager = new SessionManager(ctx);
                database = getDatabaseInstance();
                manager = getManagerInstance();
                startReplications();
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "CouchBaseDBHelper" + e.getMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "CouchBaseDBHelper" + e.getMessage());
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "CouchBaseDBHelper" + e.getMessage());
        }
    }

    private URL createSyncURL() {
        URL syncURL = null;
        String host = "ec2-52-77-159-96.ap-southeast-1.compute.amazonaws.com";
        String port = "4984";
        try {
            syncURL = new URL("http://" + host + ":" + port + "/" + DB_NAME);
        } catch (MalformedURLException me) {
            me.printStackTrace();
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "createSyncURL" + e.getMessage());
        }
        return syncURL;
    }

    private void startReplications() {
        try {
            if (database == null) {
                Log.e(TAG, "getDatabase Instance");
                database = getDatabaseInstance();
            }
            if (database != null && !database.isOpen()) {
                database.open();
            }
            Replication pushReplication = database.createPushReplication(this.createSyncURL());
            pushReplication.setContinuous(true);
            pushReplication.start();
        } catch (Exception e) {
            Log.e(TAG, "in CouchDB in start replication" + database + "Message" + e.getMessage());
            Crashlytics.log(android.util.Log.ERROR, TAG, "startReplications" + e.getMessage());
        }
    }

    @Override
    public void changed(Replication.ChangeEvent event)
    {
        try {
            Replication replication = event.getSource();
            Log.d(TAG, "Replication : " + replication + " changed.");
            if (!replication.isRunning()) {
                String msg = String.format("Replicator %s not running", replication);
                Log.d(TAG, msg);
            } else {
                int processed = replication.getCompletedChangesCount();
                int total = replication.getChangesCount();
                String msg = String.format("Replicator processed %d / %d", processed, total);
                Log.d(TAG, msg);
            }

            if (event.getError() != null) {
                showError("Sync error", event.getError());
            }
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "changed" + e.getMessage());
        }
    }

    public void showError(final String errorMessage, final Throwable throwable)
    {
        try
        {
        String msg = String.format("%s: %s", errorMessage, throwable);
        Log.e(TAG, msg, throwable);
//        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "showError" + e.getMessage());
        }
    }

    public Database getDatabaseInstance() throws CouchbaseLiteException
    {
        try
        {
        if ((database == null) & (manager != null)) {
            database = manager.getDatabase(DB_NAME);
            Log.e(TAG, "Database instance: " + database);
        }
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "getDatabaseInstance" + e.getMessage());
        }
        return database;
    }

    public Manager getManagerInstance() throws IOException
    {
        try
        {
        if (manager == null) {
            manager = new Manager(new AndroidContext(ctx), Manager.DEFAULT_OPTIONS);
        }
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "getManagerInstance" + e.getMessage());
        }
        return manager;
    }

    public void deleteDatabaseList(Context context)
    {
        List<File>  databaseList = new ArrayList<>();
        try
        {
        // look for standard sqlite databases in the databases dir
        String[] contextDatabases = context.databaseList();
        for (String database : contextDatabases) {
            // don't show *-journal databases, they only hold temporary rollback data
            if (!database.endsWith("-journal")) {
                databaseList.add(context.getDatabasePath(database));
            }
        }

        FilenameFilter filenameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".sql")
                        || filename.endsWith(".sqlite")
                        || filename.endsWith(".db")
                        || filename.endsWith(".cblite")
                        || filename.endsWith(".cblite2");
            }
        };

        // CouchBase Lite stores the databases in the app files dir
        String[] cbliteFiles = context.fileList();
        for (String filename : cbliteFiles) {
            if (filenameFilter.accept(context.getFilesDir(), filename)) {
                Log.e(TAG, "All the cblite database" + context.getFilesDir() + "/" + filename + "size" + context.getFilesDir().getTotalSpace());
                File file1 = new File(context.getFilesDir(), filename);
                Log.e(TAG, " cblite database file size" + file1.getTotalSpace());
                file1.delete();
                //databaseList.add(file1);
            }
        }
    }

    catch(Exception e)
    {
        Crashlytics.log(android.util.Log.ERROR, TAG, "getDatabaseList" + e.getMessage());
    }

    }

    public void extractAllList(final String userId, final String companyId, String locId, String status)
    {
        try {
            Query query = database.createAllDocumentsQuery();
            //query.setStartKey("profile");
            QueryEnumerator rows = null;
            try {
                rows = query.run();
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }

            Log.e("Total Rows: ", rows.getCount() + "");
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            String mLastUpdateTime = dateFormat.format(curDate).toString();
            for (Iterator<QueryRow> it = rows; it.hasNext(); ) {
                QueryRow row = it.next();
                if (row != null) {
                    try {
                        if(row.getDocumentId().contains(mLastUpdateTime) && row.getDocument().getProperty("CompanyId").equals(companyId) && row.getDocument().getProperty("UID").equals(userId)
                                && row.getDocument().getProperty("LocID").equals(locId) && row.getDocument().getProperty("completeStatus").equals(status)) {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateExistingData" + e.getMessage());
        }
    }

    public void updateDoc(final String userId, final String latitude, final String longitude, final double distance, final String docId, final String locationId) throws CouchbaseLiteException {

        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();

            final Document doc = database.getDocument(docId + "_" + mLastUpdateTime);
            Log.e(TAG, "GPS update" + doc.getCurrentRevisionId());
            doc.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("date", mLastUpdateTime);
                    properties.put("UserName", userId);
                    properties.put("latitude", latitude);
                    properties.put("longitude", longitude);
                    properties.put("LocID", locationId);
                    properties.put("profile", "GPS");
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateDoc" + e.getMessage());
        }
    }

    public void updateWaypoint(final List<String> waypoints, final String docId, final String locId, final String userName, final String userId) throws CouchbaseLiteException {

        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();

            final Document doc = database.getDocument("waypoint_Trip" + sManager.getEndTripCount() + "_" + docId + "_" + mLastUpdateTime);
            doc.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("username", userName);
                    properties.put("userid", userId);
                    properties.put("locationid", locId);
                    properties.put("date", mLastUpdateTime);
                    properties.put("waypoint", waypoints);
                    properties.put("locationid", sManager.getLocId());
                    properties.put("tracker","TrackPoint");
                    properties.put("TripCount", sManager.getEndTripCount());
                    properties.put("TripStart", sManager.getStartTrip());
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });

        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateWaypoint" + e.getMessage());
        }
    }

    public void addNewRecord(final String user, final String docId, final double latitude, final double longitude, final double distance) throws CouchbaseLiteException {

        try {
            Map<String, Object> properties = new HashMap<>();
            Log.e(TAG, "Adding LatLang to couchBase" + latitude + ":" + longitude);
            //Document document = database.createDocument();
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            Document document = database.getDocument(docId + "_" + mLastUpdateTime);

            properties.put("date", "\""+mLastUpdateTime+"\"");
            properties.put("username", user);
            properties.put("latitude", latitude);
            properties.put("longitude", longitude);
            properties.put("profile", "GPS");
            document.putProperties(properties);

        }  catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "addNewRecord" + e.getMessage());
        }
    }

    private Map<String, Object> getQueryData(QueryEnumerator rows)
    {

        Map<String, Object> mapInfo = new HashMap<>();
        try
        {
            for (Iterator<QueryRow> it = rows; it.hasNext(); ) {
                QueryRow row = it.next();

                if (row.getDocument().getProperty("key").equals(1)) {
                    Log.e("DocumentId:", row.getDocumentId() + "");
                    mapInfo.put("latitude", row.getDocument().getProperty("latitude"));
                    Log.e("Latitude:", row.getDocument().getProperty("latitude") + "");
                    mapInfo.put("longitude", row.getDocument().getProperty("longitude"));
                    Log.e("Longitude:", row.getDocument().getProperty("longitude") + "");
                    mapInfo.put("distance", row.getDocument().getProperty("distance"));
                    Log.e("Distance:", row.getDocument().getProperty("distance") + "");
                }
            }
        }  catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "getQueryData" + e.getMessage());
        }
        return mapInfo;
    }

    public void updateCompleteDoc(final List<String> scanData, final String companyId, final String locId, final String PickUpId, final String assignId)
    {
        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdate = dateFormat.format(curDate).toString();

            if (database == null) {
                Log.e(TAG, "getDatabase Instance");
                try {
                    database = getDatabaseInstance();
                } catch (CouchbaseLiteException e) {
                    e.printStackTrace();
                }
            }
            if (database != null && !database.isOpen()) {
                database.open();
            }

            String docId = "PickUp" + "_" + companyId + "_" + locId + "_" + mLastUpdate;
            if(sManager == null) {
                sManager = new SessionManager(ctx);
            }
            String gps = sManager.getText();
            Log.e(TAG, "Print gps in CouchDB" + gps);
            if (gps != null && gps != "") {
                String[] mapData = gps.split(":");
                if (mapData.length > 2) {

                    lat = Double.parseDouble(mapData[0]);
                    lng = Double.parseDouble(mapData[1]);
                }
            }

            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
            final String time = format.format(curDate).toString();
            final Map<String, Object> data = new HashMap<>();
            data.put("scanItem", scanData);
            data.put("created_on", time);
            data.put("completeStatus", "Y");
            data.put("Latitude", lat);
            data.put("Longitude", lng);

            final Document document = database.getDocument(docId);
            try {
                document.update(new Document.DocumentUpdater() {
                    @Override
                    public boolean update(UnsavedRevision newRevision) {
                        Map<String, Object> properties = newRevision.getUserProperties();
                        Log.e(TAG, "Map properties data" + data);
                        properties.putAll(properties);
                        Log.e(TAG, "Map properties" + properties);
                        properties.put(PickUpId, data);
                        Log.e(TAG, "Map properties111111" + properties);
                        properties.put("Profile", "PickUPCompleteStatus");
                        newRevision.setUserProperties(properties);
                        return true;
                    }
                });
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }
        }  catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateCompleteDoc" + e.getMessage());
        }
    }

    public void updateInCompleteDoc(final String reason, final String remarks, final String companyId, final String locId, final String PickUpId)
    {
        try
        {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdate = dateFormat.format(curDate).toString();
            String docId = "PickUp" + "_" + companyId + "_" + locId + "_" + mLastUpdate;
            if(sManager == null) {
                sManager = new SessionManager(ctx);
            }
            String gps = sManager.getText();
            Log.e(TAG, "Print gps in CouchDB" + gps);
            if (gps != null && gps != "") {
                String[] mapData = gps.split(":");
                if (mapData.length > 2) {
                    lat = Double.parseDouble(mapData[0]);
                    lng = Double.parseDouble(mapData[1]);
                }
            }

            SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
            final String time = dateFormat.format(curDate).toString();
            final Map<String, Object> data = new HashMap<>();
            data.put("pickupReason", reason);
            data.put("pickupRemark", remarks);
            data.put("created_on", time);
            data.put("completeStatus", "N");
            data.put("Latitude", lat);
            data.put("Longitude", lng);

            final Document document = database.getDocument(docId);
            try {
                document.update(new Document.DocumentUpdater() {
                    @Override
                    public boolean update(UnsavedRevision newRevision) {
                        Map<String, Object> properties = newRevision.getUserProperties();
                        Log.e(TAG, "Map properties data" + data);
                        properties.put(PickUpId, data);
                        properties.put("Profile", "PickUPCompleteStatus");
                        newRevision.setUserProperties(properties);
                        /*loginTask = new LongOperation(); //every time create new object, as AsynTask will only be executed one time.
                        loginTask.execute();*/
                        return true;
                    }
                });
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }
        }  catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateInCompleteDoc" + e.getMessage());
        }
    }

    public void updateinitialRVPStatus(String docId, final String status, final String mLastUpdate1,final String waybillNo,
                                       final String signature, final String custName, final String companyId, final String locId, final String uid, final String assignmentNo,
                                       final String image, final String itemDcr, final String reasonreturn, final String descrivedValue, final String relationship, final String custNo, final String pickupreason, final String pickUpremark,final Double lat1,final
                                       Double lng1) {
        Log.e(TAG, "RVP Complete status" + status);
        Log.e(TAG, "DocId" + docId);


        final Document document = database.getDocument(docId);

        /*if(sManager == null) {
            sManager = new SessionManager(ctx);
        }
        String gps = sManager.getText();
        Log.e(TAG, "Print gps in CouchDB" + gps);
        if (gps != null && gps != "") {
            String[] mapData = gps.split(":");
            if (mapData.length > 2) {
                lat = Double.parseDouble(mapData[0]);
                lng = Double.parseDouble(mapData[1]);
            }
        }*/
        try {
            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();

                    properties.put("waybillNo", waybillNo);
                    properties.put("AssignmentNo", assignmentNo);
                    properties.put("LocID", locId);
                    properties.put("UID", uid);
                    properties.put("created_on", mLastUpdate1);
                    properties.put("CompanyId", companyId);
                    properties.put("pickupReason", pickupreason);
                    properties.put("pickupRemark", pickUpremark);
                    properties.put("completeStatus", status);
                    properties.put("profile","RVP");
                    properties.put("image", image);
                    properties.put("itemDescription", itemDcr);
                    properties.put("returnReason", reasonreturn);
                    properties.put("describedValue", descrivedValue);
                    properties.put("signature", signature);
                    properties.put("custName", custName);
                    properties.put("custNo", custNo);
                    properties.put("relationship",relationship);
                    properties.put("Latitude", lat1);
                    properties.put("Longitude", lng1);
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateInitialStatus" + e.getMessage());
        }
    }

    public void updateRVPFailedData(final String docId,final String waybillNo,
                                    final String pickupreason, final String pickUpremark, final String companyId, final String locId, final String uid, final String assignmentNo,
                                    final String image, final String itemDcr, final String reasonreturn, final String descrivedValue, final String noOfPcs) {

        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdate = dateFormat.format(curDate).toString();

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        final String mLastUpdate1 = dateFormat1.format(curDate).toString();

        if(sManager == null) {
            sManager = new SessionManager(ctx);
        }

        String gps = sManager.getText();
        Log.e(TAG, "Print gps in CouchDB" + gps);
        if (gps != null && gps != "") {
            String[] mapData = gps.split(":");
            if (mapData.length > 2) {
                lat = Double.parseDouble(mapData[0]);
                lng = Double.parseDouble(mapData[1]);
            }
        }
        final Document document = database.getDocument(docId+"_"+mLastUpdate);
        try {

            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("waybillNo", waybillNo);
                    properties.put("AssignmentNo", assignmentNo);
                    properties.put("LocID", locId);
                    properties.put("UID", uid);
                    properties.put("created_on", mLastUpdate1);
                    properties.put("CompanyId", companyId);
                    properties.put("pickupReason", pickupreason);
                    properties.put("pickupRemark", pickUpremark);
                    properties.put("completeStatus", "N");
                    properties.put("profile","RVP");
                    properties.put("image", image);
                    properties.put("itemDescription", itemDcr);
                    properties.put("noOfpcs", noOfPcs);
                    properties.put("returnReason", reasonreturn);
                    properties.put("describedValue", descrivedValue);
                    properties.put("signature", "NA");
                    properties.put("custName", "NA");
                    properties.put("custNo", "NA");
                    properties.put("relationship","NA");
                    properties.put("Latitude", lat);
                    properties.put("Longitude", lng);
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateInitialStatus" + e.getMessage());
        }
    }

    public void updateRVPSuccessData(final String docId, final String waybillNo,
                                     final String signature, final String custName, final String companyId, final String locId, final String uid, final String assignmentNo,
                                     final String image, final String itemDcr, final String reasonreturn, final String descrivedValue, final String noOfPcs, final String relationship, final String custNo) {

        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdate = dateFormat.format(curDate).toString();

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        final String mLastUpdate1 = dateFormat1.format(curDate).toString();

        if(sManager == null) {
            sManager = new SessionManager(ctx);
        }
        String gps = sManager.getText();
        Log.e(TAG, "Print gps in CouchDB" + gps);
        if (gps != null && gps != "") {
            String[] mapData = gps.split(":");
            if (mapData.length > 2) {
                lat = Double.parseDouble(mapData[0]);
                lng = Double.parseDouble(mapData[1]);
            }
        }

        final Document document = database.getDocument(docId+"_"+mLastUpdate);
        try {
            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("waybillNo", waybillNo);
                    properties.put("AssignmentNo", assignmentNo);
                    properties.put("LocID", locId);
                    properties.put("UID", uid);
                    properties.put("created_on", mLastUpdate1);
                    properties.put("CompanyId", companyId);
                    properties.put("pickupReason", "NA");
                    properties.put("pickupRemark", "NA");
                    properties.put("completeStatus", "Y");
                    properties.put("profile","RVP");
                    properties.put("image", image);
                    properties.put("itemDescription", itemDcr);
                    properties.put("noOfpcs", noOfPcs);
                    properties.put("returnReason", reasonreturn);
                    properties.put("describedValue", descrivedValue);
                    properties.put("signature", signature);
                    properties.put("custName", custName);
                    properties.put("custNo", custNo);
                    properties.put("relationship",relationship);
                    properties.put("Latitude", lat);
                    properties.put("Longitude", lng);
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateInitialStatus" + e.getMessage());
        }
    }


    public void updateInitialStatus(final String docId, final List<String> scanItem,final String status, final String createdOn,
                                    final String pickupreason, final String pickUpremark, final String companyId, final String locId, final String uid, final String assignmentNo, final String reasonId) {

        //final List<String> scanItem = new ArrayList<>();
        final Document document = database.getDocument(docId);
        if(sManager == null) {
            sManager = new SessionManager(ctx);
        }
        String gps = sManager.getText();
        if (gps != null && gps != "") {
            String[] mapData = gps.split(":");
            if (mapData.length > 2) {
                Log.e(TAG, "Print gps in CouchDB" + gps);
                lat = Double.parseDouble(mapData[0]);
                lng = Double.parseDouble(mapData[1]);
            }
        }
        else {
            try {
                lat = (sManager.getLat() != null) ? Double.parseDouble(sManager.getLat()) : 0.0;
                lng = (sManager.getLng() != null) ? Double.parseDouble(sManager.getLng()) : 0.0;
            }catch(Exception e) {
                Crashlytics.log(Log.ERROR, TAG, e.getMessage());
            }
        }

        try {
            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("scanItem", scanItem);
                    properties.put("AssignmentNo", assignmentNo);
                    properties.put("LocID", locId);
                    properties.put("UID", uid);
                    properties.put("created_on", createdOn);
                    properties.put("CompanyId", companyId);
                    properties.put("pickupReason", pickupreason);
                    properties.put("reasonId", reasonId);
                    properties.put("pickupRemark", pickUpremark);
                    properties.put("completeStatus", status);
                    properties.put("Latitude",lat);
                    properties.put("Longitude",lng);
                    properties.put("profile","Pickup");
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateInitialStatus" + e.getMessage());
        }
    }
    public void updateDoc(final List<String> scanData, final String docId, final String assignId, final String companyId)
    {
        //Log.e(TAG,"Update document with pickUpId" + pickUpId + "docId" + docId );
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdate = dateFormat.format(curDate).toString();
        Log.e(TAG, "Update scanned Data " + scanData + "in doc Id" + docId + "_" + assignId + "_" + companyId + "_" + mLastUpdate);
        try {
            final Document document = database.getDocument(docId + "_" + assignId + "_" + companyId + "_" + mLastUpdate);
            if(sManager == null) {
                sManager = new SessionManager(ctx);
            }
            String gps = sManager.getText();
            Date curDate1 = new Date();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            final String mLastUpdateTime = dateFormat1.format(curDate1).toString();

            if (gps != null && gps != "") {
                String[] mapData = gps.split(":");
                if (mapData.length > 2) {
                    Log.e(TAG, "Print gps in CouchDB" + gps);
                    lat = Double.parseDouble(mapData[0]);
                    lng = Double.parseDouble(mapData[1]);
                }
            }
            else {
                try {
                    lat = (sManager.getLat() != null) ? Double.parseDouble(sManager.getLat()) : 0.0;
                    lng = (sManager.getLng() != null) ? Double.parseDouble(sManager.getLng()) : 0.0;
                }catch(Exception e) {
                    Crashlytics.log(Log.ERROR, TAG, e.getMessage());
                }
            }
            Log.e(TAG, "Updating new scanItem in CouchDB");
            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("AssignmentNo", docId);
                    properties.put("scanItem", scanData);
                    properties.put("created_on", mLastUpdateTime);
                    properties.put("completeStatus", "Y");
                    properties.put("Latitude", lat);
                    properties.put("Longitude", lng);
                    properties.put("profile", "Pickup");
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });
        } catch (CouchbaseLiteException e) {
            Log.e("Error while updating", "Error putting", e);
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateDoc" + e.getMessage());
        }
    }

    public void updateDoc(final String reason, final String remark, final String docId, final String assignId, final String companyId, final String reasonId) {
        //Log.e(TAG,"Update document with pickUpId" + pickUpId + "docId" + docId );
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();
        final String mLastUpdateTime1 = dateFormat1.format(curDate).toString();
        if(sManager == null) {
            sManager = new SessionManager(ctx);
        }
        String gps = sManager.getText();
        if (gps != null && gps != "") {
            String[] mapData = gps.split(":");
            if (mapData.length > 2) {
                Log.e(TAG, "Print gps in CouchDB" + gps);
                lat = Double.parseDouble(mapData[0]);
                lng = Double.parseDouble(mapData[1]);
            }
        }
        else {
            try {
                lat = (sManager.getLat() != null) ? Double.parseDouble(sManager.getLat()) : 0.0;
                lng = (sManager.getLng() != null) ? Double.parseDouble(sManager.getLng()) : 0.0;
            }catch(Exception e) {
                Crashlytics.log(Log.ERROR, TAG, e.getMessage());
            }
        }

        try {
            final Document document = database.getDocument(docId + "_" + assignId + "_" + companyId + "_" + mLastUpdateTime);
            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("pickupReason", reason);
                    properties.put("reasonId", reasonId);
                    properties.put("pickupRemark", remark);
                    properties.put("created_on", mLastUpdateTime1);
                    properties.put("completeStatus", "N");
                    properties.put("Latitude", lat);
                    properties.put("Longitude", lng);
                    properties.put("profile", "Pickup");
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });

        } catch (CouchbaseLiteException e) {
            Log.e("Error while updating", "Error putting", e);
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateDoc" + e.getMessage());
        }
    }

    public void addNewRecord(Map<String, Object> data, String assign_no, String date, String companyId) throws CouchbaseLiteException {
        Log.e(TAG, "Adding data to couchBase" + data + assign_no);
        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            Document document = database.getDocument(assign_no + "_" + companyId + "_" + mLastUpdateTime);
            String docId = document.getId();
            document.putProperties(data);
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "addNewRecord" + e.getMessage());
        }
    }

    public boolean retrieveCompletePickUpStatus(String docId) {
        try {
            Set<String> statusList = sManager.getCompleteStatus();
            Document doc = database.getDocument(docId);
            Log.e(TAG, "Rows of Complete for Complete: " + doc.getId());
            Object completeProperty = doc.getProperty("completeStatus");
            if (completeProperty != null) {
                if (completeProperty.toString().trim().equalsIgnoreCase("Y")) {
                    return true;
                }
            }
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "retrieveCompletePickUpStatus" + e.getMessage());
        }
        return false;
    }

    public boolean retrieveInCompletePickUpStatus(String docId) {
        try {
            Document doc = database.getDocument(docId);
            Log.e(TAG, "Rows of Complete for Incomplete: " + doc.getId());
            Object completeProperty = doc.getProperty("completeStatus");
            if (completeProperty != null) {
                if (completeProperty.toString().trim().equalsIgnoreCase("N")) {
                    return true;
                }
            }
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "retrieveInCompletePickUpStatus" + e.getMessage());
        }

        return false;
    }

    public boolean retrieveAllPickUpStatus(String docId) {
        try {
            /*if(database !=null && !database.isOpen()) {
                database.open();
            }*/
            Document doc = database.getDocument(docId);
            Log.e(TAG, "Rows of Complete for All: " + doc.getId());
            Object completeProperty = doc.getProperty("completeStatus");
            Object rvpstatus = doc.getProperty("status");
            if (completeProperty != null) {
                if (completeProperty.toString().trim().equalsIgnoreCase("NA")) {
                    Log.e(TAG, "Rows of Complete for All: " + completeProperty.toString().trim());
                    return true;
                }
            }
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "retrieveAllPickUpStatus" + e.getMessage());
        }
        return false;
    }

    public  void attachImage(String docId, Bitmap image) throws CouchbaseLiteException {
        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            Document task = database.getDocument(docId + "_" + mLastUpdateTime);
            if (task == null || image == null) return;
            UnsavedRevision revision = task.createRevision();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 50, out);
            ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
            revision.setAttachment("image", "image/jpg", in);
            revision.save();
            Log.e(TAG, " Attaching image..");
        }catch (Exception e) {
            Log.e(TAG, "While attaching image" + e.getMessage());
        }
    }

    /*public void addDistanceBattery(final String custid, final String userId, final int battery,final float distance) {
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        final String mLastUpdateTime = dateFormat.format(curDate).toString();

        try {
            final Document document = database.getDocument("Distance_"+ custid + "_" + userId + "_" + mLastUpdateTime);
            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    Log.e(TAG, "Map properties data" + properties);
                    properties.put("battery", battery);
                    properties.put("distance", distance);
                    properties.put("created_on", mLastUpdateTime);
                    properties.put("custId", custid);
                    properties.put("UID", userId);
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });

        } catch (CouchbaseLiteException e) {
            Log.e("Error while updating", "Error putting", e);
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateDoc" + e.getMessage());
        }

    }*/

   /* private void getQueryData(QueryEnumerator rows) {
        for(Iterator<QueryRow> it = rows; it.hasNext();) {
            QueryRow row = it.next();
            Log.e("DocumentId:", row.getDocumentId() + "");
            Log.e("Scanned Data:", row.getDocument().getProperty("CONSIGNEE_CONTACT_NO") + "");
            Log.e("Name:", row.getDocument().getProperty("name") + "");
            Log.e("PickUp Id", row.getDocument().getProperty("ASSIGNMENT_NO") + "");
        }
    }*/

    public boolean queryDocId(String docId) {
        Query query = database.createAllDocumentsQuery();
        QueryEnumerator rows = null;

        try {
            rows = query.run();
            for (Iterator<QueryRow> it = rows; it.hasNext(); ) {
                QueryRow row = it.next();
                if (row.getDocumentId().equals(docId)) {
                    Log.e(TAG, "Query result found");
                    if(row.getDatabase().getDocument(docId).getProperty("completeStatus").equals("NA")) {
                        Log.e(TAG, "Completestatus is NA");
                        return false;
                    }
                    return true;
                }
            }
            Log.e(TAG, "Query result not found");
            countShipment  = sManager.getAssignedShippmentSize() + 1;
            sManager.setAssignedShippmentSize(countShipment);
            sManager.setNotify(true);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "queryDocId" + e.getMessage());
        }
        return false;
    }

    public void deletePreviousDatabase() {
        Query query = database.createAllDocumentsQuery();
        QueryEnumerator rows = null;
        //getYesterdayDateString();
        //String docId = "";
        try {
            rows = query.run();
            for (Iterator<QueryRow> it = rows; it.hasNext(); ) {
                QueryRow row = it.next();
                if (row.getDocumentId().contains(getYesterdayDateString())) {
                    Log.e(TAG, "Query result found" + row.getDocumentId());
                    deleteDocument(row.getDocumentId());
                }
            }
            Log.e(TAG, "Query result not found");
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }


    public boolean deleteDocument(String docid) throws CouchbaseLiteException {

        Document document = database.getDocument(docid);
        if (!document.isDeleted()) {
            return document.delete();
        }
        return true;
    }

    public void addPickUpscanRVPRecord(final String assignmentId, final String locId, final String uuid,
                                       final String companyId, final String assign_no, final String date,
                                       final String waybillNo, final String itemDcr, final String reasonreturn,
                                       final String descrivedValue, final String noOfPcs ) throws CouchbaseLiteException {
        //Log.e(TAG,"Adding data to couchBase" + scanData);
        //database.beginTransaction();
        try {
           /* if(database !=null && !database.isOpen()) {
                database.open();
            }*/

            List<String> scanItem = new ArrayList<>();
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();

            if(sManager == null) {
                sManager = new SessionManager(ctx);
            }
            String gps = sManager.getText();
            if (gps != null && gps != "") {
                String[] mapData = gps.split(":");
                if (mapData.length > 2) {
                    Log.e(TAG, "Print gps in CouchDB" + gps);
                    lat = Double.parseDouble(mapData[0]);
                    lng = Double.parseDouble(mapData[1]);
                }
            }
            else {
                try {
                    lat = (sManager.getLat() != null) ? Double.parseDouble(sManager.getLat()) : 0;
                    lng = (sManager.getLng() != null) ? Double.parseDouble(sManager.getLng()) : 0;
                }catch(Exception e) {
                    Crashlytics.log(Log.ERROR, TAG, e.getMessage());
                }
            }

            Document document = database.getDocument(assignmentId + "_" + assign_no + "_" + companyId + "_" + date);

            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    List<String> scanItem = new ArrayList<>();
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("waybillNo", waybillNo);
                    properties.put("AssignmentNo", assignmentId);
                    properties.put("LocID", locId);
                    properties.put("UID", uuid);
                    properties.put("created_on", mLastUpdateTime);
                    properties.put("CompanyId", companyId);
                    properties.put("pickupReason", "NA");
                    properties.put("pickupRemark", "NA");
                    properties.put("completeStatus", "NA");
                    properties.put("profile","RVP");
                    properties.put("image", "NA");
                    properties.put("itemDescription", itemDcr);
                    properties.put("noOfpcs", noOfPcs);
                    properties.put("returnReason", reasonreturn);
                    properties.put("describedValue", descrivedValue);
                    properties.put("signature", "NA");
                    properties.put("custName", "NA");
                    properties.put("custNo", "NA");
                    properties.put("relationship","NA");
                    properties.put("Latitude", lat);
                    properties.put("Longitude", lng);
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });

          /*
            Map<String, Object> properties = new HashMap<>();
            Log.e(TAG, "Map properties data" + properties);
            properties.put("waybillNo", waybillNo);
            properties.put("AssignmentNo", assignmentId);
            properties.put("LocID", locId);
            properties.put("UID", uuid);
            properties.put("created_on", mLastUpdateTime);
            properties.put("CompanyId", companyId);
            properties.put("pickupReason", "NA");
            properties.put("pickupRemark", "NA");
            properties.put("completeStatus", "NA");
            properties.put("profile","RVP");
            properties.put("image", "NA");
            properties.put("itemDescription", itemDcr);
            properties.put("noOfpcs", noOfPcs);
            properties.put("returnReason", reasonreturn);
            properties.put("describedValue", descrivedValue);
            properties.put("signature", "NA");
            properties.put("custName", "NA");
            properties.put("custNo", "NA");
            properties.put("relationship","NA");
            properties.put("Latitude", lat);
            properties.put("Longitude", lng);

            Document document = database.getDocument(assignmentId + "_" + assign_no + "_" + companyId + "_" + date);
            document.putProperties(properties);*/
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "addPickUpscanRecord" + e.getMessage());
        }
    }

    public void addPickUpscanRecord(final String assignmentId, final String locId,final String uuid,
                                    final String companyId, final String assign_no, final String date) throws CouchbaseLiteException {
        //Log.e(TAG,"Adding data to couchBase" + scanData);
        //database.beginTransaction();
        try {
           /* if(database !=null && !database.isOpen()) {
                database.open();
            }*/
            if(sManager == null) {
                sManager = new SessionManager(ctx);
            }
            String gps = sManager.getText();
            if (gps != null && gps != "") {
                String[] mapData = gps.split(":");
                if (mapData.length > 2) {
                    Log.e(TAG, "Print gps in CouchDB" + gps);
                    lat = Double.parseDouble(mapData[0]);
                    lng = Double.parseDouble(mapData[1]);
                }
            }
            else {
                try {
                    lat = (sManager.getLat() != null) ? Double.parseDouble(sManager.getLat()) : 0;
                    lng = (sManager.getLng() != null) ? Double.parseDouble(sManager.getLng()) : 0;
                }catch(Exception e) {
                    Crashlytics.log(Log.ERROR, TAG, e.getMessage());
                }
            }

            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();

            Document document = database.getDocument(assignmentId + "_" + assign_no + "_" + companyId + "_" + date);

            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    List<String> scanItem = new ArrayList<>();
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("scanItem", scanItem);
                    properties.put("AssignmentNo", assignmentId);
                    properties.put("LocID", locId);
                    properties.put("UID", uuid);
                    properties.put("created_on", mLastUpdateTime);
                    properties.put("CompanyId", companyId);
                    properties.put("pickupReason", "NA");
                    properties.put("reasonId", "NA");
                    properties.put("pickupRemark", "NA");
                    properties.put("completeStatus", "NA");
                    properties.put("Latitude",lat);
                    properties.put("Longitude",lng);
                    properties.put("profile","Pickup");
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });



     /*
            Map<String, Object> properties = new HashMap<>();
            Log.e(TAG, "Map properties data" + properties);
            properties.put("scanItem", scanItem);
            properties.put("AssignmentNo", assignmentId);
            properties.put("LocID", locId);
            properties.put("UID", uuid);
            properties.put("created_on", mLastUpdateTime);
            properties.put("CompanyId", companyId);
            properties.put("pickupReason", "NA");
            properties.put("reasonId", "NA");
            properties.put("pickupRemark", "NA");
            properties.put("completeStatus", "NA");
            properties.put("Latitude",lat);
            properties.put("Longitude",lng);
            properties.put("profile","Pickup");
            Document document = database.getDocument(assignmentId + "_" + assign_no + "_" + companyId + "_" + date);
            document.putProperties(properties);*/
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "addPickUpscanRecord" + e.getMessage());
        }
    }

    public void addECOMscanRecord(final String assignmentId, final String locId, final String uuid,
                                  final String companyId, final String assign_no, final String date) throws CouchbaseLiteException {
        //Log.e(TAG,"Adding data to couchBase" + scanData);
        //database.beginTransaction();
        try {
           /* if(database !=null && !database.isOpen()) {
                database.open();
            }*/

           /* if(database !=null && !database.isOpen()) {
                database.open();
            }*/

            List<String> scanItem = new ArrayList<>();
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();

            if(sManager == null) {
                sManager = new SessionManager(ctx);
            }
            String gps = sManager.getText();
            if (gps != null && gps != "") {
                String[] mapData = gps.split(":");
                if (mapData.length > 2) {
                    Log.e(TAG, "Print gps in CouchDB" + gps);
                    lat = Double.parseDouble(mapData[0]);
                    lng = Double.parseDouble(mapData[1]);
                }
            }
            else {
                try {
                    lat = (sManager.getLat() != null) ? Double.parseDouble(sManager.getLat()) : 0;
                    lng = (sManager.getLng() != null) ? Double.parseDouble(sManager.getLng()) : 0;
                }catch(Exception e) {
                    Crashlytics.log(Log.ERROR, TAG, e.getMessage());
                }
            }

            Document document = database.getDocument(assignmentId + "_" + assign_no + "_" + companyId + "_" + date);

            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    List<String> scanItem = new ArrayList<>();
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("scanItem", scanItem);
                    properties.put("AssignmentNo", assignmentId);
                    properties.put("LocID", locId);
                    properties.put("UID", uuid);
                    properties.put("created_on", mLastUpdateTime);
                    properties.put("CompanyId", companyId);
                    properties.put("pickupReason", "NA");
                    properties.put("reasonId", "NA");
                    properties.put("pickupRemark", "NA");
                    properties.put("completeStatus", "NA");
                    properties.put("doctype","NA");
                    properties.put("Latitude",lat);
                    properties.put("Longitude",lng);
                    properties.put("profile","ECOM");
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });

      /*      Map<String, Object> properties = new HashMap<>();
            Log.e(TAG, "Map properties data" + properties);
            properties.put("scanItem", scanItem);
            properties.put("AssignmentNo", assignmentId);
            properties.put("LocID", locId);
            properties.put("UID", uuid);
            properties.put("created_on", mLastUpdateTime);
            properties.put("CompanyId", companyId);
            properties.put("pickupReason", "NA");
            properties.put("reasonId", "NA");
            properties.put("pickupRemark", "NA");
            properties.put("completeStatus", "NA");
            properties.put("doctype","NA");
            properties.put("Latitude",lat);
            properties.put("Longitude",lng);
            properties.put("profile","ECOM");
            Document document = database.getDocument(assignmentId + "_" + assign_no + "_" + companyId + "_" + date);
            document.putProperties(properties);*/
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "addPickUpscanRecord" + e.getMessage());
        }
    }

    public void updateECOMDetails(final String docId, final String status, final List<String> scanItem, final String createdOn,
                                  final String pickupreason, final String pickUpremark, final String companyId, final String locId, final String uid, final String assignmentNo, final String doctype,final String reasonId) {
        final Document document = database.getDocument(docId);


           /* if(database !=null && !database.isOpen()) {
                database.open();
            }*/

        try {
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            final String mLastUpdateTime = dateFormat.format(curDate).toString();

            if(sManager == null) {
                sManager = new SessionManager(ctx);
            }
            String gps = sManager.getText();
            if (gps != null && gps != "") {
                String[] mapData = gps.split(":");
                if (mapData.length > 2) {
                    Log.e(TAG, "Print gps in CouchDB" + gps);
                    lat = Double.parseDouble(mapData[0]);
                    lng = Double.parseDouble(mapData[1]);
                }
            }
            else {
                try {
                    lat = (sManager.getLat() != null) ? Double.parseDouble(sManager.getLat()) : 0;
                    lng = (sManager.getLng() != null) ? Double.parseDouble(sManager.getLng()) : 0;
                }catch(Exception e) {
                    Crashlytics.log(Log.ERROR, TAG, e.getMessage());
                }
            }
            try {
                document.update(new Document.DocumentUpdater() {
                    @Override
                    public boolean update(UnsavedRevision newRevision) {
                        Map<String, Object> properties = newRevision.getUserProperties();
                        properties.put("scanItem", scanItem);
                        properties.put("AssignmentNo", assignmentNo);
                        properties.put("LocID", locId);
                        properties.put("UID", uid);
                        properties.put("created_on", mLastUpdateTime);
                        properties.put("CompanyId", companyId);
                        properties.put("pickupReason", pickupreason);
                        properties.put("reasonId", reasonId);
                        properties.put("pickupRemark", pickUpremark);
                        properties.put("completeStatus", status);
                        properties.put("doctype", doctype);
                        properties.put("profile","ECOM");
                        properties.put("Latitude",lat);
                        properties.put("Longitude",lng);
                        newRevision.setUserProperties(properties);
                        return true;
                    }
                });
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }}
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "updateInitialStatus" + e.getMessage());
        }
    }

    public void updateInitialEcomData(final String docId, final List<String> scanItem,final String status, final String createdOn,final String pickupreason, final String pickUpremark,final String companyId,final String locId, final String uid,final String assignmentNo, final String doctype, final String reasonId) {
        //final List<String> scanItem = new ArrayList<>();
        final Document document = database.getDocument(docId);
        try {
            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put("scanItem", scanItem);
                    properties.put("AssignmentNo", assignmentNo);
                    properties.put("LocID", locId);
                    properties.put("UID", uid);
                    properties.put("created_on", createdOn);
                    properties.put("CompanyId", companyId);
                    properties.put("pickupReason", pickupreason);
                    properties.put("reasonId", reasonId);
                    properties.put("pickupRemark", pickUpremark);
                    properties.put("completeStatus", status);
                    properties.put("doctype", doctype);
                    properties.put("profile","ECOM");
                    properties.put("Latitude",lat);
                    properties.put("Longitude",lng);
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }


    public HashMap<String, String> getDocMap() {
        return storeDocId;
    }

    public void setDocMap(HashMap<String, String> docMap) {
        storeDocId = docMap;
    }

    public void addAttachment() {
        InputStream stream = new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
        Document doc = database.getDocument("Robin");
        UnsavedRevision newRev = doc.getCurrentRevision().createRevision();
        newRev.setAttachment("photo.jpg", "image/jpeg", stream);
        try {
            newRev.save();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Object> getShipperAdress(String docId) {
        Map<String, Object> add = new HashMap<>();
        try {
            /*if(database !=null && !database.isOpen()) {
                database.open();
            }*/
            Document doc = database.getDocument(docId);
            Log.e(TAG, "Shipper Doc Info: " + doc.getId());
            Object shipAdd1 = doc.getProperty("SHIPPERADDRESS1");
            Object shipAdd2 = doc.getProperty("SHIPPERADDRESS2");
            Object shipCity = doc.getProperty("SHIPPERCITY");
            Object shipPin = doc.getProperty("SHIPPERPINCODE");
            Object shipConct = doc.getProperty("SHIPPERCONTACTNO");
            add.put("shipAdd1", shipAdd1);
            add.put("shipAdd2", shipAdd2);
            add.put("shipCity", shipCity);
            add.put("shipPin", shipPin);
            add.put("shipConct", shipConct);

        } catch (Exception e) {

        }
            /*if(database !=null && database.isOpen()) {
                database.close();
            }
        }
        finally {
            if(database !=null && database.isOpen()) {
                database.close();
            }
        }*/
        return add;
    }

    public void setScanItems(final String pickupRef, final Set<String> scanItems, String docId) {
        final Document document = database.getDocument(scanItemsKey + "_" + docId);
        try {
            document.update(new Document.DocumentUpdater() {
                @Override
                public boolean update(UnsavedRevision newRevision) {
                    Map<String, Object> properties = newRevision.getUserProperties();
                    properties.put(pickupRef, scanItems);
                    newRevision.setUserProperties(properties);
                    return true;
                }
            });
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public List<String> getScanItems(String doc ,String pickupRef) {
        String docId = scanItemsKey + "_" + doc;
        List<String> scannedData = null;
        Document document = database.getDocument(docId);
        if(document.getProperty(pickupRef) != null) {
            scannedData = (List<String>) document.getProperty(pickupRef);
            Log.e(TAG, "scanned Data" + scannedData);
        }
        return scannedData;
    }


    public void addGPSdata() {
        Document document = database.getDocument("gps");
    }

    private void shutdownDatabase() {
        manager.close();
    }

    private String getYesterdayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -2);
        //Log.e(TAG, "Date: " + dateFormat.format(cal.getTime()));
        return dateFormat.format(cal.getTime());
    }
}

package com.traconmobi.tom.model;

import com.google.gson.annotations.SerializedName;



/**
 * Created by kumargaurav on 12/8/15.
 */

public class Row {

    @SerializedName("id")
    private String id;

    @SerializedName("key")
    private String key;

    @SerializedName("value")
    private Value value;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}


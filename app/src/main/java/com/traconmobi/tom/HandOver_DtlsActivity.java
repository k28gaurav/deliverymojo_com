/*************************************************************/

/**********CREATED BY : ASHWINI NAIK ************************/
/********CREATED DATE : 08/03/2013 **************************/
/***********PURPOSE   : TO SAVE THE COD DETAILS OF THE CUSTOMER ******/

/************************************************************/
package com.traconmobi.tom;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import android.view.*;

import org.kobjects.base64.Base64;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.http.OkHttpHandler;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.Media;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.AttributeSet;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.ViewGroup.LayoutParams;

import io.fabric.sdk.android.Fabric;

public class HandOver_DtlsActivity extends Activity
{
	 // The minimum distance to change Updates in meters
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = (float) 11.1 ; // 10 meters
 
    // The minimum time between updates in milliseconds
//    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * randBetween(0, 5) ; // 1 minute
    
    Location location; // location
    
	private final String NAMESPACE_gps_log = "http://tempuri.org/";
	private final String URL_gps_log = "http://navatech.cloudapp.net/TrackOnMobi_WS_PAREKH_Test/TrackOnMobi_WS_V_2.asmx";
	@SuppressWarnings("unused")
	private final String SOAP_ACTION_gps_log = "http://tempuri.org/InsertTOM_Gps_Log";
	private final String METHOD_NAME_gps_log = "InsertTOM_Gps_Log";
	
	public static String tempDir;
	final String curnt_dt =TOMLoginUserActivity.currentdate;
	public int count = 1;
	protected Button imageview;
	static final String EXTRA_MESSAGE =null;
	public String current = null;	
	private Bitmap mBitmap;
	static SQLiteDatabase db;
	public static String new_lat,new_lon,imei_num,s1,s2,s3,bb,rel_type,ba1,r_sign,sign,update_pod,room = null,showdate,awb="",emp_indx="",COD_Amount,str_amt,st,sign1;
	String showtime;
	String BA1 ="null";
	 double amt;	
	static LinearLayout mContent;
	static LinearLayout ll;
	public static String employeeId,aa,count_del_sync,count_del,name,photoname=null,cod_amount;
    signature mSignature,btnsave;
	Button mClear,cancelButton;
	static Button mGetDelivered,btnsgn,btnreset,btnpicture;
    View mView;
    static File mypath;
    Cursor crs,cursor,c_routesyn;
    String e_indx="";
    private String uniqueId;
    static ImageView iv;
    byte [] HRHK,BA;
	static File image;	
	Uri ImageUri; 
	public static EditText received_by,receiver_contct_numbr;		
	public static int mYear,mMonth,mDay,hour,minute,shw_cnt;
	String PageName,PageName1 ;
	Bundle bundle;
	TextView version_name,single_cod_amt;
	Spinner s ;
	static CaptureSignature.signature msign;
	static String ssign;
	private String selectedImagePath;
    private ImageView img;
    static Bitmap  theImage;
    String currentTime;

 // Session Manager Class
    SessionManager session;
    TinyDB tiny;
    
    public String Receiver,awb_id,congn_nme,recvr_nme,amt_coltd,result_rcvr,result,entityString_gps,session_user_id,session_user_pwd,session_USER_LOC,session_USER_LOC_ID,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,Session_single_cod_amt;
	public static String session_DB_PATH,Assignment_typ,session_RESPONSE_AWB_NUM,session_RESPONSE_RECEVR;
	public static String session_DB_PWD;
    
  	Uri imageUri= null;
    File fileimage = null;
    final int TAKE_PHOTO_CODE = 1;
	private static final int SIGNATURE_ACTIVITY = 0;
	@SuppressWarnings("unused")
	private static final int multiple = 0;

	protected static final String DEL_UPDATE_ASSGN_ID = null;
		
    private boolean clicked = false; // this is a member variable
    @SuppressWarnings("deprecation")
    public static String getassgnmt;
	public static String TAG="HandOver_DtlsActivity";
	TextView title;
    
    public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }
    
	@Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	try
		{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
			//Intializing Fabric
		Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_hand_over__dtls);        
        getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_hand_over__dtls);


			title=(TextView)findViewById(R.id.txt_title);
			title.setText("HANDOVER UPDATE");

           
        
        Intent intent = getIntent();
        getassgnmt=intent.getStringExtra(HandOverMainActivity.EXTRA_MESSAGE1);
        
        Crashlytics.log(android.util.Log.ERROR, TAG, "getassgnmt " + getassgnmt);
        
        /* Use the LocationManager class to obtain GPS locations */

        try
		{
		LocationManager mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

		LocationListener mlocListener = new MyLocationListener();
		Crashlytics.log(android.util.Log.ERROR, TAG, "mlocListener " + mlocListener + mlocManager);
		 if (location == null) {
//           locationManager.requestLocationUpdates(
//                   LocationManager.GPS_PROVIDER,
//                   MIN_TIME_BW_UPDATES,
//                   MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
		mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_BW_UPDATES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES, mlocListener);
//		Toast.makeText( getApplicationContext(),"loc 1 lat "+ location.getLatitude()+ "loc 1 lon "+ location.getLongitude(),Toast.LENGTH_SHORT).show();
		
		if (mlocManager != null) {
           location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
             new_lat = String.valueOf(location.getLatitude());
             new_lon = String.valueOf(location.getLongitude());
             if (new_lat == null ) {
     	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat null");
     	    	new_lat="null";
     	    } else {
     	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat not null ");
     	    }
        		if (new_lon == null ) {
     	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon null");
     	    	new_lon="null";
     	    } else {
     	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon not null ");
     	    }
//             if(new_lat == "null" || new_lat=="" || new_lat == null)
//             {
//            	 new_lat="NULL";
//             }
//             if(new_lon == "null" || new_lon =="" || new_lon == null)
//             {
//            	 new_lon="NULL";
//             }
//           Crashlytics.log(android.util.Log.ERROR,TAG,"loc 2 lat "+ location.getLatitude());
//           Crashlytics.log(android.util.Log.ERROR,TAG,"loc 2 lon "+ location.getLongitude());
//           Toast.makeText( getApplicationContext(),"loc 2 lat "+ location.getLatitude()+ "loc 2 lon "+ location.getLongitude(),Toast.LENGTH_SHORT).show();
           if (location != null) {
        	   new_lat = String.valueOf(location.getLatitude());
        	   new_lon = String.valueOf(location.getLongitude());
        	   if (new_lat == null ) {
       	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat null");
       	    	new_lat="null";
       	    } else {
       	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat not null ");
       	    }
          		if (new_lon == null ) {
       	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon null");
       	    	new_lon="null";
       	    } else {
       	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon not null ");
       	    }
//        	   if(new_lat == "null" || new_lat=="" || new_lat == null)
//               {
//              	 new_lat="NULL";
//               }
//               if(new_lon == "null" || new_lon =="" || new_lon == null)
//               {
//              	 new_lon="NULL";
//               }
//               Crashlytics.log(android.util.Log.ERROR,TAG,"loc 3 lat "+ location.getLatitude());
//               Crashlytics.log(android.util.Log.ERROR,TAG,"loc 3 lon "+ location.getLongitude());
//               Toast.makeText( getApplicationContext(),"loc 3 lat "+ location.getLatitude()+ "loc 3 lon "+ location.getLongitude(),Toast.LENGTH_SHORT).show();
//               
           }
           else
           {
        	   Crashlytics.log(android.util.Log.ERROR, TAG, "location2 not null ");
           }
       }
		else
		{
			Crashlytics.log(android.util.Log.ERROR, TAG, "mlocManager not null ");
		}
		
		 }
		 else
		 {
			 Crashlytics.log(android.util.Log.ERROR, TAG, "location1 not null ");
		 }
		 
		}
        catch(NullPointerException n)
        {
        	Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity onCreate LocationManager NullPointerException " + n.getMessage());
        }
        catch(Exception e)
        {
        	e.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity onCreate LocationManager " + e.getMessage() + e.getStackTrace().toString());
        }
        
        
     // Session class instance
        session = new SessionManager(getApplicationContext());
        
        tiny =new TinyDB(getApplicationContext());
        
     // get AuthenticateDb data from session
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
         
        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
      
        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);    
                    
        // get user data from session
        HashMap<String, String> login_Dts = session.getLoginDetails();
         
        // Userid
        session_user_id = login_Dts.get(SessionManager.KEY_UID);
         
        // pwd
        session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);
        
        
     // get AuthenticateDb data from session
        HashMap<String, String> SingleCOD_Dts = session.getSingleCODDetails();
        
        Session_single_cod_amt = SingleCOD_Dts.get(SessionManager.KEY_SINGLE_AMOUNT);
        Crashlytics.log(android.util.Log.ERROR, TAG, "Session_single_cod_amt " + Session_single_cod_amt);
        
     // get user data from session
        HashMap<String, String> user = session.getUserDetails();
         
        // session_USER_LOC
        session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);


		//session_USER_LOC_ID
		session_USER_LOC_ID=user.get(SessionManager.KEY_USER_LOC_ID);

			// session_USER_NUMERIC_ID
        session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);
        
        // session_CURRENT_DT
        session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);
         
        // session_CUST_ACC_CODE
        session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);
        
        // session_USER_NAME
        session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);
        
        
        // get AssgnMultiple_num_Dts data from session
        HashMap<String, String> AssgnMultiple_num_Dts = session.getAssgnMultiple_num_Details();
         
        // DB_PATH
        session_RESPONSE_AWB_NUM = AssgnMultiple_num_Dts.get(SessionManager.KEY_RESPONSE_AWB_NUM);
        
        Crashlytics.log(android.util.Log.ERROR, TAG, "session_RESPONSE_AWB_NUM" + session_RESPONSE_AWB_NUM);
        
        
     // get AssgnMultiple_num_Dts data from session
        HashMap<String, String> AssgnMultiple_recevr_Dts = session.getAssgnMultiple_recevr_Details();
         
        // DB_PATH
        session_RESPONSE_RECEVR = AssgnMultiple_recevr_Dts.get(SessionManager.KEY_RESPONSE_RECEVR);
        
        Crashlytics.log(android.util.Log.ERROR, TAG, "session_RESPONSE_RECEVR" + session_RESPONSE_RECEVR);
        
        img = (ImageView)findViewById(R.id.sign);
//        Intent intent = getIntent();
//		 employeeId = intent.getStringExtra(TOMConDetailsActivity.EXTRA_MESSAGE);
//		 aa=intent.getStringExtra(UndeliveryActivity.EXTRA_MESSAGE);
// 
//		 if(employeeId == null)
//	        {
//	        	employeeId = aa;
//	        	aa=null;
//	        }
//		 employeeId=employeeId.replaceAll("null", "");		 
//		  bundle = this.getIntent().getExtras();
//		   PageName = bundle.getString("page_name");
//		   PageName1=bundle.getString("page_nme");
//		   if(PageName == null)
//	        {
//			   PageName = PageName1;
//			   PageName1=null;
//	        }
//		   PageName=PageName.replaceAll("null", "");
		   
		   received_by =(EditText)findViewById(R.id.et_associate_name);
//		   receiver_contct_numbr=(EditText)findViewById(R.id.et_associate_contct_no);
		   
//	        s = (Spinner)findViewById(R.id.spinnerDel);

        /** Code for signature */
	        
        tempDir = Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.external_dir) + "/";

//        if(session_DB_PATH != null && session_DB_PWD != null)
//      	  {
////	        db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
//        	db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//	    	cursor = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Assignment_Type,emp.T_Consignee_Name , emp.T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp.T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount,emp.T_Signature FROM TOM_Assignments  emp LEFT OUTER JOIN TOM_Assignments  mgr ON emp._id = mgr._id  where emp.T_Assignment_Number = ?", 
//	    			new String[]{""+employeeId});
//			Log.d("Tracking emp",employeeId);
//			while (cursor.moveToNext()){
//				awb=cursor.getString(cursor.getColumnIndex("T_Assignment_Number"));
//				emp_indx=cursor.getString(cursor.getColumnIndex("_id"));
//				Assignment_typ =cursor.getString(cursor.getColumnIndex("T_Assignment_Type"));
//				String shw_cod_amt = cursor.getString(cursor.getColumnIndex("F_Amount"));
////				Crashlytics.log(android.util.Log.ERROR,TAG,"shw_cod_amt " + shw_cod_amt);
//				single_cod_amt = (TextView)findViewById(R.id.codtotal);
//				single_cod_amt.setText("COD : " + shw_cod_amt + ".00");
//			}
//			cursor.close();
//			db.close();
//      	  }
//    	else
//		{
//			Crashlytics.log(android.util.Log.ERROR,TAG,"Error HandOver_DtlsActivity step 1 TOMLoginUserActivity.file is null ");
//			//onClickGoToHomePage();
//		}
//        uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + awb;   // + "_" + Math.random();
//        awb=awb.replace("/", "");
//        current = awb + ".png";       
//        mypath= new File( Environment.getExternalStorageDirectory()
//	               + "/Android/data/com.navatech.tom/", "signatures" + "/" + current);//new File( Environment.getExternalStorageDirectory(), "signatures" + "/" + current);   //File(tempDir,current);      
//        Crashlytics.log(android.util.Log.ERROR,TAG,"tempDir " + tempDir);
//        mContent = (LinearLayout) findViewById(R.id.linearLayout);
//        mSignature = new signature(this, null);      
//        mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);   
//
//        mGetDelivered = (Button)findViewById(R.id.btnsave);    
//        btnsgn = (Button)findViewById(R.id.signature_title);
//        btnreset = (Button)findViewById(R.id.reset);
//        btnpicture = (Button)findViewById(R.id.picture);
//        mView = mContent;
//
//        imageview = (Button) this.findViewById(R.id.picture);
//        imageview.setOnClickListener(new View.OnClickListener() {
//        	@Override
//			public void onClick(View v) {
//        		if(clicked) {
//
//        			takePhoto();
//
//               } else {
//            	   	takePhoto();
//               }
//
//               clicked = true;
//        		
//        	}});  
//       
//        cancelButton = (Button) findViewById(R.id.btncancel);		

//        String shw[] = session_RESPONSE_AWB_NUM.split("/");
//        for(int i = 0 ; i< shw.length ; i++)
//        {
//         result = shw[i];
//        }
//        uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + result;   // + "_" + Math.random();
//        current = result + ".png";       
//        mypath= new File( Environment.getExternalStorageDirectory()
//	               + "/Android/data/com.navatech.tom/", "Signatures" + "/" + current);//new File( Environment.getExternalStorageDirectory(), "Signatures" + "/" + current);   //File(tempDir,current);      
       
        mContent = (LinearLayout) findViewById(R.id.linearLayout);
        mSignature = new signature(this, null);      
        mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);    
//        mClear = (Button)findViewById(R.id.reset);
        mGetDelivered = (Button)findViewById(R.id.btnsave);    
        btnsgn = (Button)findViewById(R.id.signature_title);
//        btnreset = (Button)findViewById(R.id.reset);
        btnpicture = (Button)findViewById(R.id.picture);
        mView = mContent;
      
//        imageview = (Button) this.findViewById(R.id.picture);
//        imageview.setOnClickListener(new View.OnClickListener() {
//        	@Override
//			public void onClick(View v) {
//        		if(clicked) {
//        			takePhoto();
////                    Toast.makeText(TOMCODUpdateActivity.this, "You already clicked!", Toast.LENGTH_SHORT).show();
//               } else {
//            	   	takePhoto();
////                    Toast.makeText(TOMCODUpdateActivity.this, "You clicked for the first time!", 1000).show();
//               }
//
//               clicked = true;
////        		takePhoto();  
//        		
//        	}}); 
       
        cancelButton = (Button) findViewById(R.id.btncancel);	
         final Calendar c = Calendar.getInstance();
         String month=c.get(Calendar.MONTH)+1+"";
         if(month.length()<2){
              month="0"+month;   
         }  
         String date=c.get(Calendar.DAY_OF_MONTH)+"";
         if(date.length()<2)
         {
         	date="0"+date;
         }
         mYear= c.get(Calendar.YEAR);
         mMonth = c.get(Calendar.MONTH);
         mDay = c.get(Calendar.DAY_OF_MONTH);
         hour = c.get(Calendar.HOUR_OF_DAY);
         minute = c.get(Calendar.MINUTE);

         showdate =""+ c.get(Calendar.YEAR)+ "-" +month+ "-" +
                 date;
         showtime =" " +
                 c.get(Calendar.HOUR_OF_DAY) + ":" +
                 c.get(Calendar.MINUTE) +  ":" +
                 c.get(Calendar.SECOND);  
       
         Crashlytics.log(android.util.Log.ERROR, TAG, "showtime " + showtime);
         Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
         Date currentLocalTime = cal.getTime();
         DateFormat date1 = new SimpleDateFormat("HH:mm:ss z");   
         date1.setTimeZone(TimeZone.getTimeZone("GMT")); 
         String localTime = date1.format(currentLocalTime); 

//      /** Create an ArrayAdapter using the string array and a default spinner layout*/
//         ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                 R.array.relationship, android.R.layout.simple_spinner_item);
//         /** Specify the layout to use when the list of choices appears*/
//         adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//         /** Apply the adapter to the spinner*/         
//         
//         s.setAdapter(adapter);
//         s.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner());
         
         cancelButton.setOnClickListener(new OnClickListener()
         {
        	 @Override
			public void onClick(View v)
        	 {
        		 	Intent goToNextActivity = new Intent(getApplicationContext(), HandOverMainActivity.class);
////        	        goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        	        startActivity(goToNextActivity);
//        		 finish();
        	 }
         });  

	}
	catch(Exception e)
	{
		e.getStackTrace();
		Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity onCreate " + e.getMessage());
		//onClickGoToHomePage();
	}
	catch(UnsatisfiedLinkError err)
	{
		err.getStackTrace();
		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
		//onClickGoToHomePage();
	}
    }

	 /* Class My Location Listener */

   	public class MyLocationListener implements LocationListener

   	{

   	@Override

   	public void onLocationChanged(Location loc)

   	{

   		new_lat=String.valueOf(loc.getLatitude());

   		new_lon=String.valueOf(loc.getLongitude());

   		if (new_lat == null ) {
   	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat null");
   	    	new_lat="null";
   	    } else {
   	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat not null ");
   	    }
      		if (new_lon == null ) {
   	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon null");
   	    	new_lon="null";
   	    } else {
   	    	Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon not null ");
   	    }
//   	String Text = "My current location is: " +"Latitud = " + loc.getLatitude() +"Longitud = " + loc.getLongitude();

//   	Crashlytics.log(android.util.Log.ERROR,TAG,"Text " + Text);
//   	Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();

   	}

   	@Override

   	public void onProviderDisabled(String provider)

   	{

//   	Toast.makeText( getApplicationContext(),"Gps Disabled",	Toast.LENGTH_SHORT ).show();
// // create GPSTracker class object
//    gps = new GPSTracker(HandOver_DtlsActivity.this);
//   	gps.showSettingsAlert();
   	}

   	@Override

   	public void onProviderEnabled(String provider)

   	{

//   	Toast.makeText( getApplicationContext(),"Gps Enabled",Toast.LENGTH_SHORT).show();

   	}

   	@Override

   	public void onStatusChanged(String provider, int status, Bundle extras)

   	{

   	}

   	}/* End of Class MyLocationListener */
   	
    @Override
    protected void onDestroy() 
    {
    	try
		{
        	super.onDestroy();
        	stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
        	stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
        	android.os.Process.killProcess(android.os.Process.myPid());
        	System.exit(0);
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity onDestroy " + e.getMessage());
			//onClickGoToHomePage();
		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
//    		stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
//        	stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
//        	android.os.Process.killProcess(android.os.Process.myPid());
//        	System.exit(0);
    	}
    }
 
    private String getTodaysDate() { 
 
        final Calendar c = Calendar.getInstance();
       int todaysDate =     (c.get(Calendar.YEAR) * 10000) +     
       ((c.get(Calendar.MONTH) + 1) * 100) + 
      (c.get(Calendar.DAY_OF_MONTH));     
      return(String.valueOf(todaysDate));
 
    }
 
    private String getCurrentTime() {
 
        final Calendar c = Calendar.getInstance();        
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm:ss z");   
        date.setTimeZone(TimeZone.getTimeZone("GMT")); 
        String localTime = date.format(currentLocalTime); 
        currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
        (c.get(Calendar.MINUTE)) + ":" +
        (c.get(Calendar.SECOND));
        return(String.valueOf(currentTime));
 
    }
 
    public class signature extends View 
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();
 
        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();
 
        public signature(Context context, AttributeSet attrs) 
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }
 
        public void save(View v) 
        {
        	 try
             { 
	            if(mBitmap == null)
	            {
	               mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
	                
	            }         
            	Canvas canvas = new Canvas(mBitmap);
              FileOutputStream mFileOutStream = new FileOutputStream(mypath);           
              v.draw(canvas); 
              mBitmap.compress(Bitmap.CompressFormat.PNG, 100,mFileOutStream);         
              Bitmap bitmapOrg =Media.getBitmap(getContentResolver(), Uri.fromFile(mypath));
              ByteArrayOutputStream hrhk = new ByteArrayOutputStream();
              bitmapOrg.compress(Bitmap.CompressFormat.PNG, 100, hrhk);
              HRHK = hrhk.toByteArray();              
              bb =Base64.encode(HRHK);              
              byte [] converttobyte =bb.getBytes();            
              s1=bb.replace("\n", "").replace("\r", "").replaceAll(" ", "");                             
              s2=s1.replace("\n", "").replace("\r", "").replaceAll(" ", "");                  
              s3=s2.replace("\n", "").replace("\r", "").replaceAll(" ", "");
              byte[] BA2 =Base64.decode(bb);
              hrhk.flush();
              hrhk.close();
              String url = Media.insertImage(getContentResolver(), mBitmap, "title", null);
          
             //In case you want to delete the file
             //boolean deleted = mypath.delete();
             //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
             //If you want to convert the image to string use base64 converter
             //   String vv= Base64.encode(ss);
             // Log.d("ssss",vv);
            }
            catch(Exception e) 
            { 
            	Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity signature " + e.getMessage());
            	//onClickGoToHomePage();
            } 
    	   catch(UnsatisfiedLinkError err)
    		{
    			err.getStackTrace();
    			Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
    			//onClickGoToHomePage();
    		}
            
        }
 
        public void clear() 
        {
        	try
    		{
            path.reset();
            invalidate();
	        }
			catch(Exception e)
			{
				e.getStackTrace();
				Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity clear " + e.getMessage());
				//onClickGoToHomePage();
			}
        	catch(UnsatisfiedLinkError err)
        	{
        		err.getStackTrace();
        		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
        		//onClickGoToHomePage();
        	}
        }
 
        @Override
        protected void onDraw(Canvas canvas) 
        {
            canvas.drawPath(path, paint);
        }
 
        @Override
        public boolean onTouchEvent(MotionEvent event) 
        {
        	try
    		{
            float eventX = event.getX();
            float eventY = event.getY();
      
            mGetDelivered.setEnabled(true);
            switch (event.getAction()) 
            {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(eventX, eventY);
                lastTouchX = eventX;
                lastTouchY = eventY;
                return true;
 
            case MotionEvent.ACTION_MOVE:
 
            case MotionEvent.ACTION_UP:
 
                resetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();
                for (int i = 0; i < historySize; i++) 
                {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);
                    expandDirtyRect(historicalX, historicalY);
                    path.lineTo(historicalX, historicalY);
                }
                path.lineTo(eventX, eventY);
                break;
 
            default:
                debug("Ignored touch event: " + event.toString());
                return false;
            }
 
            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));
 
            lastTouchX = eventX;
            lastTouchY = eventY;
        }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity onTouchEvent " + e.getMessage());
		}
            return true;
        }
 
        private void debug(String string){
        }
 
        private void expandDirtyRect(float historicalX, float historicalY) 
        {
            if (historicalX < dirtyRect.left) 
            {
                dirtyRect.left = historicalX;
            } 
            else if (historicalX > dirtyRect.right) 
            {
                dirtyRect.right = historicalX;
            }
 
            if (historicalY < dirtyRect.top) 
            {
                dirtyRect.top = historicalY;
            } 
            else if (historicalY > dirtyRect.bottom) 
            {
                dirtyRect.bottom = historicalY;
            }
        }
 
        private void resetDirtyRect(float eventX, float eventY) 
        {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
    
 /**Listener Implementation of Spinner For Selecting Room*/

    public static class Listener_Of_Selecting_Room_Spinner implements OnItemSelectedListener 
    {
        static String RoomType;

        @SuppressLint("NewApi")
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) 
        {   
        	try
    		{
        		
            // By using this you can get the position of item which you
            // have selected from the dropdown 

            RoomType = (parent.getItemAtPosition(pos)).toString();  
            ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
       
           if(!RoomType.equals("SELECT"))
           {
        	   received_by.setEnabled(true);
        	   btnsgn.setEnabled(true);
//        	   btnreset.setEnabled(true);
        	   btnpicture.setEnabled(true);
           if(RoomType.equals("SELF"))
           {
        	   
        	   if(session_DB_PATH != null && session_DB_PWD != null)
             	  {
//		        	db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);	
        		   db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
			   		@SuppressWarnings("unused")
					String strFilter = "T_Assignment_Number='" + employeeId +"' ";
			   		@SuppressWarnings("unused")
					String isCOD="";	 
			   		Cursor c_chk = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount,emp.T_Signature FROM TOM_Assignments   emp LEFT OUTER JOIN TOM_Assignments   mgr ON emp._id = mgr._id  where emp.T_Assignment_Number = ?", 
			      			new String[]{""+employeeId});			   		
			   		while (c_chk.moveToNext()){
			   			name=c_chk.getString(c_chk.getColumnIndex("T_Consignee_Name"));
//			   			Crashlytics.log(android.util.Log.ERROR,TAG,"Consignee name" + name);
			   			received_by.setText(name);	
			   			
			   			/***code used to clear the signature*****/
			   			if(iv == null || iv.equals("null"))
			   			{
			   				Crashlytics.log(android.util.Log.ERROR, TAG, "Image of the image-view has no image");
			   			}
			   			else if(iv != null)
			   			{			   				
			   				iv.setImageBitmap(null);
			   				iv.destroyDrawingCache();
			   				sign1 = "null";			   			
			   			}		
			   		}			   		
			   		c_chk.close();
			   		db.close();
             	  }
		       	else
		   		{
		   			Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity step 2 session_DB_PATH is null ");
//		   			HandOver_DtlsActivity codupdte = new HandOver_DtlsActivity();
//		   			codupdte.onClickGoToHomePage();		   			
		   		}       	  
        	   
           }
           else
           {
        	   received_by.setText("");  
        		/***code used to clear the signature*****/
        	   if(iv == null || iv.equals("null"))
	   			{
	   				Crashlytics.log(android.util.Log.ERROR, TAG, "Image of the image-view has no image");
	   			}
	   			else if(iv != null)
	   			{	   				
	   				iv.setImageBitmap(null);
	   				iv.destroyDrawingCache();
	   				sign1 = "null";  			
	   			}
        	   sign1 = null;
          	   theImage = null;
          	   iv  = null;
           }           
           }
           else
      		{
      		 received_by.setEnabled(false);
      		 btnsgn.setEnabled(false);

   			 btnpicture.setEnabled(false); 
   			 received_by.setText("");  
   			 sign1 = null;
   			 theImage = null;
      	   	 iv  = null;
      		}
        }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Listener_Of_Selecting_Room_Spinner " + e.getMessage());
//			HandOver_DtlsActivity codupdte = new HandOver_DtlsActivity();
//   			codupdte.onClickGoToHomePage();	
		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
//    		HandOver_DtlsActivity codupdte = new HandOver_DtlsActivity();
//   			codupdte.onClickGoToHomePage();	
    	}
        }
        @Override
		public void onNothingSelected(AdapterView<?> parent) 
        {           
            // Do nothing.
        }
     
     }
    
    /**FUNCTION TO CAPTURE IMAGE/PHOTO**/
    
    private void takePhoto(){
    	try
    	{
    		new SoapAccessTask().execute();
//	       final Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//	       image = getFile(this);
//	       intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(image) ); 
//	       startActivityForResult(intent, TAKE_PHOTO_CODE);  
    	}
    	catch(Exception e)
    	{
    		e.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Exception takePhoto " + e.getMessage());
    	}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
    		//onClickGoToHomePage();	
    	}
	     }  

    
    private File getFile(Context context)
    {  
    	try
    	{
//	       final File path = new File( Environment.getExternalStorageDirectory(), "Image keeper" ); 	
//    		final File path = new File(Environment.getExternalStorageDirectory()
//    	               + "/Android/data/com.traconmobi.parekh/", "Image keeper"+session_CURRENT_DT );
    		final File path = new File( Environment.getExternalStorageDirectory()
 	               + "/Android/data/com.navatech.tom/", "Image keeper" ); 
	       photoname=null;
	       if(!path.exists()){  
	         path.mkdir(); 
	       }
	       try {
//	    	   File sdcard = Environment.getExternalStorageDirectory();
//	    	    File mymirFolder = new File(sdcard.getAbsolutePath() + "/Image keeper/");
//	    	    if(!mymirFolder.exists())
//	    	    {
//	    	        File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
//	    	        noMedia.mkdirs();
//	    	        noMedia.createNewFile();
//	    	    }
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
	    	   File sdcard = new File(Environment.getExternalStorageDirectory()
		               + "/Android/data/com.traconmobi.parekh"); 
//	    	   File mymirFolder = new File(sdcard.getAbsolutePath()+ "/Image keeper'" + session_CURRENT_DT + "' /");
	    	    File mymirFolder = new File(sdcard.getAbsolutePath() + "/Image keeper/");
	    	    if(!mymirFolder.exists())
	    	    {
	    	        File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
	    	        noMedia.mkdirs();
	    	        noMedia.createNewFile();
	    	    }
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	       employeeId=employeeId.replace("/", "");
	       photoname= employeeId + ".png";
	       fileimage = new File(path, photoname);			
    	}
    	
		catch(SQLiteException ex)
		{
			ex.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception getFile  SQLiteException" + ex.getMessage());
			//onClickGoToHomePage();	
		}
	     catch(Exception e) 
	     {
	    	 e.getStackTrace();
	    	 Crashlytics.log(android.util.Log.ERROR, TAG, "Exception getFile " + e.getMessage());
	    	 //onClickGoToHomePage();	
	     }
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
    		//onClickGoToHomePage();	
    	}
    	return fileimage; 
	      
	     }  
    
    //***********************************FUNCTION TO GET THE SIGNATURE OF THE RECIEVER**************************************//
    
    public void getsignature(View v)
    {
    	try
		{
    		Intent intent = new Intent(HandOver_DtlsActivity.this,CaptureSignature.class);
//        	intent.putExtra(EXTRA_MESSAGE, employeeId);
    		Crashlytics.log(android.util.Log.ERROR, TAG, "sending session_RESPONSE_RECEVR.." + session_RESPONSE_RECEVR);
    		
//    		Crashlytics.log(android.util.Log.ERROR,TAG,"sending .." + session_RESPONSE_AWB_NUM);
    		
//    		session_RESPONSE_RECEVR
    		
//    		String shw[] = session_RESPONSE_RECEVR.toString().trim().split(",");
//            for(int i = 0 ; i< shw.length ; i++)
//            {
//             result = shw[i];
//             Crashlytics.log(android.util.Log.ERROR,TAG,"temp result" +result);
//            }
           
////    		  List<String> items = new List<String>(session_RESPONSE_AWB_NUM.toString().split("|"));
//    		
//    		List<String> items = new ArrayList<String>();
//    		items.add(session_RESPONSE_AWB_NUM);
//    		 String[] stringsArray = new String[items.size()];
//    		 Crashlytics.log(android.util.Log.ERROR,TAG,"sending stringsArray.." + stringsArray);
//    		 
//    		 for(int i=0; i<items.size(); i++)
//    		 {
//    			    Crashlytics.log(android.util.Log.ERROR,TAG,"items" + items.get(i));
//    		 }
//    		for(String temp:items){
//    			Crashlytics.log(android.util.Log.ERROR,TAG,"temp" +temp);
//    			
//    		}
//    				new ArrayList<String>(session_RESPONSE_AWB_NUM.toString().split("|"));
//    		  for(int i= 0 ; i<items.size() ; i++)
//              {
////              foreach (String item in items)
////              {
//                  sbinner = item;
//              }
//    		
//        	 String shw[] = session_RESPONSE_AWB_NUM.toString().split("|");
//        	 Crashlytics.log(android.util.Log.ERROR,TAG,"sending lenght .." + shw.length);
//        	 Crashlytics.log(android.util.Log.ERROR,TAG,"sending lenght .." +shw[0]);
//             for(int i= 0 ; i<shw.length ; i++)
//             {
//              result = shw[0];
//              Crashlytics.log(android.util.Log.ERROR,TAG,"sending result .." + result);
//              String rcvr[] = result.split(",");
//	        	for(int j = 0 ; j< rcvr.length ; j++)
//		        {
//		        result_rcvr = rcvr[j];
//		        Crashlytics.log(android.util.Log.ERROR,TAG,"sending result_rcvr .." + result_rcvr);
//              }
//             }
//        	intent.putExtra(EXTRA_MESSAGE,"850/1203");
             intent.putExtra(EXTRA_MESSAGE,session_RESPONSE_RECEVR);
        	startActivityForResult(intent, SIGNATURE_ACTIVITY);
        	 mGetDelivered.setEnabled(true);
	    }
	    catch(Exception e) 
	    {
	   	 e.getStackTrace();
	   	 Crashlytics.log(android.util.Log.ERROR, TAG, "Exception getsignature " + e.getMessage());
	   	 //onClickGoToHomePage();	
	    }
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
    		//onClickGoToHomePage();	
    	}
    }
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	try
		{
//    	super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
        //********************************Activity Result for Photo/Image **************************//
        case TAKE_PHOTO_CODE: 
        	 if(requestCode == TAKE_PHOTO_CODE){
        	 if (resultCode == RESULT_OK) {
        		
        	            try { 
		        	              Toast toast = Toast
		                      			.makeText(this, "Photo capture Successfull",Toast.LENGTH_SHORT);
		                      			toast.setGravity(Gravity.CENTER, 0, 0);
		                      			toast.show();

		        	            } 
		        	            catch (SQLiteException e) {
		        	                // TODO: handle exception
		        	                e.printStackTrace();
		        	                Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity TAKE_PHOTO_CODE SQLiteException ");
		        	                //onClickGoToHomePage();

		        	            }
        	                    catch(Exception e)
        	                    {
        	                    	e.printStackTrace();
		        	                Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity TAKE_PHOTO_CODE Exception ");
        	                    }
		        	            catch(UnsatisfiedLinkError err)
		        	        	{
		        	        		err.getStackTrace();
		        	        		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity TAKE_PHOTO_CODE UnsatisfiedLinkError ");
		        	        		//onClickGoToHomePage();	
		        	        	}
        	            		catch(OutOfMemoryError o)
        	            		{
        	            			o.getStackTrace();
        	            			Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity TAKE_PHOTO_CODE OutOfMemoryError ");
//        	            			Intent goToNextActivity = new Intent(getApplicationContext(), TOMContactActivity.class);
//        	            			startActivity(goToNextActivity);
        	            		}

        	            finally {
//        	            	if (db != null) 
//        	                {
//        	              	  db.close();
//        	                }
							// This block contains statements that are ALWAYS executed
							// after leaving the try clause, regardless of whether we leave it:
							// 1) normally after reaching the bottom of the block;
							// 2) because of a break, continue, or return statement;
							// 3) with an exception handled by a catch clause above; or
							// 4) with an uncaught exception that has not been handled.
							// If the try clause calls System.exit(), however, the interpreter
							// exits before the finally clause can be run.
							}
		        	          break;  
		
		        	        } 
        	 
        	 else if ( resultCode == RESULT_CANCELED) {
        		 photoname ="null";
//              Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
              Toast toast = Toast
            			.makeText(this, "Picture was not taken",Toast.LENGTH_SHORT);
            			toast.setGravity(Gravity.BOTTOM, 105, 50);
            			toast.show();

          } else {
        	  photoname ="null";
//              Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
              Toast toast = Toast
            			.makeText(this, "Picture was not taken",Toast.LENGTH_SHORT);
            			toast.setGravity(Gravity.BOTTOM, 105, 50);
            			toast.show();

          }
		        	 }
		        	      else
		        	      {
		        	    	  photoname ="null";
		        	    	  Toast toast = Toast
		                      			.makeText(this, "No Photo captured",Toast.LENGTH_SHORT);
		                      			toast.setGravity(Gravity.BOTTOM, 105, 50);
		                      			toast.show();
		        	      }
        //********************************Activity Result for signature **************************//
        case SIGNATURE_ACTIVITY:
            if (resultCode == RESULT_OK) {
 
                Bundle bundle = data.getExtras();
                String status  = bundle.getString("status");
                @SuppressWarnings("unused")
				String sign =bundle.getString("getsign");
                sign1 = CaptureSignature.s1;
                Crashlytics.log(android.util.Log.ERROR, TAG, "decoded string" + sign1);
                 if(sign1 != null)
                {
               
                if(status.equalsIgnoreCase("done")){
                	
                	 @SuppressWarnings("unused")
     				ByteArrayOutputStream bao = new ByteArrayOutputStream();   
                     
                     byte[] ba2 =Base64.decode(sign1);      

                     
                     ByteArrayInputStream imageStream_sign = new ByteArrayInputStream(ba2);
                     Crashlytics.log(android.util.Log.ERROR, TAG, "stream");
                     Crashlytics.log(android.util.Log.ERROR, TAG, "imageStream");
                 		  theImage = BitmapFactory.decodeStream(imageStream_sign);
                 		 iv = (ImageView)findViewById(R.id.sign);            		
                 		iv.setImageBitmap(theImage);
     		            iv.setScaleType(ImageView.ScaleType.FIT_XY);
     		            iv.setAdjustViewBounds(true);
     		            
     		            
                    Toast toast = Toast.makeText(this, "Signature capture successful!", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    mGetDelivered.setEnabled(true);
            		mGetDelivered.setBackgroundResource(R.drawable.button);
                }
                else  if(status.equalsIgnoreCase("cancel")){
                	sign1 = null;
                	   theImage = null;
                	iv  = null;
            		 iv = (ImageView)findViewById(R.id.sign);            		
            		iv.setImageBitmap(theImage);
		            iv.setScaleType(ImageView.ScaleType.FIT_XY);
		            iv.setAdjustViewBounds(true);
		            
                
                    Toast toast = Toast.makeText(this, "Signature not captured !", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            }
                 else // if(sign1 == null)
                 {
                	 sign1 = null;
               	   	 theImage = null;
               	   	 iv  = null;
              		 iv = (ImageView)findViewById(R.id.sign);            		
              		 iv.setImageBitmap(theImage);
  		             iv.setScaleType(ImageView.ScaleType.FIT_XY);
  		             iv.setAdjustViewBounds(true);
  		            
                 	Toast toast = Toast
                 			.makeText(this, "Please capture Signature!",Toast.LENGTH_SHORT);
                 			toast.setGravity(Gravity.CENTER, 0, 0);
                 			toast.show();
                 }
                
            break;
        }
        }
    }
	catch(Exception e)
	{
		e.getStackTrace();
		Crashlytics.log(android.util.Log.ERROR, TAG, "Exception onActivityResult " + e.getMessage());
		//onClickGoToHomePage();
	}
	catch(UnsatisfiedLinkError err)
	{
		err.getStackTrace();
		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
		//onClickGoToHomePage();	
	}
    }  
    
  //starting asynchronus task
    class SoapAccessTask extends AsyncTask<String, Void, Void> {    
    	
        @Override
        protected void onPreExecute() {
             //if you want, start progress dialog here
        	// NOTE: You can call UI Element here.

        }

		@Override
		protected Void doInBackground(String... params) 
		{
			try
			{
				final Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		       image = getFile(HandOver_DtlsActivity.this);
		       intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(image) ); 
		       startActivityForResult(intent, TAKE_PHOTO_CODE);  
			}
			catch(Exception e)
			{
				e.getStackTrace();
				Crashlytics.log(android.util.Log.ERROR, TAG, "Exception TAKE_PHOTO_CODE " + e.getMessage());
			}
			catch(UnsatisfiedLinkError err)
	    	{
	    		err.getStackTrace();
	    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
	    		//onClickGoToHomePage();	
	    	}
			return null;
		}
		@Override
		protected void onPostExecute(Void unused)
		{
	         // NOTE: You can call UI Element here.
			 // Close progress dialog

		}
    }
    
    public void Onclk_cancel(View v)
    {
    	Intent goToNextActivity = new Intent(getApplicationContext(), HandOverMainActivity.class);
    	////    goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	startActivity(goToNextActivity);
    }

  /**METHOD TO SAVE COD DETAILS */
    public void On_handover_submit(View v) 
    {	
    	try
  		 {
		/** Get the values provided by the user via the UI **/
		
		received_by = (EditText)findViewById(R.id.et_associate_name);
//		receiver_contct_numbr=(EditText)findViewById(R.id.et_associate_contct_no);
		Receiver = received_by.getText().toString().trim();	
//		final String Receiver_num = receiver_contct_numbr.getText().toString().trim();
		final String date = showdate;
		final String time = showtime;
//		mGetDelivered.setEnabled(false);
//		mGetDelivered.setBackgroundColor(Color.GRAY);
//		 st =s.getSelectedItem().toString(); 
   		 
   		if(Receiver.isEmpty())  
        {
        Toast toast= Toast.makeText(HandOver_DtlsActivity.this,
				"Please enter dispatch associate name !!", Toast.LENGTH_LONG) ;
         toast.setGravity(Gravity.CENTER,0,0);
		 toast.show();
//        received_by.setEnabled(false);
//        mGetDelivered.setEnabled(true);
//		mGetDelivered.setBackgroundResource(R.drawable.button);
        }
   		
   		
//   		else if(Receiver_num.isEmpty())
//		 {
//			 Toast.makeText(HandOver_DtlsActivity.this,  
//                     "Please enter contact number !!", Toast.LENGTH_LONG)  
//                     .show();  
//			 mGetDelivered.setEnabled(true);
//			 mGetDelivered.setBackgroundResource(R.drawable.button);
//		 }
		 
		 else{
			
//         if(Receiver.toString()!= null && st.equals("SELECT"))  
//         {  
//      
//         Toast.makeText(HandOver_DtlsActivity.this,  
//                                    "Please Select the Relationship type !!", Toast.LENGTH_LONG).show();  
//     	
//          mGetDelivered =(Button)findViewById(R.id.btnsave);
//          Toast.makeText(HandOver_DtlsActivity.this,  
//                  "Please Sign !!", Toast.LENGTH_LONG).show();  	
//		 mGetDelivered.setEnabled(false);
//         return;                      
//                 		
//              }    

//       if(Receiver.toString()!= null && Receiver_num.toString()!= null)
			   if(Receiver.toString()!= null)
		{ 	
 	
//		mView.setDrawingCacheEnabled(true);
//		mSignature.save(mView);






//	if(session_DB_PATH != null && session_DB_PWD != null)
//  	  {
////		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
//		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
////      	 final String employeeId = getIntent().getStringExtra(TOMContactActivity.EXTRA_MESSAGE );
//		String strFilter = "_id='" + employeeId +"' ";//"T_Assignment_Number='" + employeeId +"' ";
//		String isCOD="";	 
//		cursor = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount,emp.T_Signature,emp.B_is_Completed,mgr.T_IMEI FROM TOM_Assignments emp JOIN TOM_Users  mgr ON mgr.T_U_ID= emp.T_OUT_Scan_U_ID  where emp.T_Assignment_Number = ?", 
//    			new String[]{""+employeeId});
//	
//		while (cursor.moveToNext())
//		{
//		new ContentValues();		
//	
//		room = Listener_Of_Selecting_Room_Spinner.RoomType;
//		room.toString();
//		awb=cursor.getString(cursor.getColumnIndex("T_Assignment_Number"));	
//		imei_num=cursor.getString(cursor.getColumnIndex("T_IMEI"));
//		r_sign =cursor.getString(cursor.getColumnIndex("T_Signature"));
//		update_pod =cursor.getString(cursor.getColumnIndex("B_is_Completed"));
//		cursor.getInt(cursor.getColumnIndex("_id"));
//		e_indx = cursor.getString(cursor.getColumnIndex("_id"));
//		isCOD =cursor.getString(cursor.getColumnIndex("B_is_Amountable"));
//		COD_Amount = cursor.getString(cursor.getColumnIndex("F_Amount"));
//		amt=Double.parseDouble(COD_Amount);	
//		
//		name=cursor.getString(cursor.getColumnIndex("T_Consignee_Name"));
//		
//		}	
//		cursor.close();
//		db.close();
//		}
//	   	else
//			{
//				Crashlytics.log(android.util.Log.ERROR,TAG,"Error HandOver_DtlsActivity step 5 TOMLoginUserActivity.file is null ");
//				//onClickGoToHomePage();
//			}
//		if( amt == 0) //isCOD.equals("0") &&
//		{
//			if(sign1 == null || sign1.isEmpty() || sign1.equals("null"))
//            {
//            	Toast toast = Toast
//            			.makeText(this, "Please capture Signature!",Toast.LENGTH_SHORT);
//            			toast.setGravity(Gravity.BOTTOM, 105, 50);
//            			toast.show();
//            			Crashlytics.log(android.util.Log.ERROR,TAG,"sign1 "+sign1);
//            }
//            else  if(sign1 != null || !(sign1.isEmpty()) || !(sign1.equals("null")))
//            {
            	AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			      
		        // Setting Dialog Title
		        alertDialog.setTitle("Alert");
		  
		        // Setting Dialog Message
		        alertDialog.setMessage("Are you sure you want to confirm ?");
		  
		        // On pressing Settings button
		        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
		            @Override
					public void onClick(DialogInterface dialog,int which) 
		            {
//            	 mGetDelivered.setEnabled(true);
//         		mGetDelivered.setBackgroundResource(R.drawable.button);
		            	try
		            	{
            	startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
            	if(session_DB_PATH != null && session_DB_PWD != null)
              	  {
//	            	db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);	
	            	db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//	            	String strFilter = "T_Assignment_Id='" + employeeId +"' ";
////	            	tiny.putString("DEL_UPDATE_ASSGN_ID", employeeId);
//	            	crs = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount FROM TOM_Assignments   emp LEFT OUTER JOIN TOM_Assignments   mgr ON emp._id = mgr._id  where emp.T_Assignment_Number = ? and emp.D_OutScan_Date='" + session_CURRENT_DT +"'", 
//	        			new String[]{""+employeeId});
//	            	while (crs.moveToNext())
//	            	{
//	    			ContentValues cv = new ContentValues();    			
//	    			cv.put("isDelivered", "1");
//	    			cv.put("isCOD_Collected ", "0");	
//	    			cv.put("COD_amt_collected", "0");    			
//	    			cv.put("Received_by", Receiver);     			
//	     			cv.put("Receiver_relationship", room);
//	     			cv.put("Delivered_Date",date);
//	     			// create class object
//	                gps = new GPSTracker(HandOver_DtlsActivity.this);
//	
//	                gps.getLocation();
//	                Location loc =  gps.getLocation();
//	    			cv.put("AcquiredLat", gps.getLatitude());
//	    			cv.put("AcquiredLong", gps.getLongitude());
//	     			cv.put("Delivered_Time",String.valueOf(currentTime));
//	     			cv.put("Receiver_Signature",sign1);
	            	Crashlytics.log(android.util.Log.ERROR, TAG, "shw session_RESPONSE_AWB_NUM" + session_RESPONSE_AWB_NUM);
	            	String shw[] = session_RESPONSE_AWB_NUM.toString().split("::");
	            	Crashlytics.log(android.util.Log.ERROR, TAG, "scan list result[]" + shw[0]);
			        for(int k = 0 ; k< shw.length ; k++)
			        {
			        	String result_shw = shw[k];
			        	Crashlytics.log(android.util.Log.ERROR, TAG, "shw comin results " + result_shw);
			        	String rcvr[] = result_shw.split(",");
			        	for(int j = 0 ; j< rcvr.length ; j++)
				        {
				        result_rcvr = rcvr[j];
				        awb_id= rcvr[0];
				        congn_nme= rcvr[1];
				        recvr_nme= rcvr[2];
				        amt_coltd= rcvr[3];
				        Crashlytics.log(android.util.Log.ERROR, TAG, "shw comin result_rcvr " + result_rcvr + awb_id + congn_nme + recvr_nme + amt_coltd);
	            		ContentValues cv = new ContentValues();    			
	        			cv.put("T_Assignment_Id",awb_id);
	        			cv.put("T_Handover_Assignment_Type ",getassgnmt); 
	        			cv.put("T_Consignee_Name",congn_nme);   
	        			cv.put("T_Received_by",recvr_nme);
	        			cv.put("F_Amount_Collected",amt_coltd);
	        			cv.put("T_Handover_Received_by_Collected_from", Receiver);     			
//	         			cv.put("T_Handover_Contact_number",Receiver_num);
	         			 final Calendar c = Calendar.getInstance();
	         	         String month=c.get(Calendar.MONTH)+1+"";
	         	         if(month.length()<2){
	         	              month="0"+month;   
	         	         }  
	         	         String shwdate=c.get(Calendar.DAY_OF_MONTH)+"";
	         	         if(shwdate.length()<2)
	         	         {
	         	        	shwdate="0"+shwdate;
	         	         }
	         	         mYear= c.get(Calendar.YEAR);
	         	         mMonth = c.get(Calendar.MONTH);
	         	         mDay = c.get(Calendar.DAY_OF_MONTH);
	         	         hour = c.get(Calendar.HOUR_OF_DAY);
	         	         minute = c.get(Calendar.MINUTE);

	         	         showdate =""+ c.get(Calendar.YEAR)+ "-" +month+ "-" +
	         	        		shwdate;
	         	        showtime =" " +
	         	                 c.get(Calendar.HOUR_OF_DAY) + ":" +
	         	                 c.get(Calendar.MINUTE) +  ":" +
	         	                 c.get(Calendar.SECOND);  
	         	        
	         			Crashlytics.log(android.util.Log.ERROR, TAG, "showdate " + showdate);
	         			cv.put("D_Handover_Date",showdate);
	         			cv.put("T_Out_Scan_Location",session_USER_LOC_ID);
	         			cv.put("T_OUT_Scan_U_ID",session_USER_NUMERIC_ID);
	         			cv.put("T_Cust_Acc_NO",session_CUST_ACC_CODE);
	        			cv.put("T_AcquiredLat",new_lat);
	        			cv.put("T_AccquiredLon",new_lon);
	        			Crashlytics.log(android.util.Log.ERROR, TAG, "D_Handover_time " + showtime);
	         			cv.put("D_Handover_time",showtime);
	         			if(sign1 == null || sign1.isEmpty() || sign1.equals("null"))
	         	            {
	         				sign1="null";
	         				cv.put("T_Handover_Signature",sign1);
	         	            }
	         			else  if(sign1 != null || !(sign1.isEmpty()) || !(sign1.equals("null")))
	         			{
	         				String sign_dt_tm=CaptureSignature.s1_datetime;
	         				cv.put("T_Handover_Signature",sign1);
	         				cv.put("T_Handover_Signature_dt",sign_dt_tm);
	         			}
//	         			cv.put("T_Handover_Signature",sign1);
//	     			if(clicked == false)
//	     			{
//	     				photoname = "null";	     			
//	     			}
//	     			cv.put("T_Photo",photoname);
//	     			db.update("TOM_Assignments",cv, strFilter,null);
//	         		db.insertOrThrow("TOM_HandOver_Assignments", null, cv);
	         		db.insertWithOnConflict("TOM_HandOver_Assignments", null, cv, SQLiteDatabase.CONFLICT_REPLACE);
	         		String flg="Y";
	         		Cursor c_updt=db.rawQuery("UPDATE TOM_Assignments SET C_Handover_sync='"+flg + "' where T_Assignment_Number='" + awb_id + "'", null);
	         		while(c_updt.moveToNext())
	         		{
	         			
	         		}
	         		c_updt.close();
	         		Crashlytics.log(android.util.Log.ERROR, TAG, "inserted values " + cv);
	         		
				        }
	            	}
//	    		crs.close();
	    		db.close();
	    		
//	    		Crashlytics.log(android.util.Log.ERROR,TAG,"imei_num " + imei_num);
	    		//Call to webservice gps insert   
	    		if (new_lat == null && new_lon == null)
	    		{
	    			Crashlytics.log(android.util.Log.ERROR, TAG, " gps null");
	    		}
	    		else
	    		{
	            entityString_gps ="IMEI="+imei_num+"&Acq_latitude="+new_lat+"&Acq_longitude="+new_lon+"&speed=0"+"&distance=0"+"&Accuracy=0"+"&Date="+date+"&Time="+String.valueOf(currentTime)+"&Net_latitude=0"+"&Net_longitude=0"+"&UserID="+session_USER_NAME+"&CUST_ACC_NO="+session_CUST_ACC_CODE;
	            Crashlytics.log(android.util.Log.ERROR, TAG, "entityString_gps " + entityString_gps);
	            Handler mHandler = new Handler(getMainLooper());
	            mHandler.post(new Runnable() 
	            {
	                @Override
	                public void run() 
	                {
	                	new Thread(new Runnable() 
	                	{ 
	      				    @Override
	      				        public void run() 
	      				         {

	      				    	
	      				    	/*int reqId = 109;
	    						HttpHandler handler_os = new HttpHandler(URL_gps_log + "/" + METHOD_NAME_gps_log + "?" + entityString_gps,null, null, reqId);
	    						handler_os.addHttpLisner(HandOver_DtlsActivity.this);								    						
	    						handler_os.sendRequest();*/
                                     OkHttpHandler handler_del = new OkHttpHandler(URL_gps_log + "/" + METHOD_NAME_gps_log + "?" + entityString_gps);
                                     handler_del.sendRequest();
	      				         }
	                	}).start();
	                }
	            });
              	  }
            }
    	   	else
    			{
    				Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity step 6 TOMLoginUserActivity.file is null ");
    				//onClickGoToHomePage();
    			}
//            	mGetDelivered.setEnabled(false);
//       		 mGetDelivered.setBackgroundColor(Color.GRAY);
    		Toast.makeText(HandOver_DtlsActivity.this,"Record Saved succesfully", Toast.LENGTH_LONG).show();
//    		if(session_DB_PATH != null && session_DB_PWD != null)
//          	  {
////	    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
//	    		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//	 			c_routesyn=db.rawQuery("UPDATE TBL_route_lan_lon SET C_Active_status=1 WHERE T_Assignment_Number = ?",
//	 					new String[]{""+employeeId});
//	 			while(c_routesyn.moveToNext()){
//	 				
//	 			}
//	 			c_routesyn.close();
//	 			db.close();
//            }
//    	   	else
//    			{
//    				Crashlytics.log(android.util.Log.ERROR,TAG,"Error HandOver_DtlsActivity step 7 TOMLoginUserActivity.file is null ");
//    				//onClickGoToHomePage();
//    			}
//
//	        try {
////	        	CallSmsDelUndelApi csms = new CallSmsDelUndelApi(getApplicationContext());//.main(new String[]{});
//// 				csms.callsms();
////				finish();
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
	        
//	        if(PageName.equals("from_delpage"))
// 			{
// 			count_del =bundle.getString("count_val");
// 			count_del_sync=count_del+1;
 			Intent goToNextActivity = new Intent(getApplicationContext(), HomeMainActivity.class);
// 			goToNextActivity.putExtra(DEL_UPDATE_ASSGN_ID, employeeId);
// 			Bundle bundle = new Bundle();
// 	        bundle.putString("counter", count_del_sync);
// 	        goToNextActivity.putExtras(bundle);
 			startActivity(goToNextActivity);
// 			
// 			}
// 			else if(PageName.equals("from_undelpage"))
// 			{
// 				Intent goToNextActivity = new Intent(getApplicationContext(), UndeliveryActivity.class);
//// 				goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//     			startActivity(goToNextActivity);
// 			}
		            }
		            catch(Exception e)
		            {
		            	Crashlytics.log(android.util.Log.ERROR, TAG, "Exception handover details " + e.getMessage());
		            }
            }
        });
  
        // on pressing cancel button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
//        	Intent goToNextActivity = new Intent(getApplicationContext(),TOMDel_pickup_screenActivity.class);
//			 startActivity(goToNextActivity);
//            	mGetDelivered.setEnabled(true);
//            	mGetDelivered.setBackgroundResource(R.drawable.button);
            dialog.cancel();
            }
        });
  
        // Showing Alert Message
        alertDialog.show();
//		}
//		}

//		    else if( amt > 0) //isCOD.equals("1") &&
//		    	{	
//		    	
//		    	if(sign1 == null || sign1.isEmpty() || sign1.equals("null"))
//	             {
//	             	Toast toast = Toast
//	             			.makeText(this, "Please capture Signature!",Toast.LENGTH_SHORT);
//	             			toast.setGravity(Gravity.BOTTOM, 105, 50);
//	             			toast.show();
//	             			
//	             }
//	             else  if(sign1 != null || !(sign1.isEmpty()) || !(sign1.equals("null")))
//	             {
//	            	 mGetDelivered.setEnabled(true);
//	            		mGetDelivered.setBackgroundResource(R.drawable.button);
//	            	 startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));	            	
//		    	NumberFormat formatter = new DecimalFormat("###.00");
//				str_amt = formatter.format(amt);				
//		    	LinearLayout layout = new LinearLayout(this);
//		    	layout.setOrientation(LinearLayout.VERTICAL);
//
//    	            final TextView tv1 = new TextView(this);
//    	            tv1.setText("COD Message");
//    	            tv1.setTextColor(Color.WHITE);
//    	            layout.addView(tv1);
//
//    	            final TextView tv2 = new TextView(this);
//    	            tv2.setText("COD TO BE COLLECTED : " + str_amt);
//    	            tv2.setTextColor(Color.WHITE);
//    	            layout.addView(tv2);
//	            
//	            /** Set an EditText view to get user input  */ 
//    	            final EditText input = new EditText(this); 
//    	            input.getInputType();
//    	            input.setSingleLine();
//    	            //setting text field limit to 15
//    	            int maxLength = 15;
//    	            input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
//    	            input.setInputType(InputType.TYPE_CLASS_NUMBER);		    	     
//    	            input.setKeyListener(DigitsKeyListener.getInstance("0123456789."));		    	         
//    	            layout.addView(input);	    	              
//            
//		            final TextView tv3= new TextView(this);	            
//		            tv3.setText("");
//		            tv3.setTextColor(Color.WHITE);
//		            layout.addView(tv3);	
//		            
//		            final Button btn =new Button(this);
//		            
//		            btn.setText("ok");
//		            btn.setMaxWidth(10);		    	              
//		            layout.addView(btn);		    	              
//		            btn.setOnClickListener(new View.OnClickListener() 
//		            {
//		            	@Override
//						public void onClick(View v) 
//		            	{
//		            		btn.setEnabled(false);
//	                	  if(input.getText().toString().equals(""))
//	                    		 {
//	                		  tv3.setText("Enter amount collected");
//	                		  btn.setEnabled(true);    	              
//	      		            
//	                    		 }
//	                     else if(amt > 0 )
//	                     {
//	                    	 
//	                    	 startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
//	                    	 if(Double.parseDouble(input.getText().toString()) ==  amt )  //if(input.getText().toString().equals(str_amt) || input.getText().toString().equals(COD_Amount))
//	 		        		{
//	                    		 btn.setEnabled(false);
//	                    		 btn.setBackgroundColor(Color.GRAY);
//	                    		 if(session_DB_PATH != null && session_DB_PWD != null)
//	         	              	  {
////		                    		 db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
//		                    		 db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//	//		    	 		            	final String employeeId = getIntent().getStringExtra(TOMContactActivity.EXTRA_MESSAGE );
//		                    		 String strFilter = "T_Assignment_Number='" + employeeId +"' ";
//		 		        		
//		                    		 cursor = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount FROM TOM_Assignments   emp LEFT OUTER JOIN TOM_Assignments   mgr ON emp._id = mgr._id  where emp.T_Assignment_Number= ? and emp.D_OutScan_Date='"+ session_CURRENT_DT +"' ", 
//		 		            			new String[]{""+employeeId});		                    		
//		                    		 while (cursor.moveToNext())
//		                    		 {
////			 		        			ContentValues cv = new ContentValues();		    	 		        		
////			 		        			cv.put("isDelivered", "1");
////			 		        			cv.put("Received_by", Receiver);		    	 		        			
////			 		        			cv.put("Receiver_relationship", room);
////			 		        			cv.put("Delivered_Date",date);
////			 		        		// create class object
////			 		                   gps = new GPSTracker(HandOver_DtlsActivity.this);
////		
////			 		                   gps.getLocation();
////			 		                   Location loc =  gps.getLocation();
////			 		       				Crashlytics.log(android.util.Log.ERROR,TAG,"tracker loc " + loc + gps.getLatitude());
////				 		       			cv.put("AcquiredLat", gps.getLatitude());
////				 		       			cv.put("AcquiredLong", gps.getLongitude());
////			 		        			cv.put("Delivered_Time",String.valueOf(currentTime));
////			 		        			cv.put("Receiver_Signature",sign1);
////			 		        			Crashlytics.log(android.util.Log.ERROR,TAG,"return click of button 3 " + clicked);
////			 		        			if(clicked == false)
////			 		        			{
////			 		        				photoname = "null";
////			 		        				Crashlytics.log(android.util.Log.ERROR,TAG,"photoname 3 " + photoname);
////			 		        			}
////			 		        			
////			 		        			cv.put("photo", photoname);		    	 		        			
////			 		        			cv.put("isCOD_Collected", "1");	
////			 		        			cv.put("COD_amt_collected", input.getText().toString());
////			 		        			db.update("TOM_Del",cv, strFilter,null);
//		
//		                    			 ContentValues cv = new ContentValues();		    	 		        		
//		 	 		        			cv.put("B_is_Completed", "TRUE");
//		 	 		        			cv.put("T_Received_by_Collected_from", Receiver);		    	 		        			
//		 	 		        			cv.put("T_Relationship", room);
//		 	 		        			cv.put("D_Completed_Date",date);
//
////		 				      Crashlytics.log(android.util.Log.ERROR,TAG,"new_lon " + new_lat + new_lon);
////		 	 		        			if (new_lat == null ) {
////		 			 				    	Crashlytics.log(android.util.Log.ERROR,TAG,"return null");
////		 			 				    } else {
////		 			 				    	Crashlytics.log(android.util.Log.ERROR,TAG,"return not null ");
////		 			 				    }
//		 	 		       			cv.put("T_AcquiredLat", new_lat);
//		 	 		       			cv.put("T_AccquiredLon",new_lon);
//		 		        			cv.put("D_Completed_time",String.valueOf(currentTime));
//		 		        			cv.put("T_Signature",sign1);
//		 	 		        			Crashlytics.log(android.util.Log.ERROR,TAG,"return click of button 3 " + clicked);
//		 	 		        			if(clicked == false)
//		 	 		        			{
//		 	 		        				photoname = "null";
////		 	 		        				Crashlytics.log(android.util.Log.ERROR,TAG,"photoname 3 " + photoname);
//		 	 		        			}
//		 	 		        			
//		 	 		        			cv.put("T_Photo", photoname);		    	 		        			
////		 	 		        			cv.put("B_is_Completed", "1");	
//		 	 		        			cv.put("F_Amount_Collected", input.getText().toString());
//		 	 		        			db.update("TOM_Assignments",cv, strFilter,null);
//		 		        			}
//			 		        		cursor.close();	
//			 		        		db.close();
//			 		        		
//			 		        		Crashlytics.log(android.util.Log.ERROR,TAG,"imei_num " + imei_num);
//			 			    		//Call to webservice gps insert     
//			 		        		if (new_lat == null && new_lon == null)
//			 			    		{
//			 			    			Crashlytics.log(android.util.Log.ERROR,TAG," gps >0  null");
//			 			    		}
//			 			    		else
//			 			    		{
//			 			            entityString_gps ="IMEI="+imei_num+"&Acq_latitude="+new_lat+"&Acq_longitude="+new_lon+"&speed=0"+"&distance=0"+"&Accuracy=0"+"&Date="+date+"&Time="+String.valueOf(currentTime)+"&Net_latitude=0"+"&Net_longitude=0"+"&UserID="+session_USER_NAME+"&CUST_ACC_NO="+session_CUST_ACC_CODE;
//			 			            Crashlytics.log(android.util.Log.ERROR,TAG,"entityString_gps " + entityString_gps);
//			 			            Handler mHandler = new Handler(getMainLooper());
//			 			            mHandler.post(new Runnable() 
//			 			            {
//			 			                @Override
//			 			                public void run() 
//			 			                {
//			 			                	new Thread(new Runnable() 
//			 			                	{ 
//			 			      				    @Override
//			 			      				        public void run() 
//			 			      				         {
//			 			      				    	
//			 			      				    	int reqId = 109;
//			 			    						HttpHandler handler_os = new HttpHandler(URL_gps_log + "/" + METHOD_NAME_gps_log + "?" + entityString_gps,null, null, reqId);
//			 			    						handler_os.addHttpLisner(HandOver_DtlsActivity.this);								    						
//			 			    						handler_os.sendRequest();
//			 			      				         }
//			 			                	}).start();
//			 			                }
//			 			            });
//			 			    		}
//	         	              	  }
//	                 	   	else
//	                 			{
//	                 				Crashlytics.log(android.util.Log.ERROR,TAG,"Error HandOver_DtlsActivity step 8 TOMLoginUserActivity.file is null ");
//	                 				//onClickGoToHomePage();
//	                 			}
//	 		        		btn.setEnabled(false);
//	 		        		btn.setBackgroundColor(Color.GRAY);
//	 		        		
//	 		        			Toast.makeText(HandOver_DtlsActivity.this,"Record Saved succesfully", Toast.LENGTH_LONG).show();		
//	 		        			if(session_DB_PATH != null && session_DB_PWD != null)
//	 	      	              	  {
////		 		        			db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
//		 		        			db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//		 		        			c_routesyn=db.rawQuery("UPDATE TBL_route_lan_lon SET C_Active_status=1 WHERE T_Assignment_Number = ?",
//		 		        					new String[]{""+employeeId});
//		 		        			while(c_routesyn.moveToNext()){
//		 		        				
//		 		        			}
//		 		        			c_routesyn.close();
//		 		        			db.close();	
//	 	      	              	  }
//		                 	   	else
//		                 			{
//		                 				Crashlytics.log(android.util.Log.ERROR,TAG,"Error HandOver_DtlsActivity step 9TOMLoginUserActivity.file is null ");
//		                 				//onClickGoToHomePage();
//		                 			}
//
//	 		        			try {
////	 		        				CallSmsDelUndelApi.main(new String[]{});
//	 		        				CallSmsDelUndelApi csms = new CallSmsDelUndelApi(getApplicationContext());//.main(new String[]{});
//	 		        				csms.callsms();
////									finish();
//								} catch (Exception e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//	 		        			
//	 		        			if(PageName.equals("from_delpage"))
//	 		        			{
//	 		        			count_del =bundle.getString("count_val");
//	 		        			count_del_sync=count_del+1;
//	 		        			Intent goToNextActivity = new Intent(getApplicationContext(), Conveyance_newMainActivity.class);
////	 		        			goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//	 		        			Bundle bundle = new Bundle();
//	 		        	        bundle.putString("counter", count_del_sync);
//	 		        	        goToNextActivity.putExtras(bundle);
//	 		        	        goToNextActivity.putExtra(DEL_UPDATE_ASSGN_ID, employeeId);
//	 		        			startActivity(goToNextActivity);
//	 		        			
//	 		        			}
//	 		        			else if(PageName.equals("from_undelpage"))
//	 		        			{
//	 		        				Intent goToNextActivity = new Intent(getApplicationContext(), UndeliveryActivity.class);
////	 		        				goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//    	 		        			startActivity(goToNextActivity);
//    	 		        			
//	 		        			}
//
//	 		        			else
//	 		        			{
//	 		        				Crashlytics.log(android.util.Log.ERROR,TAG,"comin out of the loop");
//	 		        			}
//	 		        			}
//	 		        		
//	 		        		else{
//	 		        			tv3.setText("Cannot exceed or decrease amount : " + str_amt);
//	 		        			btn.setEnabled(true);
//	
//	 		        	       	}        	
//	                     }
//	                   
//		            	}
//
//	              }
//		            	);
//	              
//	              final AlertDialog.Builder alert = new AlertDialog.Builder(this);
//	              alert.setView(layout);		    	 
//	              alert.create();
//	              alert.show();	   
//		    	}
//		    	}
			
		    }
       
		 }
    }
   		catch(SQLiteException e){
   			//onClickGoToHomePage();
   			Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity cod popup SQLiteException ");
		}
     catch (Exception e) 
     {
		// TODO Auto-generated catch block
		e.printStackTrace();
		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity cod popup Exception ");
		//onClickGoToHomePage();
		
     }
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity cod popup UnsatisfiedLinkError ");
    		//onClickGoToHomePage();	
    	}
	  finally {
          if (crs != null) 
          {
        	  crs.close();
          }
          
          if(c_routesyn !=null)
          {
        	  c_routesyn.close();
          }
          if(cursor !=null)
          {
        	  cursor.close();
          }
       
          if (db != null) {
              db.close();
          }
      }
		    	
	}

	public void toggleMenu(View v)
	{
		Intent homeActivity = new Intent(this, HandOverMainActivity.class);
		startActivity(homeActivity);
	}

    @Override
	public void onBackPressed() {
        // do something on back.
    	try
		{
//    		mGetDelivered.setEnabled(true);
//   		 mGetDelivered.setBackgroundColor(Color.GRAY);
    	/*Intent goToNextActivity = new Intent(getApplicationContext(), HandOverMainActivity.class);
//    	goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(goToNextActivity);*/
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity onBackPressed " + e.getMessage());
//			//onClickGoToHomePage();	
		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
//    		//onClickGoToHomePage();	
    	}
    }
//    public void onclickHome(View v)
//    {
//    	Intent goToNextActivity = new Intent(getApplicationContext(), TOMWelcomeActivity.class);
//        startActivity(goToNextActivity);
//    }
    //**********************modification for menu options(Home & Logout)**************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater() ;
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_home:
                onClickGoToHomePage();
                return true;
            case R.id.menu_log_out:
                onClickLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onClickLogOut()
    {
    	try
		{
        Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
        logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(logOutActivity);
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity onClickLogOut " + e.getMessage());
//			Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
//	        logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//	        startActivity(logOutActivity);
		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
//    		Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
//	        logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//	        startActivity(logOutActivity);	
    	}

    }

    private void onClickGoToHomePage()
    {
    	try
		{
//        Intent homePageActivity = new Intent(this,TOMWelcomeActivity.class);
////        homePageActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(homePageActivity);
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HandOver_DtlsActivity onClickGoToHomePage " + e.getMessage());
//			Intent homePageActivity = new Intent(this,TOMWelcomeActivity.class);
//	        homePageActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//	        startActivity(homePageActivity);
		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error HandOver_DtlsActivity UnsatisfiedLinkError ");
//    		Intent homePageActivity = new Intent(this,TOMWelcomeActivity.class);
//            homePageActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(homePageActivity);	
    	}

    }
   
}
 




//package com.navatech.tom;
//
//import android.os.Bundle;
//import android.app.Activity;
//import android.view.Menu;
//import android.view.Window;
//
//public class HandOver_DtlsActivity extends Activity {
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		setContentView(R.layout.activity_hand_over__dtls);
//		getWindow().setFeatureInt(Window.FEATURE_NO_TITLE,R.layout.activity_hand_over__dtls);	
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.hand_over__dtls, menu);
//		return true;
//	}
//
//}
